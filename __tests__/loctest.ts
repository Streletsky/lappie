import _ from 'lodash'
import strings from '../src/res/strings'
import stringsEn from '../src/res/strings.en'
import stringsRu from '../src/res/strings.ru'

describe('RU strings file structure should match EN string file structure', (): void => {
  const iterateAndCompareObjects = (obj, obj2): void => {
    Object.keys(obj).forEach((key): void => {
      if (typeof obj[key] === 'object') {
        iterateAndCompareObjects(obj[key], obj2[key])
      } else {
        test(`both EN and RU files have key: ${key}`, (): void => expect(obj2[key]).toBeTruthy())
      }
    })
  }

  iterateAndCompareObjects(stringsEn, stringsRu)
})

describe('i18n strings file structure should match EN string file structure', (): void => {
  const iterateAndCompareObjects = (obj, obj2): void => {
    Object.keys(obj).forEach((key): void => {
      if (typeof obj[key] === 'object') {
        iterateAndCompareObjects(obj[key], obj2[key])
      } else {
        test(`both EN and i18n files have key: ${key}`, (): void => expect(obj2[key]).toBeTruthy())
      }
    })
  }

  iterateAndCompareObjects(stringsEn, strings)
})

describe('i18n strings object properties must have matching keys in argument', (): void => {
  const iterateObject = (obj, rootPath): void => {
    Object.keys(obj).forEach((key): void => {
      if (typeof obj[key] === 'object') {
        iterateObject(obj[key], `${rootPath}${key}.`)
      } else {
        const fullKey = `${rootPath}${key}`
        test(`i18n strings object property name is the same as the key passed to t function: ${fullKey}`, (): void => {
          expect(_.get(strings, fullKey).toString()).toContain(fullKey)
        })
      }
    })
  }

  iterateObject(strings, '')
})
