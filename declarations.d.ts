declare module '*.svg' {
  import { SvgProps } from 'react-native-svg'

  const content: React.StatelessComponent<SvgProps>
  export default content
}

declare module 'convert-units'
declare module 'react-native-switch-pro'
declare module 'react-native-progress/Bar'
