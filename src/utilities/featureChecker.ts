import { Platform, Dimensions } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'

const featureChecker = {
  isIos: (): boolean => Platform.OS === 'ios',
  isAndroid: (): boolean => Platform.OS === 'android',
  isPortrait: (): boolean => {
    const dim = Dimensions.get('screen')
    return dim.height >= dim.width
  },
  isLandscape: (): boolean => {
    const dim = Dimensions.get('screen')
    return dim.width >= dim.height
  },
  isIphoneX: (): boolean => {
    return isIphoneX()
  },
  isDevEnv: (): boolean => {
    return __DEV__
  },
  getScreenHeight: (): number => Dimensions.get('screen').height,
  getScreenWidth: (): number => Dimensions.get('screen').width,
  getWindowHeight: (): number => Dimensions.get('window').height,
  getWindowWidth: (): number => Dimensions.get('window').width,
}

export default featureChecker
