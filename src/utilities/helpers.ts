import { ImageSourcePropType } from 'react-native'
import moment from 'moment'
import i18n from 'i18n-js'
import convert from 'convert-units'
import _ from 'lodash'

import R from '../res/R'
import PetType from '../types/PetType'
import IPet from '../types/Pet'
import IVaccination from '../types/Vaccination'
import ITreatment from '../types/Treatment'
import IWeight from '../types/Weight'
import IDeworming from '../types/Deworming'
import IVisit from '../types/Visit'

const yearsToRu = (years: number): string => {
  let txt
  let count = years % 100

  if (count >= 5 && count <= 20) {
    txt = 'лет'
  } else {
    count %= 10

    if (count === 1) {
      txt = 'год'
    } else if (count >= 2 && count <= 4) {
      txt = 'года'
    } else {
      txt = 'лет'
    }
  }

  return txt
}

/* const monthsToRu = (months: number): string => {
  let txt

  if (months === 1) {
    txt = 'месяц'
  } else if (months >= 2 && months <= 4) {
    txt = 'месяца'
  } else {
    txt = 'месяцев'
  }

  return txt
} */

export const getPetDefaultPhoto = (type: PetType): ImageSourcePropType => {
  switch (type) {
    case PetType.Cat:
      return R.images.placeholderCat
    case PetType.Dog:
      return R.images.placeholderDog
    case PetType.Rabbit:
      return R.images.placeholderRabbit
    case PetType.Fish:
      return R.images.placeholderFish
    case PetType.Bird:
      return R.images.placeholderBird
    default:
      return R.images.placeholderRodent
  }
}

export const getPetAge = (pet: IPet): string => {
  const today = moment(new Date())
  let { ageMonths } = pet

  if (!pet.ageOptionSelected) {
    if (!pet.birthday) return ''

    ageMonths = today.diff(moment(pet.birthday), 'months')
  } else {
    ageMonths += today.diff(moment(pet.ageEnteredOn), 'months')
  }

  const [locale] = i18n.locale.split('-')

  if (locale === 'ru') {
    if (ageMonths === 0 && pet.ageOptionSelected) return `<1 мес.`

    if (ageMonths === 0 && !pet.ageOptionSelected) {
      return `${today.diff(moment(pet.birthday), 'days')} дней`
    }

    if (ageMonths < 12) return `${ageMonths} мес.`

    if (ageMonths >= 12) {
      const years = Math.floor(ageMonths / 12)

      if (ageMonths % 12 === 0) {
        return `${years} ${yearsToRu(years)}`
      }

      const monthsOver = ageMonths - years * 12

      return `${years} ${yearsToRu(years)} ${monthsOver} мес.`
    }
  } else if (locale === 'en') {
    if (ageMonths === 0 && pet.ageOptionSelected) return `<1m.`

    if (ageMonths === 0 && !pet.ageOptionSelected) {
      return `${today.diff(moment(pet.birthday), 'days')} days`
    }

    if (ageMonths < 12) return `${ageMonths}m.`

    if (ageMonths >= 12) {
      const years = Math.floor(ageMonths / 12)

      if (ageMonths % 12 === 0) {
        return `${years}y.`
      }

      const monthsOver = ageMonths - years * 12

      return `${years}y. ${monthsOver}m.`
    }
  }

  return ''
}

export const getPetType = (pet: IPet): string => {
  const { type } = pet

  switch (type) {
    case PetType.Cat:
      return R.strings.pets.cat()
    case PetType.Dog:
      return R.strings.pets.dog()
    case PetType.Rabbit:
      return R.strings.pets.rabbit()
    case PetType.Fish:
      return R.strings.pets.fish()
    case PetType.Bird:
      return R.strings.pets.bird()
    case PetType.Rodent:
      return R.strings.pets.rodent()
    default:
      return ''
  }
}

export const convertWeightToLb = (selectedUnitIndex: number, weight: IWeight, petType: PetType): IWeight => {
  if (selectedUnitIndex === 1) {
    // if selected unit is LB, don't convert weight since we store weights in LB
    return weight
  }

  const inputWeight = weight.weightUnit1 + weight.weightUnit2 / 10

  let convertedWeight = 0
  let upperLimit = 0

  if (selectedUnitIndex === 0) {
    switch (petType) {
      case PetType.Cat:
        upperLimit = 23.8
        break
      case PetType.Rabbit:
        upperLimit = 10.8
        break
      default:
        upperLimit = 180.8
        break
    }

    convertedWeight = convert(inputWeight)
      .from('kg')
      .to('lb')
      .toFixed(3)
  }

  if (convertedWeight > upperLimit) convertedWeight = upperLimit

  let remainder = Math.round((convertedWeight - Math.floor(convertedWeight)) * 10)
  if (remainder === 10) {
    convertedWeight = Math.ceil(convertedWeight)
    remainder = 0
  }

  return { ...weight, weightUnit1: Math.floor(convertedWeight), weightUnit2: remainder }
}

export const convertWeightFromLb = (selectedUnitIndex: number, weight: IWeight, petType?: PetType): IWeight => {
  if (selectedUnitIndex === 1) {
    // if selected unit is LB, don't convert weight since we store weights in LB
    return weight
  }

  const inputWeight = weight.weightUnit1 + weight.weightUnit2 / 10

  let convertedWeight = 0
  let upperLimit = 0

  if (selectedUnitIndex === 0) {
    switch (petType) {
      case PetType.Cat:
        upperLimit = 10.8
        break
      case PetType.Rabbit:
        upperLimit = 4.8
        break
      default:
        upperLimit = 80.8
        break
    }

    convertedWeight = convert(inputWeight)
      .from('lb')
      .to('kg')
      .toFixed(3)

    if (convertedWeight > upperLimit) convertedWeight = upperLimit
  }

  if (convertedWeight > upperLimit) convertedWeight = upperLimit

  let remainder = Math.round((convertedWeight - Math.floor(convertedWeight)) * 10)
  if (remainder === 10) {
    convertedWeight = Math.ceil(convertedWeight)
    remainder = 0
  }

  return { ...weight, weightUnit1: Math.floor(convertedWeight), weightUnit2: remainder }
}

export const getWeightText = (lastWeight: IWeight, unitOfMeasureIndex: number): string => {
  const unitString = unitOfMeasureIndex === 0 ? 'kg' : 'lbs'

  if (lastWeight.weightUnit1 === 0 && lastWeight.weightUnit2 === 0) {
    return `N/A`
  }

  const convertedWeight = convertWeightFromLb(unitOfMeasureIndex, lastWeight, undefined)

  if (convertedWeight.weightUnit2 === 0) {
    return `${convertedWeight.weightUnit1} ${unitString}`
  }

  return `${convertedWeight.weightUnit1}.${convertedWeight.weightUnit2} ${unitString}`
}

export const getCurrentPetWeight = (pet: IPet, unitOfMeasureIndex: number): string => {
  if (!pet.weightEntries || pet.weightEntries.length === 0) {
    return `N/A`
  }

  const lastWeight = _.last(_.sortBy(pet.weightEntries, 'date'))!

  return getWeightText(lastWeight, unitOfMeasureIndex)
}

export const formatDate = (date: Date | moment.Moment): string => moment(date).format('DD MMMM YYYY')

export const formatDateShort = (date: Date | moment.Moment | undefined): string => moment(date).format('DD MMM YYYY')

export const formatTime = (date: Date | moment.Moment): string => moment(date).format('HH:mm')

export const getVaccinationEndDate = (vaccination: IVaccination): string => {
  const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')

  return formatDateShort(endDate)
}

export const getVaccinationRemainingTime = (vaccination: IVaccination | IDeworming): string => {
  const today = moment(new Date())
  const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')
  const [locale] = i18n.locale.split('-')

  if (locale === 'ru') {
    const diffMonths = endDate.diff(moment(today), 'months')

    if (Math.abs(diffMonths) >= 1) {
      return diffMonths >= 0 ? `Осталось ${diffMonths} месяцев` : `${Math.abs(diffMonths)} месяцев назад` // TODO: move to strings
    }

    const diffDays = endDate.diff(moment(today), 'days')

    return diffDays >= 0 ? `Осталось ${diffDays} месяцев` : `${Math.abs(diffDays)} месяцев назад`
  }

  if (locale === 'en') {
    const diffMonths = endDate.diff(moment(today), 'months')

    if (Math.abs(diffMonths) >= 1) {
      return diffMonths >= 0 ? `${diffMonths} months left` : `${Math.abs(diffMonths)} months past`
    }

    const diffDays = endDate.diff(moment(today), 'days')

    return diffDays >= 0 ? `${diffDays} days left` : `${Math.abs(diffDays)} days past`
  }

  return ''
}

export const isVaccinationEndUpcoming = (vaccination: IVaccination | IDeworming): boolean => {
  const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')
  const startDate = moment(vaccination.startDate)
  const today = new Date()

  if (startDate.diff(today, 'hours') <= 0 && endDate.diff(today, 'hours') > 0) {
    return endDate.diff(today, 'days') <= 40
  }

  return false
}

export const isVaccinationActive = (vaccination: IVaccination | IDeworming): boolean => {
  const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')
  const startDate = moment(vaccination.startDate)
  const today = new Date()

  return startDate.diff(today, 'hours') <= 0 && endDate.diff(today, 'hours') > 0
}

export const isVaccinationPlanned = (vaccination: IVaccination | IDeworming): boolean => {
  const startDate = moment(vaccination.startDate)
  const today = new Date()

  return startDate.diff(today, 'hours') > 0
}

export const isVaccinationExpired = (vaccination: IVaccination | IDeworming): boolean => {
  const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')
  const today = new Date()

  return endDate.diff(today, 'hours') <= 0
}

export const isTreatmentActive = (treatment: ITreatment): boolean => {
  const endDate = moment(treatment.endDate)
  const startDate = moment(treatment.startDate)
  const today = new Date()

  if (!treatment.endDate && startDate.diff(today, 'hours') <= 0) return true

  return startDate.diff(today, 'hours') <= 0 && endDate.diff(today, 'hours') > 0
}

export const isTreatmentPlanned = (treatment: ITreatment): boolean => {
  const startDate = moment(treatment.startDate)
  const today = new Date()

  return startDate.diff(today, 'hours') > 0
}

export const isTreatmentExpired = (treatment: ITreatment): boolean => {
  const endDate = moment(treatment.endDate)
  const today = new Date()

  return (treatment.endDate && endDate.diff(today, 'hours') <= 0) === true
}

export const getTreatmentPeriod = (treatment: ITreatment): string => {
  const startDate = moment(treatment.startDate)
  const endDate = moment(treatment.endDate)

  if (!treatment.endDate) return formatDateShort(startDate)

  return `${formatDateShort(startDate)} - ${formatDateShort(endDate)}`
}

export const getTreatmentRemainingTime = (treatment: ITreatment): string => {
  const today = moment(new Date())
  const endDate = moment(treatment.endDate)
  const [locale] = i18n.locale.split('-')

  if (locale === 'ru') {
    if (!treatment.endDate) {
      return 'Продолжается'
    }

    const diffMonths = endDate.diff(today, 'months')

    if (Math.abs(diffMonths) >= 1) {
      return diffMonths >= 0 ? `Осталось ${diffMonths} месяцев` : `${Math.abs(diffMonths)} месяцев назад` // TODO: move to strings
    }

    const diffDays = endDate.diff(today, 'days')

    return diffDays >= 0 ? `Осталось ${diffDays} месяцев` : `${Math.abs(diffDays)} месяцев назад`
  }

  if (locale === 'en') {
    if (!treatment.endDate) {
      return 'Ongoing'
    }

    const diffMonths = endDate.diff(today, 'months')

    if (Math.abs(diffMonths) >= 1) {
      return diffMonths >= 0 ? `${diffMonths} months left` : `${Math.abs(diffMonths)} months past`
    }

    const diffDays = endDate.diff(today, 'days')

    return diffDays >= 0 ? `${diffDays} days left` : `${Math.abs(diffDays)} days past`
  }

  return ''
}

export const isVisitPlanned = (visit: IVisit): boolean => {
  const startDate = moment(visit.date)
  const today = new Date()

  return startDate.diff(today, 'hours') > 0
}

export const isVisitExpired = (visit: IVisit): boolean => {
  const endDate = moment(visit.date)
  const today = new Date()

  return endDate.diff(today, 'hours') <= 0
}

export const showVaccination = (pet: IPet): boolean => pet.type !== PetType.Rodent && pet.type !== PetType.Bird && pet.type !== PetType.Fish
export const showDeworming = (pet: IPet): boolean => pet.type !== PetType.Rodent && pet.type !== PetType.Bird && pet.type !== PetType.Fish
export const showSterilization = (pet: IPet): boolean => pet.type === PetType.Cat || pet.type === PetType.Dog
export const showChip = (pet: IPet): boolean => pet.type !== PetType.Rodent && pet.type !== PetType.Bird && pet.type !== PetType.Fish
export const showAllergies = (pet: IPet): boolean => pet.type !== PetType.Rodent && pet.type !== PetType.Fish
export const showHealth = (pet: IPet): boolean => pet.type !== PetType.Fish
export const showDocs = (pet: IPet): boolean => pet.type !== PetType.Fish
export const showWeight = (pet: IPet): boolean => pet.type !== PetType.Fish && pet.type !== PetType.Rodent && pet.type !== PetType.Bird
