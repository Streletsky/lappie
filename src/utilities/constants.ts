import PetType from '../types/PetType'
import IDropdownItem from '../types/DropdownItem'
import appConfig from '../../app.json'
import featureChecker from './featureChecker'
import colors from '../res/colors'
import images from '../res/images'
import strings from '../res/strings'

const petTypes = [PetType.Cat, PetType.Dog, PetType.Rabbit, PetType.Rodent, PetType.Fish, PetType.Bird]

const petTypeColor = {
  [PetType.Dog]: colors.dog,
  [PetType.Rodent]: colors.rodent,
  [PetType.Fish]: colors.fish,
  [PetType.Rabbit]: colors.rabbit,
  [PetType.Bird]: colors.bird,
  [PetType.Cat]: colors.cat,
}

const petTypeImage = {
  [PetType.Dog]: images.typeDog,
  [PetType.Rodent]: images.typeRodent,
  [PetType.Fish]: images.typeFish,
  [PetType.Rabbit]: images.typeRabbit,
  [PetType.Bird]: images.typeBird,
  [PetType.Cat]: images.typeCat,
}

const petHealthCardImage = {
  [PetType.Dog]: images.healthCardImageDog,
  [PetType.Rodent]: images.healthCardImageRodent,
  [PetType.Fish]: images.healthCardImageFish,
  [PetType.Rabbit]: images.healthCardImageRabbit,
  [PetType.Bird]: images.healthCardImageBird,
  [PetType.Cat]: images.healthCardImageCat,
}

const languages: { [key: number]: string } = {
  0: 'en',
  1: 'ru',
}

const sexes: { [key: number]: string } = {
  0: strings.pets.male(),
  1: strings.pets.female(),
}

const periodicityDropdownItems: IDropdownItem[] = [
  { text: strings.ui.noData(), value: undefined },
  { text: strings.pets.onceIn6Months(), value: 0.5 },
  { text: strings.pets.onceIn1Years(), value: 1 },
  { text: strings.pets.onceIn2Years(), value: 2 },
  { text: strings.pets.onceIn3Years(), value: 3 },
]

const extendedPeriodicityDropdownItems: IDropdownItem[] = [
  { text: strings.ui.noData(), value: undefined },
  { text: strings.pets.daily3Times(), value: 1 },
  { text: strings.pets.daily2Times(), value: 2 },
  { text: strings.pets.daily(), value: 3 },
  { text: strings.pets.onceIn1Week(), value: 4 },
  { text: strings.pets.onceIn2Weeks(), value: 5 },
  { text: strings.pets.onceIn1Month(), value: 6 },
  { text: strings.pets.onceIn6Months(), value: 7 },
  { text: strings.pets.onceIn1Years(), value: 8 },
  { text: strings.pets.onceIn2Years(), value: 9 },
  { text: strings.pets.onceIn3Years(), value: 10 },
]

const dewormingDropdownItems: IDropdownItem[] = [{ text: 'Pill', value: 1 }, { text: 'Drop', value: 2 }]

const weightUoms: string[] = ['kg', 'lbs']

const googleMapsApiKey: string = featureChecker.isIos()
  ? appConfig.expo.ios.config.googleMapsApiKey
  : appConfig.expo.android.config.googleMaps.apiKey

const admobUnitId = featureChecker.isDevEnv()
  ? 'ca-app-pub-3940256099942544/6300978111'
  : featureChecker.isIos()
  ? 'ca-app-pub-5742121175758278/3778560070'
  : 'ca-app-pub-5742121175758278/3200771736' // get from app.json

const bigScreenSize = 375

const constants = {
  petTypes,
  petTypeColor,
  petTypeImage,
  languages,
  sexes,
  userLocationExpiresInMinutes: 10,
  userMarkersExpireInDays: 30,
  userLocationKmDeltaForCaching: 1,
  periodicityDropdownItems,
  extendedPeriodicityDropdownItems,
  weightUoms,
  petHealthCardImage,
  googleMapsApiKey,
  dewormingDropdownItems,
  admobUnitId,
  bigScreenSize,
}

export default constants
