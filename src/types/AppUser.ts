export default interface IAppUser {
  uid: string
  email: string
  password: string
}
