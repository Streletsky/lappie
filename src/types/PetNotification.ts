import PetNotificationType from './PetNotificationType'
import PetNotificationSubType from './PetNotificationSubType'
import IPet from './Pet'
import IVaccination from './Vaccination'
import ITreatment from './Treatment'

export default interface IPetNotification {
  id: string
  type: PetNotificationType
  subType: PetNotificationSubType
  title: string
  caption?: string
  pet: IPet
  payload?: IVaccination | ITreatment
}
