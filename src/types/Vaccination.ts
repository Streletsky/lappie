export default interface IVaccination {
  id: string
  name: string
  startDate: Date
  periodicity: number
  notes?: string
}
