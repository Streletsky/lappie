import { LatLng } from 'react-native-maps'

export default interface IMapPlace {
  id: string
  coordinate: LatLng
  address: string
  title: string
  rating: number
  phone?: string
  modifiedAt: Date
  detailsLoaded: boolean
}
