export default interface IDropdownItem {
  value?: string | number
  text: string
}
