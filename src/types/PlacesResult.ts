import IMapPlace from './MapPlace'

export interface ILatLng {
  lat: number
  lng: number
}

export interface IUserLocation {
  location: ILatLng
  datestamp: Date
}

export interface IPlaceResult {
  formatted_address: string
  formatted_phone_number: string
  geometry: IUserLocation
  icon: string
  id: string
  international_phone_number: string
  name: string
  place_id: string
  price_level: number
  rating: number
  url: string
  vicinity: string
}

export interface IPlacesJsonResult {
  results: IPlaceResult[]
}

export interface IPlaceJsonResult {
  result: IPlaceResult
}

export interface IUserMarkersWithLocation {
  markers: IMapPlace[]
  userLocation: IUserLocation
}
