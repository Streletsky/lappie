enum DewormingType {
  Pill = 1,
  Drop = 2,
}

export default DewormingType
