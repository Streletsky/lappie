export default interface IVisit {
  id: string
  date: Date
  name: string
  clinic: string
  notes: string
}
