export default interface IWeight {
  id: string
  date: Date
  weightUnit1: number
  weightUnit2: number
}
