import { ISettingsState } from '../reducers/settingsReducer'
import { IDoctorsState } from '../reducers/doctorsReducer'
import { IPetsState } from '../reducers/petsReducer'
import { ILocationState } from '../reducers/locationReducer'

export default interface IAppState {
  settings: ISettingsState
  doctors: IDoctorsState
  pets: IPetsState
  location: ILocationState
}
