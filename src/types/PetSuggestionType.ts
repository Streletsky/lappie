enum PetSuggestionType {
  PlayWithPet = 1,
  AddFirstWeight = 2,
  CheckWater = 3,
}

export default PetSuggestionType
