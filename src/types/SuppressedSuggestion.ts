import PetSuggestionType from './PetSuggestionType'

export default interface ISuppressedSuggestion {
  petId: string
  suggestionType: PetSuggestionType
  suppressedUntil: Date
}
