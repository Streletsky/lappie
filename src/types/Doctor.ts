export default interface IDoctor {
  id: string
  name: string
  clinic?: string
  phone?: string
  address?: string
  notes?: string
  createdAt: Date
  modifiedAt: Date
}
