enum PetType {
  Cat = 1,
  Dog = 2,
  Rabbit = 3,
  Fish = 4,
  Bird = 5,
  Rodent = 6,
}

export default PetType
