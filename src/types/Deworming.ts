import DewormingType from './DewormingType'

export default interface IDeworming {
  id: string
  name: string
  startDate: Date
  periodicity: number
  notes?: string
  type: DewormingType
}
