/* eslint-disable @typescript-eslint/no-explicit-any */
import { NavigationScreenProp } from 'react-navigation'

export interface INavigatableComponentProps {
  navigation: NavigationScreenProp<any, any>
}
