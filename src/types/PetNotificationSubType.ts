enum PetNotificationSubType {
  PlannedVaccination = 1,
  VaccinationAboutToExpire = 2,
  NoActiveVaccination = 3,
  RecentlyExpiredVaccination = 4,
  PlannedDeworming = 5,
  DewormingAboutToExpire = 6,
  NoActiveDeworming = 7,
  RecentlyExpiredDeworming = 8,
  PlannedTreatment = 9,
  OnGoingTreatment = 10,
}

export default PetNotificationSubType
