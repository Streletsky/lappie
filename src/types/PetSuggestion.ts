import IPet from './Pet'
import PetSuggestionType from './PetSuggestionType'

export default interface IPetSuggestion {
  id: string
  type: PetSuggestionType
  title: string
  pet: IPet
}
