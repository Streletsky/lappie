export default interface ITreatment {
  id: string
  name: string
  description: string
  startDate: Date
  endDate?: Date
  periodicity?: number
  timeOfDay1?: Date
  timeOfDay2?: Date
  timeOfDay3?: Date
  timeOfDay1CheckedAt?: Date
  timeOfDay2CheckedAt?: Date
  timeOfDay3CheckedAt?: Date
}
