export default interface IUserProfile {
  email: string
  username: string
}
