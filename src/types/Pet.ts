import PetType from './PetType'
import Sex from './Sex'
import IVaccination from './Vaccination'
import ITreatment from './Treatment'
import IWeight from './Weight'
import IDeworming from './Deworming'
import IVisit from './Visit'

export default interface IPet {
  id: string
  name: string
  breed?: string
  photo?: string
  type: PetType
  sex?: Sex
  birthday?: Date
  ageEnteredOn?: Date
  ageMonths: number
  ageOptionSelected: boolean
  weightEntries: IWeight[]
  createdAt: Date
  modifiedAt: Date
  sterilization: boolean
  chip: boolean
  chipData: {
    serialNumber: string
    clinic: string
    date: Date
  }
  vaccinations: IVaccination[]
  dewormings: IDeworming[]
  treatments: ITreatment[]
  visits: IVisit[]
}
