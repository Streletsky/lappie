enum PetNotificationType {
  Vaccination = 1,
  Treatment = 2,
  Food = 3,
  Hygiene = 4,
  Deworming = 5,
}

export default PetNotificationType
