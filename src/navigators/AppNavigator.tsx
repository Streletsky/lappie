import React, { ReactElement } from 'react'
import { View } from 'react-native'
import {
  CreateNavigatorConfig,
  NavigationStackRouterConfig,
  NavigationScreenConfig,
  NavigationScreenProp,
  NavigationRoute,
} from 'react-navigation'
import { createStackNavigator, NavigationStackProp, NavigationStackConfig, NavigationStackOptions } from 'react-navigation-stack'
import { createBottomTabNavigator, NavigationBottomTabOptions } from 'react-navigation-tabs'

import R from '../res/R'
import AppIcons from '../components/AppIcons'
import PetsScreen from '../screens/pets/PetsScreen'
import PetProfileScreen from '../screens/pets/PetProfileScreen'
import PetHealthScreen from '../screens/pets/PetHealthScreen'
import PetProfileSetupScreen from '../screens/pets/PetProfileSetupScreen'
import PetWeightSetupScreen from '../screens/pets/PetWeightSetupScreen'
import PetHealthSetupScreen from '../screens/pets/PetHealthSetupScreen'
import SettingsScreen from '../screens/settings/SettingsScreen'
import AboutScreen from '../screens/settings/AboutScreen'
import HelpScreen from '../screens/settings/HelpScreen'
import LegalScreen from '../screens/settings/LegalScreen'
import DoctorsScreen from '../screens/doctors/DoctorsScreen'
import DoctorProfileScreen from '../screens/doctors/DoctorProfileScreen'
import VaccinationsOverviewScreen from '../screens/pets/VaccinationsOverviewScreen'
import VaccinationInformationScreen from '../screens/pets/VaccinationInformationScreen'
import TreatmentsOverviewScreen from '../screens/pets/TreatmentsOverviewScreen'
import TreatmentInformationScreen from '../screens/pets/TreatmentInformationScreen'
import WeightOverviewScreen from '../screens/pets/WeightOverviewScreen'
import WeightDetailsScreen from '../screens/pets/WeightDetailsScreen'
import LibraryScreen from '../screens/library/LibraryScreen'
import DewormingsOverviewScreen from '../screens/pets/DewormingsOverviewScreen'
import DewormingInformationScreen from '../screens/pets/DewormingInformationScreen'
import VisitsOverviewScreen from '../screens/pets/VisitsOverviewScreen'
import VisitInformationScreen from '../screens/pets/VisitInformationScreen'
import VacinationArticleScreen from '../screens/settings/help/VaccinationArticleScreen'
import DewormingArticleScreen from '../screens/settings/help/DewormingArticleScreen'
import PlannedVisitsArticleScreen from '../screens/settings/help/PlannedVisitsArticleScreen'
import WeightArticleScreen from '../screens/settings/help/WeightArticleScreen'
import TreatmentArticleScreen from '../screens/settings/help/TreatmentArticleScreen'
import DoctorsArticleScreen from '../screens/settings/help/DoctorsArticleScreen'
import UserAccountArticleScreen from '../screens/settings/help/UserAccountArticleScreen'
import { INavigatableComponentProps } from '../types/NavigatableComponentProps'

const defaultStackNavigationOption: CreateNavigatorConfig<
  NavigationStackConfig,
  NavigationStackRouterConfig,
  NavigationStackOptions,
  NavigationStackProp
> = {
  defaultNavigationOptions: {
    headerTitleStyle: {
      ...R.styles.fonts.headline,
      width: '100%',
    },
    headerStyle: {
      backgroundColor: R.colors.white,
      borderBottomColor: R.colors.darkCloud,
      borderBottomWidth: 0,
      elevation: 0,
    },
    // eslint-disable-next-line react/prop-types
    headerBackImage: ({ tintColor }): ReactElement => (
      <View style={R.styles.headerLeftButton}>
        <AppIcons name="back" size={14} color={tintColor} />
      </View>
    ),
    headerBackTitleVisible: false,
    headerTintColor: R.colors.black,
    headerTitleAlign: 'center',
  },
}

const PetsScreenStack = createStackNavigator(
  {
    PetsScreen: { screen: PetsScreen },
    PetWeightSetupScreen: { screen: PetWeightSetupScreen },
    PetHealthSetupScreen: { screen: PetHealthSetupScreen },
    PetProfileSetupScreen: { screen: PetProfileSetupScreen },
    PetProfileScreen: { screen: PetProfileScreen },
    PetHealthScreen: { screen: PetHealthScreen },
    VaccinationsOverviewScreen: { screen: VaccinationsOverviewScreen },
    VaccinationInformationScreen: { screen: VaccinationInformationScreen },
    TreatmentsOverviewScreen: { screen: TreatmentsOverviewScreen },
    TreatmentInformationScreen: { screen: TreatmentInformationScreen },
    WeightOverviewScreen: { screen: WeightOverviewScreen },
    WeightDetailsScreen: { screen: WeightDetailsScreen },
    DewormingsOverviewScreen: { screen: DewormingsOverviewScreen },
    DewormingInformationScreen: { screen: DewormingInformationScreen },
    VisitsOverviewScreen: { screen: VisitsOverviewScreen },
    VisitInformationScreen: { screen: VisitInformationScreen },
  },
  defaultStackNavigationOption,
)

PetsScreenStack.navigationOptions = ({
  navigation,
}: INavigatableComponentProps): NavigationScreenConfig<{}, NavigationScreenProp<NavigationRoute>> => {
  const { routeName } = navigation.state.routes[navigation.state.index]
  let tabBarVisible = true

  if (routeName === 'PetWeightSetupScreen' || routeName === 'PetHealthSetupScreen' || routeName === 'PetProfileSetupScreen') {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}

const DoctorsScreenStack = createStackNavigator(
  {
    DoctorsScreen: { screen: DoctorsScreen },
    DoctorProfileScreen: { screen: DoctorProfileScreen },
  },
  defaultStackNavigationOption,
)

const LibraryScreenStack = createStackNavigator(
  {
    LibraryScreen: { screen: LibraryScreen },
  },
  defaultStackNavigationOption,
)

const SettingsScreenStack = createStackNavigator(
  {
    SettingsScreen: { screen: SettingsScreen },
    AboutScreen: { screen: AboutScreen },
    HelpScreen: { screen: HelpScreen },
    LegalScreen: { screen: LegalScreen },
    VacinationArticleScreen: { screen: VacinationArticleScreen },
    DewormingArticleScreen: { screen: DewormingArticleScreen },
    PlannedVisitsArticleScreen: { screen: PlannedVisitsArticleScreen },
    WeightArticleScreen: { screen: WeightArticleScreen },
    TreatmentArticleScreen: { screen: TreatmentArticleScreen },
    DoctorsArticleScreen: { screen: DoctorsArticleScreen },
    UserAccountArticleScreen: { screen: UserAccountArticleScreen },
  },
  defaultStackNavigationOption,
)

const AppNavigator = createBottomTabNavigator(
  {
    Pets: PetsScreenStack,
    Doctors: DoctorsScreenStack,
    // Library: LibraryScreenStack,
    Settings: SettingsScreenStack,
  },
  {
    defaultNavigationOptions: ({ navigation }: INavigatableComponentProps): NavigationBottomTabOptions => ({
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ tintColor }): ReactElement => {
        const { routeName } = navigation.state

        let iconName = ''
        if (routeName === 'Pets') {
          iconName = `paw`
        } else if (routeName === 'Settings') {
          iconName = `settings`
        } else if (routeName === 'Doctors') {
          iconName = `doctors`
        } else if (routeName === 'AddButton') {
          iconName = `plus`
        } else if (routeName === 'Library') {
          iconName = `library`
        }

        return (
          <View>
            <AppIcons
              name={iconName}
              size={routeName === 'AddButton' ? 34 : 27}
              color={routeName === 'AddButton' ? R.colors.black : tintColor}
              style={{ width: routeName === 'AddButton' ? 34 : 27 }}
            />
          </View>
        )
      },
    }),
    tabBarOptions: {
      activeTintColor: R.colors.cedar,
      inactiveTintColor: R.colors.black,
      style: {
        borderTopColor: R.colors.darkCloud,
        borderTopWidth: 1,
        backgroundColor: R.colors.white,
      },
      labelStyle: {
        ...R.styles.fonts.footnote,
      },
      showLabel: true,
    },
    initialRouteName: 'Pets',
  },
)

const RootStack = createStackNavigator(
  {
    AppNavigator,
  },
  { headerMode: 'none', mode: 'modal' },
)

export default RootStack
