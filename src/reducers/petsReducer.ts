import IPet from '../types/Pet'
import { IPetsAction } from '../actions/actionCreators'

export interface IPetsState {
  pets: IPet[]
}

const defaultState: IPetsState = {
  pets: [],
}

export default (state = defaultState, action: IPetsAction): IPetsState => {
  switch (action.type) {
    case 'ADD_PET': {
      return {
        ...state,
        pets: [...state.pets, { ...action.payload, createdAt: new Date(), modifiedAt: new Date() }],
      }
    }
    case 'SAVE_PET': {
      return {
        ...state,
        pets: state.pets.map(
          (pet): IPet => {
            if (pet.id !== action.payload.id) {
              return pet
            }

            return {
              ...pet,
              ...action.payload,
              modifiedAt: new Date(),
            }
          },
        ),
      }
    }
    case 'REMOVE_PET': {
      return {
        ...state,
        pets: state.pets.filter((pet): boolean => action.payload.id !== pet.id),
      }
    }
    default:
      return state
  }
}
