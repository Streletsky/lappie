import { IUserLocation, IUserMarkersWithLocation } from '../types/PlacesResult'
import { ILocationAction } from '../actions/actionCreators'
import IMapPlace from '../types/MapPlace'

export interface ILocationState {
  userLocation: IUserLocation
  savedMarkers: IUserMarkersWithLocation
}

const defaultState: ILocationState = {
  userLocation: {
    location: {
      lat: 40.783689,
      lng: -73.964916,
    },
    datestamp: new Date(2018, 12, 12, 0, 0, 0),
  },
  savedMarkers: {
    markers: [],
    userLocation: {
      location: {
        lat: 40.783689,
        lng: -73.964916,
      },
      datestamp: new Date(2018, 12, 12, 0, 0, 0),
    },
  },
}

export default function(state = defaultState, action: ILocationAction): ILocationState {
  switch (action.type) {
    case 'SAVE_LOCATION':
      return {
        ...state,
        userLocation: action.payload,
      }
    case 'SAVE_MARKERS':
      return {
        ...state,
        savedMarkers: action.payload,
      }
    case 'UPDATE_MARKER':
      return {
        ...state,
        savedMarkers: {
          ...state.savedMarkers,
          markers: state.savedMarkers.markers.map(
            (m): IMapPlace => {
              if (m.id !== action.payload.id) {
                return m
              }

              return {
                ...m,
                ...action.payload,
              }
            },
          ),
        },
      }
    default:
      return state
  }
}
