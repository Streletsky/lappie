import { combineReducers } from 'redux'

import settings from './settingsReducer'
import doctors from './doctorsReducer'
import pets from './petsReducer'
import location from './locationReducer'

const rootReducer = combineReducers({
  settings,
  doctors,
  pets,
  location,
})

export default rootReducer
