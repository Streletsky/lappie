import { ISettingsAction } from '../actions/actionCreators'

export interface ISettingsState {
  selectedWeightUnitIndex: number
  selectedLanguageIndex: number
}

const defaultState: ISettingsState = {
  selectedWeightUnitIndex: 0,
  selectedLanguageIndex: 0,
}

export default function(state = defaultState, action: ISettingsAction): ISettingsState {
  switch (action.type) {
    case 'SET_WEIGHT_UNIT':
      return {
        ...state,
        selectedWeightUnitIndex: action.payload,
      }
    case 'SET_LANGUAGE':
      return {
        ...state,
        selectedLanguageIndex: action.payload,
      }
    default:
      return state
  }
}
