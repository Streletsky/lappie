import IDoctor from '../types/Doctor'
import { IDoctorsAction } from '../actions/actionCreators'

export interface IDoctorsState {
  doctors: IDoctor[]
}

const defaultState: IDoctorsState = {
  doctors: [],
}

export default (state = defaultState, action: IDoctorsAction): IDoctorsState => {
  switch (action.type) {
    case 'ADD_DOCTOR': {
      return {
        ...state,
        doctors: [...state.doctors, { ...action.payload, createdAt: new Date(), modifiedAt: new Date() }],
      }
    }
    case 'SAVE_DOCTOR': {
      return {
        ...state,
        doctors: state.doctors.map(
          (doctor): IDoctor => {
            if (doctor.id !== action.payload.id) {
              return doctor
            }

            return {
              ...doctor,
              ...action.payload,
              modifiedAt: new Date(),
            }
          },
        ),
      }
    }
    case 'REMOVE_DOCTOR': {
      return {
        ...state,
        doctors: state.doctors.filter((doctor): boolean => action.payload.id !== doctor.id),
      }
    }
    default:
      return state
  }
}
