import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
import autoMergeLevel2 from 'redux-persist/es/stateReconciler/autoMergeLevel2'
import { AsyncStorage } from 'react-native'

import rootReducer from './reducers/rootReducer'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
  blacklist: ['ui'],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const middlewares = [thunk]

export const store = createStore(persistedReducer, applyMiddleware(...middlewares))
export const persistor = persistStore(store)
