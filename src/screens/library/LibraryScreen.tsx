import React, { Component, ReactElement } from 'react'
import { Text } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'

export default class LibraryScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.library.library(),
    }
  }

  public state = {}

  public render(): ReactElement {
    return (
      <SafeAreaView style={R.styles.safeArea}>
        <ScrollContainer>
          <Text>Library</Text>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
