import { Text, View, StyleSheet, Linking, Alert, TouchableOpacity } from 'react-native'
import React, { Component, ReactElement } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Button, Divider } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import SafeAreaView from 'react-native-safe-area-view'

import IDoctor from '../../types/Doctor'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import AppIcons from '../../components/AppIcons'
import { IDoctorsAction, saveDoctor, addDoctor, removeDoctor } from '../../actions/actionCreators'
import BottomModal from '../../components/BottomModal'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import DoctorEditForm from '../../components/DoctorEditForm'
import IAppState from '../../types/AppState'
import RemoveButton from '../../components/RemoveButton'

const styles = StyleSheet.create({
  callButtonStyle: {
    ...R.styles.buttons.primaryButton,
    backgroundColor: R.colors.poison,
    marginVertical: 21,
  },
  header: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    marginTop: 38,
    alignItems: 'center',
  },
  headerText: {
    marginTop: 35,
    alignItems: 'center',
  },
  label: {
    ...R.styles.fonts.body,
    marginTop: 6,
  },
  fieldsContainer: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
})

interface IDispatchFromProps {
  handleSaveDoctor(doctor: IDoctor): void
  handleRemoveDoctor(doctor: IDoctor): void
}

interface IComponentState {
  doctor: IDoctor
  initialDoctor: IDoctor
  isDoctorModalOpen: boolean
}

interface IStateFromProps {
  fetchDoctor(id: string): IDoctor
}

class DoctorProfileScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openDoctorModal = navigation.getParam('openDoctorModal', (): void => {})

    return {
      title: R.strings.doctors.doctorProfile(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openDoctorModal}>
          <AppIcons name="edit" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchDoctor } = this.props
    const doctorId = navigation.getParam('doctorId', undefined)
    const doctor = fetchDoctor(doctorId)

    this.state = {
      doctor,
      initialDoctor: doctor,
      isDoctorModalOpen: false,
    }
  }

  private saveDoctor = (): void => {
    const { handleSaveDoctor } = this.props
    const { doctor } = this.state

    handleSaveDoctor(doctor)

    this.setState({ isDoctorModalOpen: false, initialDoctor: doctor })
  }

  private removeDoctor = (): void => {
    const { handleRemoveDoctor, navigation } = this.props
    const { doctor } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            handleRemoveDoctor(doctor)
            navigation.navigate('DoctorsScreen')
          },
        },
      ],
      { cancelable: false },
    )
  }

  private getDoctor = (): void => {
    const { navigation, fetchDoctor } = this.props
    const doctorId = navigation.getParam('doctorId', undefined)
    const doctor = fetchDoctor(doctorId)

    this.setState({ doctor, initialDoctor: doctor })
  }

  private makeCall = (): void => {
    const { doctor } = this.state

    Linking.openURL(`tel:${doctor.phone}`)
  }

  private closeModal = (): void => {
    const { initialDoctor } = this.state

    this.setState({ doctor: initialDoctor, isDoctorModalOpen: false })
  }

  private openDoctorModal = (): void => {
    this.setState({ isDoctorModalOpen: true })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getDoctor)
    navigation.setParams({ openDoctorModal: this.openDoctorModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { doctor, isDoctorModalOpen } = this.state

    return (
      <View style={R.styles.rootView}>
        <BottomModal
          onSave={this.saveDoctor}
          title={R.strings.doctors.editDoctor()}
          onCancel={this.closeModal}
          saveButtonEnabled={(doctor.name && doctor.name.length > 0 && doctor.phone && doctor.phone.length > 0) === true}
          visible={isDoctorModalOpen}
        >
          <View>
            <DoctorEditForm doctor={doctor} setParentState={(data): void => this.setState({ doctor: data })} />
          </View>
        </BottomModal>
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={styles.header}>
              <Text style={{ fontSize: 90, textAlign: 'center' }}>🏥</Text>
              <View style={styles.headerText}>
                <Text style={[R.styles.fonts.hero, { marginBottom: 8 }]}>{doctor.name}</Text>
                {((doctor.clinic && doctor.clinic.length > 0) || (doctor.phone && doctor.phone.length > 0)) && (
                  <Text style={R.styles.fonts.body}>
                    {doctor.clinic && doctor.clinic.length > 0 && doctor.clinic}
                    {doctor.clinic && doctor.clinic.length > 0 && doctor.phone && doctor.phone.length > 0 && ', '}
                    {doctor.phone && doctor.phone.length > 0 && doctor.phone}
                  </Text>
                )}
              </View>
              <View style={{ width: '100%' }}>
                <Button
                  onPress={this.makeCall}
                  title={R.strings.doctors.callDoctor()}
                  buttonStyle={styles.callButtonStyle}
                  titleStyle={R.styles.buttons.primaryButtonTitle}
                  icon={<AppIcons name="phone" size={16} color={R.colors.white} style={{ marginRight: 8 }} />}
                />
              </View>
            </View>
            <Divider style={R.styles.divider} />
            <View>
              {doctor.address && doctor.address.length > 0 ? (
                <View>
                  <View style={styles.fieldsContainer}>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.doctors.address()}</Text>
                    <Text style={styles.label}>{doctor.address}</Text>
                  </View>
                  <Divider style={[R.styles.divider]} />
                </View>
              ) : (
                undefined
              )}
            </View>
            <View>
              {doctor.notes && doctor.notes.length > 0 ? (
                <View>
                  <View style={styles.fieldsContainer}>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.doctors.notes()}</Text>
                    <Text style={styles.label}>{doctor.notes}</Text>
                  </View>
                  <Divider style={[R.styles.divider]} />
                </View>
              ) : (
                undefined
              )}
            </View>
            <RemoveButton title={R.strings.doctors.remove()} onPress={this.removeDoctor} />
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}
const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleRemoveDoctor: (doctor: IDoctor): IDoctorsAction => dispatch(removeDoctor(doctor)),
  handleSaveDoctor: (doctor: IDoctor): IDoctorsAction => {
    return doctor.id ? dispatch(saveDoctor(doctor)) : dispatch(addDoctor(doctor))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchDoctor: (id: string): IDoctor => _.find(state.doctors.doctors, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DoctorProfileScreen)
