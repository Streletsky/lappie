import {
  View,
  Dimensions,
  Modal,
  Animated,
  Image,
  Text,
  Easing,
  StyleSheet,
  Linking,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native'
import React, { Component, ReactElement } from 'react'
import { connect } from 'react-redux'
import { BlurView } from 'expo-blur'
import { NavigationStackOptions } from 'react-navigation-stack'
import { ListItem, Button } from 'react-native-elements'
import MapView, { Region, PROVIDER_GOOGLE } from 'react-native-maps'
import moment from 'moment'
import { Dispatch } from 'redux'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import SafeAreaView from 'react-native-safe-area-view'

import { initialWindowSafeAreaInsets } from 'react-native-safe-area-context'
import AppIcons from '../../components/AppIcons'
import IDoctor from '../../types/Doctor'
import IAppState from '../../types/AppState'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import { IUserLocation, IUserMarkersWithLocation } from '../../types/PlacesResult'
import constants from '../../utilities/constants'
import { saveLocation, ILocationAction, saveMarkers, addDoctor, IDoctorsAction, updateMarker } from '../../actions/actionCreators'
import featureChecker from '../../utilities/featureChecker'
import IMapPlace from '../../types/MapPlace'
import BottomModal from '../../components/BottomModal'
import DoctorEditForm from '../../components/DoctorEditForm'
import MapService from '../../services/MapService'
import MapMarker from '../../components/MapMarker'

const styles = StyleSheet.create({
  mapBox: {
    paddingHorizontal: featureChecker.isPortrait() ? 16 : 0,
    marginTop: 15,
  },
  smallMap: {
    height: 200,
    width: '100%',
    marginBottom: 20,
    borderRadius: 9,
    position: 'relative',
  },
  blurLabel: {
    ...R.styles.borderRadiuses.large,
    width: 260,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    position: 'absolute',
    backgroundColor: '#D0D9BA',
    zIndex: 10,
    flexDirection: 'row',
    left: '50%',
    transform: [{ translateX: -130 }],
    top: 70,
    flexWrap: 'wrap',
  },
  blurLabelImage: {
    width: 40,
    height: 50,
    alignSelf: 'center',
    marginRight: 6,
  },
  closeButton: {
    position: 'absolute',
    top: 44,
    right: 16,
  },
  doctorLinkCard: {
    ...R.styles.borderRadiuses.large,
    padding: 0,
    minHeight: 70,
    paddingLeft: 20,
    paddingRight: 15,
    marginBottom: 10,
  },
  phoneCallButton: {
    ...R.styles.defaultBorder,
    borderColor: R.colors.black,
    height: 44,
    width: 44,
    borderRadius: 22,
    marginRight: 38,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 222,
  },
  calloutView: {
    ...R.styles.borderRadiuses.large,
    backgroundColor: R.colors.white,
    paddingLeft: 13,
    paddingRight: 20,
    paddingVertical: 20,
    width: '90%',
  },
})

interface IStateFromProps {
  doctors: IDoctor[]
  savedLocation: IUserLocation
  savedMarkers: IUserMarkersWithLocation
}

interface IDispatchFromProps {
  saveUserLocation(location: IUserLocation): void
  saveLocationMarkers(markers: IUserMarkersWithLocation): void
  handleSaveDoctor(doctor: IDoctor): void
  updateMarkerDetails(marker: IMapPlace): void
}

interface IComponentState {
  region?: Region
  initialRegion?: Region
  isMapFullScreen: boolean
  horizontalOffset: Animated.Value
  topOffset: Animated.Value
  bottomOffset: Animated.Value
  initialTopOffset: number
  initialBottomOffset: number
  opacity: Animated.Value
  mapOpacity: Animated.Value
  modalFade: Animated.Value
  initialHorizontalOffset: number
  doctor: IDoctor
  isDoctorModalOpen: boolean
  isMarkerModalOpen: boolean
  selectedMarkedIndex: number
  selectedMarker?: IMapPlace
}

const AnimatedMap = Animated.createAnimatedComponent(MapView)
const AnimatedView = Animated.createAnimatedComponent(View)

const screen = Dimensions.get('window')
const ASPECT_RATIO = screen.width / screen.height
const LATITUDE_DELTA = 0.028
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

class DoctorsScreen extends Component<IStateFromProps & IDispatchFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.doctors.doctors(),
      headerShown: false,
    }
  }

  private emptyDoctor: IDoctor

  private smallMapContainer?: View

  private mapService: MapService

  public constructor(props: IStateFromProps & IDispatchFromProps & INavigatableComponentProps) {
    super(props)

    this.mapService = new MapService()

    this.emptyDoctor = {
      id: '',
      address: undefined,
      clinic: undefined,
      name: '',
      notes: undefined,
      phone: undefined,
      modifiedAt: new Date(),
      createdAt: new Date(),
    }

    this.state = {
      region: undefined,
      initialRegion: undefined,
      isMapFullScreen: false,
      topOffset: new Animated.Value(0),
      bottomOffset: new Animated.Value(0),
      horizontalOffset: new Animated.Value(0),
      initialBottomOffset: 0,
      initialTopOffset: 0,
      initialHorizontalOffset: 0,
      opacity: new Animated.Value(1),
      mapOpacity: new Animated.Value(0),
      modalFade: new Animated.Value(1),
      doctor: this.emptyDoctor,
      isDoctorModalOpen: false,
      isMarkerModalOpen: false,
      selectedMarkedIndex: -1,
      selectedMarker: undefined,
    }
  }

  private expandMapAnimation = (): void => {
    const { opacity, topOffset, bottomOffset, mapOpacity, horizontalOffset, modalFade } = this.state

    Animated.parallel([
      Animated.timing(modalFade, {
        toValue: 1,
        duration: 200,
        easing: Easing.bezier(0.68, 0.01, 0.31, 0.3),
      }),
      Animated.timing(mapOpacity, {
        toValue: 1,
        duration: 300,
        easing: Easing.bezier(0.68, 0.01, 0.31, 0.3),
      }),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 300,
        easing: Easing.bezier(0.87, 0.03, 0.36, 0.92),
      }),
      Animated.timing(topOffset, {
        toValue: 0,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
      Animated.timing(bottomOffset, {
        toValue: 0,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
      Animated.timing(horizontalOffset, {
        toValue: 0,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
    ]).start((): void => {
      this.getLocationMarkers()
    })
  }

  private collapseMap = (callback?: Function): void => {
    const {
      opacity,
      topOffset,
      bottomOffset,
      mapOpacity,
      horizontalOffset,
      initialHorizontalOffset,
      initialTopOffset,
      initialBottomOffset,
      modalFade,
      initialRegion,
    } = this.state

    Animated.parallel([
      Animated.timing(modalFade, {
        toValue: 0,
        duration: 350,
        easing: Easing.bezier(0.68, 0.01, 0.31, 0.3),
      }),
      Animated.timing(mapOpacity, {
        toValue: 0,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),

      Animated.timing(opacity, {
        toValue: 1,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),

      Animated.timing(topOffset, {
        toValue: initialTopOffset,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
      Animated.timing(bottomOffset, {
        toValue: initialBottomOffset,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
      Animated.timing(horizontalOffset, {
        toValue: initialHorizontalOffset,
        duration: 300,
        easing: Easing.bezier(0.64, 0.04, 0.35, 1),
      }),
    ]).start((): void => {
      this.setState({ isMapFullScreen: false, selectedMarkedIndex: -1, selectedMarker: undefined, region: initialRegion })

      if (callback) {
        callback()
      }
    })
  }

  public expandMap = (): void => {
    const { region } = this.state

    if (!region) {
      return
    }

    this.setState({ isMapFullScreen: true })

    if (!this.smallMapContainer) return

    this.smallMapContainer.measure((fx, fy, width, height, px, py): void => {
      this.setState({
        horizontalOffset: new Animated.Value(px),
        topOffset: new Animated.Value(py),
        bottomOffset: new Animated.Value(screen.height - height - py),
        initialTopOffset: py,
        initialBottomOffset: screen.height - height - py,
        initialHorizontalOffset: px,
      })

      this.expandMapAnimation()
    })
  }

  private getLocationMarkers = (): void => {
    const { initialRegion } = this.state
    const { saveLocationMarkers, savedMarkers } = this.props

    if (!initialRegion) return

    let diffDays = 0
    let distanceFromPreviousLocation = 0
    if (savedMarkers && savedMarkers.userLocation) {
      const currentDateTime = new Date()

      const a = moment(currentDateTime)
      const b = moment(savedMarkers.userLocation.datestamp)
      diffDays = a.diff(b, 'days')

      distanceFromPreviousLocation = this.mapService.coordinatesDistance(
        savedMarkers.userLocation.location.lat,
        savedMarkers.userLocation.location.lng,
        initialRegion.latitude,
        initialRegion.longitude,
        'K',
      )
    }

    if (diffDays > constants.userMarkersExpireInDays || distanceFromPreviousLocation > constants.userLocationKmDeltaForCaching) {
      this.mapService
        .fetchNearbyDoctors(initialRegion.latitude, initialRegion.longitude)
        .then((markers): void => {
          saveLocationMarkers({
            markers,
            userLocation: {
              datestamp: new Date(),
              location: {
                lat: initialRegion.latitude,
                lng: initialRegion.longitude,
              },
            },
          })
        })
        .catch((error): void => console.log(error))
    }
  }

  private saveDoctor = (): void => {
    const { handleSaveDoctor } = this.props
    const { doctor } = this.state

    doctor.id = uuid()
    handleSaveDoctor(doctor)

    this.setState({ doctor: this.emptyDoctor, isDoctorModalOpen: false })
  }

  private closeDoctorModal = (): void => {
    this.setState({ doctor: this.emptyDoctor, isDoctorModalOpen: false })
  }

  private showDoctorModal = (marker: IMapPlace | undefined = undefined): void => {
    const { doctors } = this.props

    this.setState({ isDoctorModalOpen: true })

    if (marker) {
      this.setState({
        doctor: {
          id: '',
          address: marker.address,
          clinic: marker.title,
          name: `Doctor ${doctors.length + 1}`,
          notes: undefined,
          phone: marker.phone,
          modifiedAt: new Date(),
          createdAt: new Date(),
        },
      })
    }
  }

  private addDoctorFromMap = (marker: IMapPlace | undefined = undefined): void => {
    this.collapseMap((): void => {
      this.setState({ isMarkerModalOpen: false })

      this.showDoctorModal(marker)
    })
  }

  private makeCall = (phone?: string): void => {
    if (phone) {
      Linking.openURL(`tel:${phone}`)
    }
  }

  private onMarkerClick = (marker: IMapPlace, index: number): void => {
    const { updateMarkerDetails } = this.props

    this.setState({
      region: {
        latitude: marker.coordinate.latitude,
        longitude: marker.coordinate.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      selectedMarkedIndex: index,
    })

    const a = moment()
    const b = moment(marker.modifiedAt)
    const diffDays = a.diff(b, 'days')

    if (diffDays <= constants.userMarkersExpireInDays && marker.detailsLoaded) {
      this.setState({ isMarkerModalOpen: true, selectedMarker: marker })
      return
    }

    this.mapService.fetchPlaceDetails(marker.id).then((place): void => {
      marker.address = place.formatted_address
      marker.phone = place.formatted_phone_number
      marker.modifiedAt = new Date()
      marker.detailsLoaded = true

      updateMarkerDetails(marker)

      this.setState({
        region: {
          latitude: marker.coordinate.latitude,
          longitude: marker.coordinate.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
        isMarkerModalOpen: true,
        selectedMarker: marker,
      })
    })
  }

  public componentDidMount = (): void => {
    const { savedLocation, saveUserLocation } = this.props

    const currentDateTime = new Date()

    const a = moment(currentDateTime)
    const b = moment(savedLocation.datestamp)
    const diffMinutes = a.diff(b, 'minutes')

    if (diffMinutes > constants.userLocationExpiresInMinutes) {
      this.mapService
        .getLocationAsync()
        .then((location): void => {
          this.setState({
            region: {
              latitude: location.coords.latitude,
              longitude: location.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            },
            initialRegion: {
              latitude: location.coords.latitude,
              longitude: location.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            },
          })

          saveUserLocation({
            datestamp: currentDateTime,
            location: {
              lat: location.coords.latitude,
              lng: location.coords.longitude,
            },
          })
        })
        .catch((error): void => console.log(error))
    } else {
      this.setState({
        region: {
          latitude: savedLocation.location.lat,
          longitude: savedLocation.location.lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
        initialRegion: {
          latitude: savedLocation.location.lat,
          longitude: savedLocation.location.lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
      })
    }
  }

  public render(): ReactElement {
    const { doctors, navigation, savedMarkers } = this.props
    const {
      region,
      initialRegion,
      isMapFullScreen,
      opacity,
      modalFade,
      horizontalOffset,
      bottomOffset,
      topOffset,
      mapOpacity,
      doctor,
      isDoctorModalOpen,
      isMarkerModalOpen,
      selectedMarkedIndex,
      selectedMarker,
    } = this.state

    return (
      <View style={R.styles.rootView}>
        <BottomModal
          onSave={this.saveDoctor}
          title={R.strings.doctors.addDoctor()}
          onCancel={this.closeDoctorModal}
          saveButtonEnabled={(doctor.name && doctor.name.length > 0 && doctor.phone && doctor.phone.length > 0) === true}
          visible={isDoctorModalOpen}
        >
          <View>
            <DoctorEditForm doctor={doctor} setParentState={(data): void => this.setState({ doctor: data })} />
          </View>
        </BottomModal>
        <SafeAreaView style={[R.styles.safeArea, { paddingTop: initialWindowSafeAreaInsets ? initialWindowSafeAreaInsets.top : 0 }]}>
          <ScrollContainer>
            <View style={R.styles.pushContentOpposite}>
              <View>
                <View style={[R.styles.screenHeaderWrapper, !(doctors && doctors.length > 0) && { marginBottom: 0 }]}>
                  <Text style={R.styles.fonts.hero}>{R.strings.doctors.doctors()}</Text>
                  <TouchableOpacity onPress={(): void => this.showDoctorModal()}>
                    <AppIcons name="plus" size={24} color={R.colors.amber} />
                  </TouchableOpacity>
                </View>
                {!(doctors && doctors.length > 0) && (
                  <View style={{ paddingHorizontal: 35, marginTop: 25 }}>
                    <Text style={R.styles.fonts.title}>{R.strings.doctors.youDontHaveDoctors()}</Text>
                  </View>
                )}
                <View style={R.styles.defaultContainer}>
                  {doctors.map(
                    (d): ReactElement => (
                      <View key={d.id}>
                        <TouchableOpacity onPress={(): boolean => navigation.navigate('DoctorProfileScreen', { doctorId: d.id })}>
                          <ListItem
                            title={d.name}
                            subtitle={d.clinic || undefined}
                            titleStyle={R.styles.fonts.body}
                            subtitleStyle={[R.styles.fonts.callout, { color: R.colors.porpoise }]}
                            containerStyle={styles.doctorLinkCard}
                            rightElement={
                              <View style={R.styles.rowCenter}>
                                <TouchableOpacity style={styles.phoneCallButton} onPress={(): void => this.makeCall(d.phone)}>
                                  <AppIcons name="phone" size={18} color={R.colors.black} />
                                </TouchableOpacity>
                                <AppIcons name="chevron" size={16} color={R.colors.darkCloud} />
                              </View>
                            }
                          />
                        </TouchableOpacity>
                      </View>
                    ),
                  )}
                </View>
              </View>
              <View style={styles.mapBox}>
                <TouchableOpacity onPress={this.expandMap}>
                  <View
                    style={styles.smallMap}
                    collapsable={false}
                    ref={(container): void => {
                      this.smallMapContainer = container || undefined
                    }}
                  >
                    <BlurView tint="light" intensity={70} style={styles.blurLabel}>
                      <Image source={R.images.mapMarker} style={styles.blurLabelImage} />
                      <Text style={[R.styles.fonts.headline, { alignSelf: 'center' }]}>{R.strings.doctors.browseDoctors()}</Text>
                    </BlurView>
                    <MapView
                      style={{ flex: 1 }}
                      showsUserLocation={false}
                      region={region}
                      provider={PROVIDER_GOOGLE}
                      zoomEnabled={false}
                      rotateEnabled={false}
                      scrollEnabled={false}
                      pitchEnabled={false}
                      onPress={this.expandMap}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollContainer>
          <Modal
            supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
            animationType="none"
            visible={isMapFullScreen}
            presentationStyle="overFullScreen"
            transparent
          >
            <AnimatedView style={{ flex: 1, opacity: modalFade }}>
              <AnimatedView
                style={{
                  left: horizontalOffset,
                  right: horizontalOffset,
                  top: topOffset,
                  bottom: bottomOffset,
                  position: 'absolute',
                  backgroundColor: R.colors.periwinkle,
                  opacity,
                }}
              />
              <AnimatedMap
                style={{
                  height: '100%',
                  width: '100%',
                  opacity: mapOpacity,
                  position: 'absolute',
                }}
                showsUserLocation
                region={region}
                initialRegion={initialRegion}
                provider={PROVIDER_GOOGLE}
                onPress={(): void => this.setState({ selectedMarkedIndex: -1 })}
              >
                {savedMarkers.markers.map(
                  (marker, index): ReactElement => (
                    <MapMarker
                      key={marker.id}
                      marker={marker}
                      onMarkerClick={(m): void => this.onMarkerClick(m, index)}
                      isSelected={selectedMarkedIndex === index}
                    />
                  ),
                )}
              </AnimatedMap>
              <AnimatedView style={[styles.closeButton, { opacity: mapOpacity }]}>
                <Button
                  title={R.strings.ui.close()}
                  buttonStyle={R.styles.buttons.smallButton}
                  titleStyle={R.styles.buttons.smallButtonTitle}
                  onPress={(): void => this.collapseMap()}
                />
              </AnimatedView>
            </AnimatedView>
            <Modal
              supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
              animationType="fade"
              visible={isMarkerModalOpen}
              presentationStyle="overFullScreen"
              transparent
            >
              {selectedMarker && (
                <TouchableWithoutFeedback
                  onPress={(): void => this.setState({ isMarkerModalOpen: false, selectedMarkedIndex: -1, selectedMarker: undefined })}
                >
                  <View style={[R.styles.rowCenter, { flex: 1, backgroundColor: R.colors.transparent }]}>
                    <View style={styles.calloutView}>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={[R.styles.fonts.headline, { maxWidth: '80%' }]}>{selectedMarker.title}</Text>
                        <TouchableOpacity onPress={(): void => this.addDoctorFromMap(selectedMarker)}>
                          <AppIcons name="plus" size={24} color={R.colors.amber} />
                        </TouchableOpacity>
                      </View>
                      {selectedMarker.rating && (
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 11 }}>
                          <Image source={R.images.smallStar} style={{ marginRight: 6 }} />
                          <Text style={R.styles.fonts.star}>{selectedMarker.rating}</Text>
                        </View>
                      )}
                      {selectedMarker.phone && (
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 27 }}>
                          <View style={[styles.phoneCallButton, { marginRight: 8 }]}>
                            <TouchableOpacity onPress={(): void => this.makeCall(selectedMarker.phone)}>
                              <AppIcons name="phone" size={18} color={R.colors.black} />
                            </TouchableOpacity>
                          </View>
                          <View>
                            <Text style={R.styles.fonts.button}>{selectedMarker.phone}</Text>
                            <Text style={R.styles.fonts.captionSmall}>{selectedMarker.address}</Text>
                          </View>
                        </View>
                      )}
                      {!selectedMarker.phone && (
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 27 }}>
                          <View>
                            <Text style={R.styles.fonts.captionSmall}>{selectedMarker.address}</Text>
                          </View>
                        </View>
                      )}
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              )}
            </Modal>
          </Modal>
        </SafeAreaView>
      </View>
    )
  }
}

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    doctors: state.doctors.doctors,
    savedLocation: state.location.userLocation,
    savedMarkers: state.location.savedMarkers,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  saveUserLocation: (location: IUserLocation): ILocationAction => dispatch(saveLocation(location)),
  saveLocationMarkers: (markers: IUserMarkersWithLocation): ILocationAction => dispatch(saveMarkers(markers)),
  updateMarkerDetails: (marker: IMapPlace): ILocationAction => dispatch(updateMarker(marker)),
  handleSaveDoctor: (doctor: IDoctor): IDoctorsAction => {
    return dispatch(addDoctor(doctor))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DoctorsScreen)
