import React, { Component, ReactElement } from 'react'
import { Text, StyleSheet, View, Image } from 'react-native'
import { Divider } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
  image: {
    height: 300,
    left: 0,
    right: 0,
    alignSelf: 'center',
  },
  title: {
    ...R.styles.fonts.hero,
    marginVertical: 10,
    textAlign: 'center',
  },
  header: {
    ...R.styles.fonts.title,
    marginTop: 30,
    marginBottom: 10,
  },
  paragrpah: {
    ...R.styles.fonts.body,
    marginVertical: 5,
  },
  version: {
    ...R.styles.fonts.caption,
    marginVertical: 5,
  },
  list: {
    paddingLeft: 8,
    ...R.styles.fonts.description,
    marginVertical: 5,
  },
})

export default class HomeScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.about(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <Image resizeMode="contain" style={styles.image} source={R.images.aboutImage} />
          <View style={styles.container}>
            <Text style={styles.title}>About Puff</Text>
            <Text style={styles.paragrpah}>Puff is a smart helper for you to track your pet’s health and care, easy and simple.</Text>
            <Text style={styles.paragrpah}>
              Keep medical records, track treatment progress and plan care, and Puff will make sure you don&apos;t forget anything.
            </Text>
            <Text style={styles.paragrpah}>
              Puff can remind about expiring dates and upcoming plans, as well as suggest useful little things just for your pet.
            </Text>
            <Text style={styles.header}>Pet health & Care</Text>
            <Text style={styles.paragrpah}>
              Add your pet and information about its health will always be at hand. Puff has everything to help you with your care:
            </Text>
            <Text style={styles.list}>📒 Add and store medical information - vaccinations, parasite defense, medical history</Text>
            <Text style={styles.list}>💊 Add and track treatment plans, fully customizable</Text>
            <Text style={styles.list}>💗 Track your pet’s growth and development with weight tracking</Text>
            <Text style={styles.list}>🩺 Quickly find a veterinarian nearby and store his contact details for quick access</Text>
            <Divider style={[R.styles.divider, { marginTop: 17 }]} />
            <Text style={styles.header}>Smart Assistant</Text>
            <Text style={styles.paragrpah}>
              Puff keeps track of dates and times, and you won&apos;t miss a thing, no complicated setup needed:
            </Text>
            <Text style={styles.list}>✅ Treatment Reminders</Text>
            <Text style={styles.list}>✅ Scheduled Event Reminders</Text>
            <Text style={styles.list}>✅ Notifications of imminent expiration of vaccination and parasite protection</Text>
            <Text style={styles.list}>✅ Personalized Pet Care Suggestions and Events</Text>
            <Divider style={[R.styles.divider, { marginTop: 17 }]} />
            <Text style={styles.header}>More to Go</Text>
            <Text style={styles.paragrpah}>More features are coming soon:</Text>
            <Text style={styles.list}>🧫 Allergies</Text>
            <Text style={styles.list}>🛁 Care Procedures</Text>
            <Text style={styles.list}>🥘 Feeding</Text>
            <Divider style={[R.styles.divider, { marginVertical: 17 }]} />

            <Text style={styles.paragrpah}>
              The developers of this app are two initiative cats who would like all animals to have good care and love.
            </Text>
            <Text style={styles.paragrpah}>We are open to your feedback and suggestions: e-mail us at help.puff@gmail.com</Text>
            <Text style={styles.version}>App Version: 1.0</Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
