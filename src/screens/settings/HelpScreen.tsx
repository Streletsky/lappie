import React, { Component, ReactElement } from 'react'
import { View, Text, SectionList } from 'react-native'
import { Divider } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import R from '../../res/R'
import LinkCard from '../../components/LinkCard'

const navigatableScreens = [
  {
    title: R.strings.articles.helpArticles.sectionTitles.basics(),
    index: 0,
    data: [
      {
        key: 1,
        title: R.strings.articles.helpArticles.titles.vaccination(),
        route: 'VacinationArticleScreen',
        arrayLength: 6,
      },
      {
        key: 2,
        title: R.strings.articles.helpArticles.titles.deworming(),
        route: 'DewormingArticleScreen',
        arrayLength: 6,
      },
      {
        key: 3,
        title: R.strings.articles.helpArticles.titles.treatmentPlans(),
        route: 'TreatmentArticleScreen',
        arrayLength: 6,
      },
      {
        key: 4,
        title: R.strings.articles.helpArticles.titles.plannedVisits(),
        route: 'PlannedVisitsArticleScreen',
        arrayLength: 6,
      },
      {
        key: 5,
        title: R.strings.articles.helpArticles.titles.weight(),
        route: 'WeightArticleScreen',
        arrayLength: 6,
      },
      {
        key: 6,
        title: R.strings.articles.helpArticles.titles.doctors(),
        route: 'DoctorsArticleScreen',
        arrayLength: 6,
      },
    ],
  },
  {
    title: R.strings.articles.helpArticles.sectionTitles.guide(),
    index: 1,
    data: [
      {
        key: 1,
        title: R.strings.articles.helpArticles.titles.userAccount(),
        route: 'UserAccountArticleScreen',
        arrayLength: 1,
      },
    ],
  },
]

export default class HelpScreen extends Component<INavigatableComponentProps> {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  private renderSectionHeader = ({ section }): ReactElement => (
    <View style={[R.styles.listSectionContainer, { borderTopWidth: 0 }]}>
      <Text style={R.styles.listSectionTitle}>{section.title}</Text>
    </View>
  )

  private renderItem = ({ item }): ReactElement => {
    const { navigation } = this.props

    return (
      <View key={item.key}>
        <LinkCard
          title={item.title}
          onPress={(): boolean => navigation.navigate(item.route)}
          regularFont
          noRoundingBottom={item.key !== item.arrayLength}
          noRoundingTop={item.key !== 1}
          containerStyle={{ marginTop: 0 }}
        />
        {item.key !== item.arrayLength && <Divider style={R.styles.divider} />}
      </View>
    )
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={R.styles.safeArea}>
        <View style={R.styles.defaultContainer}>
          <SectionList renderSectionHeader={this.renderSectionHeader} renderItem={this.renderItem} sections={navigatableScreens} />
        </View>
      </SafeAreaView>
    )
  }
}
