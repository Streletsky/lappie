import React, { Component, ReactElement } from 'react'
import { Text, View, StyleSheet, AsyncStorage, Modal, Image } from 'react-native'
import { Button, Divider } from 'react-native-elements'
import { connect } from 'react-redux'
import { AnyAction } from 'redux'
import { NavigationStackOptions } from 'react-navigation-stack'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { ThunkDispatch } from 'redux-thunk'
import { LinearGradient } from 'expo-linear-gradient'
import SafeAreaView from 'react-native-safe-area-view'
import { reloadAsync } from 'expo-updates'

import { initialWindowSafeAreaInsets } from 'react-native-safe-area-context'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import SectionRadioButton from '../../components/SectionRadioButton'
import { setWeightUnitSetting, ISettingsAction, setLanguageSetting } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import constants from '../../utilities/constants'
import AppIcons from '../../components/AppIcons'
import LinkCard from '../../components/LinkCard'
import SignupForm from '../../components/SignupForm'
import BetaPlaceholder from '../../components/BetaPlaceholder'
import FirebaseAuthService from '../../services/FirebaseAuthService'

const styles = StyleSheet.create({
  premiumSection: {
    ...R.styles.borderRadiuses.large,
    padding: 17,
    justifyContent: 'center',
    alignContent: 'center',
    marginVertical: 10,
    marginHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
  image: {
    marginTop: 16,
    marginBottom: 0,
    alignSelf: 'center',
  },
  buyButtonStyle: {
    borderRadius: 22,
    backgroundColor: R.colors.white,
    width: '100%',
    height: 44,
  },
  buyButtonStyleTitle: {
    ...R.styles.fonts.button,
    color: R.colors.black,
  },
  authSection: {
    marginTop: 10,
    marginHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
  syncText: {
    color: R.colors.emerald,
    opacity: 0.8,
    marginRight: 5,
  },
})

interface IDispatchFromProps {
  handleWeightUnitSelection(index: number): void
  handleLanguageSelection(index: number): void
}

interface IStateFromProps {
  selectedWeightUnitIndex: number
  selectedLanguageIndex: number
}

interface IComponentState {
  isSignUpOpen: boolean
  isBetaUpOpen: boolean
}

class SettingsScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.settings(),
      headerShown: false,
    }
  }

  private authService: FirebaseAuthService

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    this.authService = new FirebaseAuthService()

    this.state = {
      isSignUpOpen: false,
      isBetaUpOpen: false,
    }
  }

  private setLanguage = (langIndex: number): void => {
    const { handleLanguageSelection } = this.props
    const langStr = constants.languages[langIndex]

    AsyncStorage.setItem('lang', langStr)
    handleLanguageSelection(langIndex)
    reloadAsync()
  }

  private setWeightUnit = (weightIndex: number): void => {
    const { handleWeightUnitSelection } = this.props

    handleWeightUnitSelection(weightIndex)
  }

  private renderListItemRightElement = (itemId: number): ReactElement => {
    if (itemId === 2) {
      const { selectedWeightUnitIndex } = this.props

      return (
        <SectionRadioButton
          handleSelection={this.setWeightUnit}
          selectedIndex={selectedWeightUnitIndex}
          buttons={constants.weightUoms}
          width={136}
        />
      )
    }

    if (itemId === 3) {
      const buttons = ['EN', 'RU']
      const { selectedLanguageIndex } = this.props

      return <SectionRadioButton handleSelection={this.setLanguage} selectedIndex={selectedLanguageIndex} buttons={buttons} width={136} />
    }

    return <AppIcons name="chevron" size={14} color={R.colors.fossil} />
  }

  public render(): ReactElement {
    const { navigation } = this.props
    const { isSignUpOpen, isBetaUpOpen } = this.state
    const user = new FirebaseAuthService().getCurrentUser()

    return (
      <SafeAreaView style={[R.styles.safeArea, { paddingTop: initialWindowSafeAreaInsets ? initialWindowSafeAreaInsets.top : 0 }]}>
        <ScrollContainer>
          <View style={R.styles.pushContentOpposite}>
            <View>
              <View style={R.styles.screenHeaderWrapper}>
                <Text style={R.styles.fonts.hero}>{R.strings.settings.settings()}</Text>
              </View>
              <View style={styles.authSection}>
                {(!user || user.isAnonymous) && (
                  <LinkCard
                    title={R.strings.user.inviteToSignup()}
                    onPress={(): void => this.setState({ isSignUpOpen: true })}
                    regularFont
                    containerStyle={{ backgroundColor: R.colors.honey }}
                    color={R.colors.white}
                  />
                )}
                {user && !user.isAnonymous && (
                  <View style={[R.styles.rowCenter, R.styles.pushContentOpposite, { paddingHorizontal: 20 }]}>
                    <Text style={R.styles.fonts.headline}>{user.email}</Text>
                    <TouchableOpacity onPress={this.authService.signOut}>
                      <AppIcons name="exit" size={22} color={R.colors.black} />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
              <LinearGradient
                style={styles.premiumSection}
                colors={[...R.colors.gradient.fill.mauve]}
                start={R.colors.gradient.direction.diagonalReverse.start}
                end={R.colors.gradient.direction.diagonalReverse.end}
              >
                <Text style={[R.styles.fonts.hero, { textAlign: 'center', color: R.colors.white }]}>
                  {R.strings.settings.freeVersion()}
                </Text>
                <Text style={[R.styles.fonts.callout, { textAlign: 'center', color: R.colors.white, paddingHorizontal: 30 }]}>
                  {R.strings.settings.freeFeatureDesc()}
                </Text>
                <Image resizeMode="contain" style={styles.image} source={R.images.settingsBetaImage} />
                <View>
                  <Button
                    title={R.strings.settings.buyButtonTitle()}
                    buttonStyle={styles.buyButtonStyle}
                    titleStyle={styles.buyButtonStyleTitle}
                    onPress={(): void => this.setState({ isBetaUpOpen: true })}
                  />
                </View>
              </LinearGradient>
              <View style={R.styles.defaultContainer}>
                <LinkCard
                  title={R.strings.settings.aboutApp()}
                  onPress={(): boolean => navigation.navigate('AboutScreen')}
                  rightElement={this.renderListItemRightElement(1)}
                  noRoundingBottom
                  regularFont
                  containerStyle={{ marginTop: 0 }}
                />
                <Divider style={R.styles.divider} />
                <LinkCard
                  title={R.strings.settings.weightUnit()}
                  onPress={(): void => {}}
                  rightElement={this.renderListItemRightElement(2)}
                  noRounding
                  regularFont
                  containerStyle={{ marginTop: 0 }}
                />
                <Divider style={R.styles.divider} />
                <LinkCard
                  title={R.strings.settings.language()}
                  onPress={(): void => {}}
                  rightElement={this.renderListItemRightElement(3)}
                  noRounding
                  regularFont
                  containerStyle={{ marginTop: 0 }}
                />
                <Divider style={R.styles.divider} />
                <LinkCard
                  title={R.strings.settings.help()}
                  onPress={(): boolean => navigation.navigate('HelpScreen')}
                  rightElement={this.renderListItemRightElement(4)}
                  noRounding
                  regularFont
                  containerStyle={{ marginTop: 0 }}
                />
                <Divider style={R.styles.divider} />
                <LinkCard
                  title={R.strings.settings.legal()}
                  onPress={(): boolean => navigation.navigate('LegalScreen')}
                  rightElement={this.renderListItemRightElement(5)}
                  noRoundingTop
                  regularFont
                  containerStyle={{ marginTop: 0 }}
                />
              </View>
            </View>
            <View style={[R.styles.bottomButtonWrapper]}>
              <Button
                title={R.strings.settings.contactButtonTitle()}
                type="outline"
                buttonStyle={R.styles.buttons.secondaryButton}
                titleStyle={R.styles.buttons.secondaryButtonTitle}
              />
            </View>
          </View>
        </ScrollContainer>
        <Modal
          supportedOrientations={['portrait', 'portrait-upside-down']}
          animationType="fade"
          visible={isSignUpOpen}
          presentationStyle="overFullScreen"
          transparent
        >
          <SignupForm closeAction={(): void => this.setState({ isSignUpOpen: false })} />
        </Modal>
        <Modal
          supportedOrientations={['portrait', 'portrait-upside-down']}
          animationType="fade"
          visible={isBetaUpOpen}
          presentationStyle="overFullScreen"
          transparent
        >
          <BetaPlaceholder closeAction={(): void => this.setState({ isBetaUpOpen: false })} />
        </Modal>
      </SafeAreaView>
    )
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, AnyAction>): IDispatchFromProps => ({
  handleWeightUnitSelection: (index: number): ISettingsAction => dispatch(setWeightUnitSetting(index)),
  handleLanguageSelection: (index: number): ISettingsAction => dispatch(setLanguageSetting(index)),
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    selectedWeightUnitIndex: state.settings.selectedWeightUnitIndex,
    selectedLanguageIndex: state.settings.selectedLanguageIndex,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsScreen)
