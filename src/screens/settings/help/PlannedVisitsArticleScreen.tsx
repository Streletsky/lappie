import React, { Component, ReactElement } from 'react'
import { View, Text } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../../components/AppIcons'
import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

export default class PlannedVisitsArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <Text style={{ fontSize: 64, textAlign: 'center' }}>📒</Text>
            <Text style={R.styles.articleTitle}>Planned Visits & Medical History</Text>
            <Text style={R.styles.articleParagraph}>
              Plan your visits to the clinic or to a specialist, and also store any medical history and manipulations here.
            </Text>
            <Text style={R.styles.articleParagraph}>
              Click &nbsp;
              <AppIcons name="plus" size={17} color={R.colors.amber} style={{ textAlign: 'center' }} />
              &nbsp; to add a new entry. Based on the information entered, Puff will decide that this is a planned procedure or record for a
              medical history.
            </Text>
            <Text style={R.styles.articleParagraph}>
              You can edit or delete a record on the page of this record using Edit &nbsp;
              <AppIcons name="edit" size={17} color={R.colors.amber} style={{ textAlign: 'center' }} /> &nbsp;and Delete&nbsp;
              <AppIcons name="trash" size={17} color={R.colors.scarlet} style={{ textAlign: 'center' }} />.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
