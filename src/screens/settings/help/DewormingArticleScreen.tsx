import React, { Component, ReactElement } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../../components/AppIcons'
import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

const styles = StyleSheet.create({
  imageContainer: {
    padding: 20,
  },
  image: {
    flex: 1,
    height: 250,
    alignSelf: 'center',
  },
})

export default class DewormingArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <AppIcons name="deworm" size={60} color={R.colors.cedar} style={{ textAlign: 'center' }} />
            <Text style={R.styles.articleTitle}>Parasite Defense</Text>
            <Text style={R.styles.articleParagraph}>
              Add all existing ones and plan a new protection against parasites, and Puff will remind you of the time and events.
            </Text>
            <Text style={R.styles.articleParagraph}>
              Click &nbsp;
              <AppIcons name="plus" size={17} color={R.colors.cedar} style={{ textAlign: 'center' }} />
              &nbsp; to add a new entry. Based on the information entered, Puff will decide that this is a planned procedure or record for a
              medical history.
            </Text>
            <View style={styles.imageContainer}>
              <Image resizeMode="contain" style={styles.image} source={R.images.helpImageDewormActive} />
            </View>
            <Text style={R.styles.articleParagraph}>Puff supports pills and drops for parasite defense.</Text>
            <Text style={R.styles.articleParagraph}>
              You can edit or delete a record on the page of this record using Edit &nbsp;
              <AppIcons name="edit" size={17} color={R.colors.cedar} style={{ textAlign: 'center' }} /> &nbsp;and Delete&nbsp;
              <AppIcons name="trash" size={17} color={R.colors.scarlet} style={{ textAlign: 'center' }} />.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
