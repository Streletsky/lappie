import React, { Component, ReactElement } from 'react'
import { View, Text } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../../components/AppIcons'
import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

export default class WeightArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <AppIcons name="weight" size={60} color={R.colors.amber} style={{ textAlign: 'center' }} />
            <Text style={R.styles.articleTitle}>Weight</Text>
            <Text style={R.styles.articleParagraph}>
              Keep track of your pet’s growth with detailed weight statistics organized monthly or yearly.
            </Text>
            <Text style={R.styles.articleParagraph}>Add new entries from time to time to see progress. Puff will remind you of this!</Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
