import React, { Component, ReactElement } from 'react'
import { View, Text } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

export default class UserAccountArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <Text style={{ fontSize: 64, textAlign: 'center' }}>👤</Text>
            <Text style={R.styles.articleTitle}>Registration and User Account</Text>
            <Text style={R.styles.articleParagraph}>
              Register to use the application on different devices. Puff automatically synchronizes your information so that it is always
              available from all devices.
            </Text>
            <Text style={R.styles.articleParagraph}>
              You can use the application without registering, but then in the event of a malfunction on your device, the data may be lost.
              Registration takes very little time and helps to save your data.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
