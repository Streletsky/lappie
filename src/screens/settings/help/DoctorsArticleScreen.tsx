import React, { Component, ReactElement } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../../components/AppIcons'
import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

const styles = StyleSheet.create({
  imageContainer: {
    padding: 20,
  },
  image: {
    flex: 1,
    height: 250,
    alignSelf: 'center',
  },
})

export default class DoctorsArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <AppIcons name="doctors" size={60} color={R.colors.amber} style={{ textAlign: 'center' }} />
            <Text style={R.styles.articleTitle}>Doctors</Text>
            <Text style={R.styles.articleParagraph}>
              On this screen you can store contact details and any additional information about a veterinarian, clinic or other pet care
              professionals.
            </Text>
            <Text style={R.styles.articleParagraph}>The added specialist will be available for quick contact in one tap.</Text>
            <View style={styles.imageContainer}>
              <Image resizeMode="contain" style={styles.image} source={R.images.helpImageDoctorMap} />
            </View>
            <Text style={R.styles.articleParagraph}>If you have enabled geolocation, then you can find a clinic nearby.</Text>
            <Text style={R.styles.articleParagraph}>
              You can edit or delete a doctor record on his page using Edit &nbsp;
              <AppIcons name="edit" size={17} color={R.colors.amber} style={{ textAlign: 'center' }} /> &nbsp;and Delete&nbsp;
              <AppIcons name="trash" size={17} color={R.colors.scarlet} style={{ textAlign: 'center' }} />.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
