import React, { Component, ReactElement } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../../components/AppIcons'
import ScrollContainer from '../../../components/ScrollContainer'
import R from '../../../res/R'

const styles = StyleSheet.create({
  imageContainer: {
    padding: 20,
    marginBottom: 20,
  },
  image: {
    flex: 1,
    height: 320,
    alignSelf: 'center',
  },
  imageBig: {
    flex: 1,
    height: 700,
    alignSelf: 'center',
  },
})

export default class TreatmentArticleScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.help(),
    }
  }

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={R.styles.articleContainer}>
            <AppIcons name="treatment" size={60} color={R.colors.poison} style={{ textAlign: 'center' }} />
            <Text style={R.styles.articleTitle}>Treatment Plans</Text>
            <Text style={R.styles.articleParagraph}>
              Puff helps you keep track of the progress of your treatment and manage manipulations in time.
            </Text>
            <Text style={R.styles.articleParagraph}>
              Tap &nbsp;
              <AppIcons name="plus" size={17} color={R.colors.poison} style={{ textAlign: 'center' }} />
              &nbsp; to add a new record and specify the necessary information: the beginning of treatment, duration, frequency of
              manipulations or receptions, comments.
            </Text>
            <Text style={R.styles.articleParagraph}>
              After that, Puff will remind you of the current or upcoming treatment and show the progress made.
            </Text>
            <View style={styles.imageContainer}>
              <Image resizeMode="contain" style={styles.image} source={R.images.helpImageTreatment} />
            </View>
            <Text style={R.styles.articleParagraph}>
              If you specify treatment application frequency and time, Puff will remind you about them and suggest marking done.
            </Text>
            <View style={styles.imageContainer}>
              <Image resizeMode="contain" style={styles.imageBig} source={R.images.helpImageTreatmentFull} />
            </View>
            <Text style={R.styles.articleParagraph}>
              You can edit or delete a record on the page of this record using Edit &nbsp;
              <AppIcons name="edit" size={17} color={R.colors.poison} style={{ textAlign: 'center' }} /> &nbsp;and Delete&nbsp;
              <AppIcons name="trash" size={17} color={R.colors.scarlet} style={{ textAlign: 'center' }} />.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
