import React, { Component, ReactElement } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { Divider } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import SafeAreaView from 'react-native-safe-area-view'

import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
  title: {
    ...R.styles.fonts.title,
    marginVertical: 10,
  },
  header: {
    ...R.styles.fonts.headline,
    marginVertical: 5,
  },
  paragrpah: {
    ...R.styles.fonts.body,
    marginVertical: 5,
  },
})

export default class HomeScreen extends Component {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.settings.legal(),
    }
  }

  public state = {}

  public render(): ReactElement {
    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={styles.container}>
            <Text style={styles.title}>Privacy Policy</Text>
            <Text style={styles.paragrpah}>
              Your privacy is important to us. It is Puff&apos;s policy to respect your privacy regarding any information we may collect
              from you through our app, Puff.
            </Text>
            <Text style={styles.paragrpah}>
              We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful
              means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.
            </Text>
            <Text style={styles.paragrpah}>
              We only retain collected information for as long as necessary to provide you with your requested service. What data we store,
              we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorized access, disclosure,
              copying, use or modification.
            </Text>
            <Text style={styles.paragrpah}>
              We don’t share any personally identifying information publicly or with third-parties, except when required to by law.
            </Text>
            <Text style={styles.paragrpah}>
              Our app may link to external sites that are not operated by us. Please be aware that we have no control over the content and
              practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.
            </Text>
            <Text style={styles.paragrpah}>
              You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you
              with some of your desired services.
            </Text>
            <Text style={styles.paragrpah}>
              Your continued use of our app will be regarded as acceptance of our practices around privacy and personal information. If you
              have any questions about how we handle user data and personal information, feel free to contact us at help.puff@gmail.com.
            </Text>
            <Text style={styles.paragrpah}>This policy is effective as of 10 April 2020.</Text>
            <Divider style={[R.styles.divider, { marginTop: 17 }]} />
            <Text style={styles.title}>Terms of Service</Text>
            <Text style={styles.header}>1.Terms</Text>
            <Text style={styles.paragrpah}>
              By accessing our app, Puff, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and
              agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you
              are prohibited from using or accessing Puff. The materials contained in Puff are protected by applicable copyright and
              trademark law.
            </Text>
            <Text style={styles.header}>2.Use License</Text>
            <Text style={styles.paragrpah}>
              Permission is granted to temporarily download one copy of Puff per device for personal, non-commercial transitory viewing
              only. This is the grant of a license, not a transfer of title, and under this license you may not:
              {'\n'}- modify or copy the materials; use the materials for any commercial purpose, or for any public display (commercial or
              non-commercial);
              {'\n'}- attempt to decompile or reverse engineer any software contained in Puff;
              {'\n'}- remove any copyright or other proprietary notations from the materials;
              {'\n'}- or transfer the materials to another person or &quot;mirror&quot; the materials on any other server.
            </Text>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
