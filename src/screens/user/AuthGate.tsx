import React, { Component, ReactNode } from 'react'
import { View, StyleSheet } from 'react-native'
// import LottieView from 'lottie-react-native'

import R from '../../res/R'
import FirebaseAuthService from '../../services/FirebaseAuthService'

const styles = StyleSheet.create({
  authGateContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: R.colors.white,
  },
  animationWrapper: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
})

interface IComponentState {
  authenticating: boolean
}

// const showLandingPage = false

class AuthGate extends Component<{}, IComponentState> {
  // private animation: LottieView

  private authService: FirebaseAuthService

  public constructor(props: {}) {
    super(props)

    this.authService = new FirebaseAuthService()

    this.state = {
      authenticating: true,
    }
  }

  private authSubscription: Function = (): void => {}

  public componentDidMount = (): void => {
    // this.animation.play()

    this.authSubscription = this.authService.onAuthStateChanged((user): void => {
      if (user) {
        this.setState({ authenticating: false })
      } else {
        this.authService.userAnonymousLogin().catch((reason): void => {
          console.log(reason)
          this.setState({ authenticating: false })
        })
      }
    })
  }

  public componentWillUnmount = (): void => {
    this.authSubscription()
  }

  public render(): ReactNode {
    const { children } = this.props
    const { authenticating } = this.state

    return authenticating ? (
      <View style={styles.authGateContainer}>
        {/*         <LottieView          ref={(animation): void => {            this.animation = animation          }}          style={styles.animationWrapper}          source={R.images.pawsAnimation}          speed={2}        />        */}
      </View>
    ) : (
      children
    )
  }
}

export default AuthGate
