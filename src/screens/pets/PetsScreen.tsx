import React, { Component, ReactElement } from 'react'
import moment from 'moment'
import { Button } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import { View, Text, TouchableOpacity, Image, StyleSheet, Modal } from 'react-native'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import SafeAreaView from 'react-native-safe-area-view'

import { initialWindowSafeAreaInsets } from 'react-native-safe-area-context'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import IPet from '../../types/Pet'
import IPetNotification from '../../types/PetNotification'
import PetNotificationCard from '../../components/PetNotificationCard'
import PetNotificationsService from '../../services/PetNotificationsService'
import PetsListItem from '../../components/PetsListItem'
import R from '../../res/R'
import ScrollContainer from '../../components/ScrollContainer'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import PetCreationMenu from '../../components/PetCreationMenu'
import PetNotificationSubType from '../../types/PetNotificationSubType'
import VaccinationEditForm from '../../components/VaccinationEditForm'
import DewormingEditForm from '../../components/DewormingEditForm'
import IDeworming from '../../types/Deworming'
import IVaccination from '../../types/Vaccination'
import { savePet, IPetsAction } from '../../actions/actionCreators'
import ActionTrackerCard from '../../components/ActionTrackerCard'
import { formatTime } from '../../utilities/helpers'
import ITreatment from '../../types/Treatment'
import AdBanner from '../../components/AdBanner'
import IPetSuggestion from '../../types/PetSuggestion'
import PetSuggestionService from '../../services/PetSuggestionService'
import PetSuggestionCard from '../../components/PetSuggestionCard'
import PetSuggestionType from '../../types/PetSuggestionType'
import WeightEditForm from '../../components/WeightEditForm'
import IWeight from '../../types/Weight'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  pets: IPet[]
}

interface IComponentState {
  isPetCreationMenuOpen: boolean
  isDewormingModalOpen: boolean
  isVaccinationModalOpen: boolean
  isWeightModalOpen: boolean
  pet?: IPet
}

const styles = StyleSheet.create({
  topPaw: {
    position: 'absolute',
    bottom: 160,
    left: -30,
    zIndex: -11,
    transform: [{ rotateY: '180deg' }],
  },
  bottomPaw: {
    position: 'absolute',
    bottom: 23,
    left: 84,
    zIndex: -11,
  },
  notificationsRecap: {
    ...R.styles.borderRadiuses.large,
    ...R.styles.columnCenter,
    backgroundColor: R.colors.black,
    marginTop: 30,
    padding: 16,
  },
  smallStar: {
    width: 37,
    height: 37,
    resizeMode: 'cover',
    position: 'absolute',
    top: -23,
  },
})

class PetsScreen extends Component<IStateFromProps & IDispatchFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.pets.pets(),
      headerShown: false,
    }
  }

  private petNotificationService: PetNotificationsService

  private petSuggestionService: PetSuggestionService

  public constructor(props: IStateFromProps & IDispatchFromProps & INavigatableComponentProps) {
    super(props)

    this.petNotificationService = new PetNotificationsService()
    this.petSuggestionService = new PetSuggestionService()

    this.state = {
      isPetCreationMenuOpen: false,
      isDewormingModalOpen: false,
      isVaccinationModalOpen: false,
      isWeightModalOpen: false,
      pet: undefined,
    }
  }

  private getNotificationAction = (notification: IPetNotification): Function => {
    const { navigation } = this.props

    switch (notification.subType) {
      case PetNotificationSubType.PlannedVaccination:
      case PetNotificationSubType.VaccinationAboutToExpire:
        return (): void => {
          navigation.navigate('VaccinationsOverviewScreen', { petId: notification.pet.id })
        }
      case PetNotificationSubType.PlannedDeworming:
      case PetNotificationSubType.DewormingAboutToExpire:
        return (): void => {
          navigation.navigate('DewormingsOverviewScreen', { petId: notification.pet.id })
        }
      case PetNotificationSubType.PlannedTreatment:
        return (): void => {
          navigation.navigate('TreatmentsOverviewScreen', { petId: notification.pet.id })
        }
      case PetNotificationSubType.NoActiveVaccination:
        return (): void => this.openVaccinationModal(notification.pet)
      case PetNotificationSubType.NoActiveDeworming:
        return (): void => this.openDewormingModal(notification.pet)
      default:
        return (): void => {}
    }
  }

  private getSuggestionAction = (suggestion: IPetSuggestion): (() => void) => {
    switch (suggestion.type) {
      case PetSuggestionType.PlayWithPet:
      case PetSuggestionType.CheckWater:
        return (): void => {}
      case PetSuggestionType.AddFirstWeight:
        return (): void => this.openWeightModal(suggestion.pet)
      default:
        return (): void => {}
    }
  }

  private closeDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: false, pet: undefined })
  }

  private openDewormingModal = (pet: IPet): void => {
    this.setState({ pet, isDewormingModalOpen: true })
  }

  private saveDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    if (!pet) {
      return
    }

    const petToSave = {
      ...pet,
      dewormings: [...pet.dewormings, deworming],
    }

    handleSavePet(petToSave)
    this.closeDewormingModal()
  }

  private closeVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: false, pet: undefined })
  }

  private openVaccinationModal = (pet: IPet): void => {
    this.setState({ pet, isVaccinationModalOpen: true })
  }

  private saveVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    if (!pet) {
      return
    }

    const petToSave = {
      ...pet,
      vaccinations: [...pet.vaccinations, vaccination],
    }

    handleSavePet(petToSave)
    this.closeVaccinationModal()
  }

  private closeWeightModal = (): void => {
    this.setState({ isWeightModalOpen: false, pet: undefined })
  }

  private openWeightModal = (pet: IPet): void => {
    this.setState({ pet, isWeightModalOpen: true })
  }

  private saveWeight = (weight: IWeight): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    if (!pet) {
      return
    }

    const petToSave = {
      ...pet,
      weightEntries: [...pet.weightEntries, weight],
    }

    handleSavePet(petToSave)

    this.closeWeightModal()
  }

  private saveTreatment = (pet: IPet, treatment: ITreatment): void => {
    const { handleSavePet } = this.props

    pet.treatments = pet.treatments.map(
      (v): ITreatment => {
        if (v.id !== treatment.id) {
          return v
        }

        return {
          ...v,
          ...treatment,
        }
      },
    )

    handleSavePet(pet)
  }

  private isTreatmentDone = (date?: Date): boolean => {
    const checkedDate = moment(date)
    const today = moment(new Date())

    return (date && checkedDate.isSame(today, 'd')) === true
  }

  private markTreatmentDate = (pet: IPet, treatment: ITreatment, timeOfDayNumber: number): void => {
    switch (timeOfDayNumber) {
      case 1:
        treatment.timeOfDay1CheckedAt = treatment.timeOfDay1CheckedAt ? undefined : new Date()
        break
      case 2:
        treatment.timeOfDay2CheckedAt = treatment.timeOfDay2CheckedAt ? undefined : new Date()
        break
      case 3:
        treatment.timeOfDay3CheckedAt = treatment.timeOfDay3CheckedAt ? undefined : new Date()
        break
      default:
        break
    }

    this.saveTreatment(pet, treatment)
  }

  private getCustomNotificationContent = (notification: IPetNotification): ReactElement | null => {
    const treatment = notification.payload as ITreatment

    if (notification.subType !== PetNotificationSubType.OnGoingTreatment) return null

    return (
      <View>
        {(treatment.periodicity === 1 || treatment.periodicity === 2 || treatment.periodicity === 3) && treatment.timeOfDay1 && (
          <View style={{ marginTop: 7 }}>
            <ActionTrackerCard
              title={formatTime(treatment.timeOfDay1)}
              onPress={(): void => this.markTreatmentDate(notification.pet, treatment, 1)}
              isDone={this.isTreatmentDone(treatment.timeOfDay1CheckedAt)}
            />
          </View>
        )}
        {(treatment.periodicity === 1 || treatment.periodicity === 2) && treatment.timeOfDay2 && (
          <View>
            <ActionTrackerCard
              title={formatTime(treatment.timeOfDay2)}
              onPress={(): void => this.markTreatmentDate(notification.pet, treatment, 2)}
              isDone={this.isTreatmentDone(treatment.timeOfDay2CheckedAt)}
            />
          </View>
        )}
        {treatment.periodicity === 1 && treatment.timeOfDay3 && (
          <View>
            <ActionTrackerCard
              title={formatTime(treatment.timeOfDay3)}
              onPress={(): void => this.markTreatmentDate(notification.pet, treatment, 3)}
              isDone={this.isTreatmentDone(treatment.timeOfDay3CheckedAt)}
            />
          </View>
        )}
      </View>
    )
  }

  public render(): ReactElement {
    const { pets, navigation } = this.props
    const { isPetCreationMenuOpen, isVaccinationModalOpen, isDewormingModalOpen, isWeightModalOpen, pet } = this.state
    const todayDate = moment(new Date()).format('DD MMMM')

    const notificationsSummary: string[] = []

    let suggestions: IPetSuggestion[] = []
    let notifications: IPetNotification[] = []
    pets.forEach((p): void => {
      const n = this.petNotificationService
        .getVaccinationNotifications(p)
        .concat(this.petNotificationService.getDewormingNotifications(p))
        .concat(this.petNotificationService.getTreatmentNotifications(p))

      notifications = notifications.concat(n)

      if (n.length > 0) {
        notificationsSummary.push(R.strings.pets.notificationsCount(p.name, n.length))
      } else {
        notificationsSummary.push(R.strings.pets.petCaughtUp(p.name))
      }

      const s = this.petSuggestionService.getSuggestions(p)
      suggestions = suggestions.concat(s)
    })

    return (
      <View style={R.styles.rootView}>
        {pet && (
          <View>
            <VaccinationEditForm
              pet={pet}
              onSave={this.saveVaccination}
              isModalOpen={isVaccinationModalOpen}
              onCancel={this.closeVaccinationModal}
            />
            <DewormingEditForm
              pet={pet}
              onSave={this.saveDeworming}
              isModalOpen={isDewormingModalOpen}
              onCancel={this.closeDewormingModal}
            />
            <WeightEditForm pet={pet} onCancel={this.closeWeightModal} isModalOpen={isWeightModalOpen} onSave={this.saveWeight} />
          </View>
        )}
        <SafeAreaView style={[R.styles.safeArea, { paddingTop: initialWindowSafeAreaInsets ? initialWindowSafeAreaInsets.top : 0 }]}>
          <ScrollContainer>
            <View style={R.styles.pushContentOpposite}>
              <View style={[pets && pets.length > 0 && { paddingBottom: R.styles.constants.containerPaddingVertical }]}>
                <View
                  style={[
                    R.styles.screenHeaderWrapper,
                    (!pets || pets.length === 0) && { marginBottom: 12 },
                    pets && pets.length > 0 && { marginTop: 20 },
                  ]}
                >
                  {!(pets && pets.length > 0) && <Text style={R.styles.fonts.hero}>{R.strings.pets.pets()}</Text>}
                  {pets && pets.length > 0 && <Text style={R.styles.fonts.title}>{todayDate}</Text>}
                  <TouchableOpacity onPress={(): void => this.setState({ isPetCreationMenuOpen: true })}>
                    <AppIcons name="plus" size={24} color={R.colors.amber} />
                  </TouchableOpacity>
                </View>
                {!(pets && pets.length > 0) && (
                  <View style={{ paddingHorizontal: 35, marginTop: 25 }}>
                    <Text style={R.styles.fonts.title}>{R.strings.pets.doYouHavePet()}</Text>
                  </View>
                )}
                <View style={R.styles.defaultContainer}>
                  {pets.map(
                    (p): ReactElement => (
                      <View key={p.id}>
                        <PetsListItem pet={p} onPress={(): boolean => navigation.navigate('PetProfileScreen', { petId: p.id })} />
                      </View>
                    ),
                  )}
                  {notifications && notifications.length > 0 && (
                    <View style={styles.notificationsRecap}>
                      <Image source={R.images.smallStar} style={styles.smallStar} />
                      {notificationsSummary.map(
                        (summary): ReactElement => (
                          <Text key={summary} style={[R.styles.fonts.headline, { color: R.colors.white }]}>
                            {summary}
                          </Text>
                        ),
                      )}
                    </View>
                  )}
                  {notifications.map(
                    (notification): ReactElement => (
                      <View key={notification.id}>
                        <PetNotificationCard
                          notification={notification}
                          pet={notification.pet}
                          onPress={this.getNotificationAction(notification)}
                          customContent={this.getCustomNotificationContent(notification)}
                        />
                      </View>
                    ),
                  )}
                  {suggestions && suggestions.length > 0 && (
                    <View style={{ paddingLeft: 13, marginTop: 40, marginBottom: 10 }}>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.suggestions()}</Text>
                    </View>
                  )}
                  {suggestions.map(
                    (suggestion): ReactElement => (
                      <View key={suggestion.id}>
                        <PetSuggestionCard suggestion={suggestion} pet={suggestion.pet} onPress={this.getSuggestionAction(suggestion)} />
                      </View>
                    ),
                  )}
                </View>
              </View>

              {!(pets && pets.length > 0) && (
                <View style={R.styles.bottomButtonWrapper}>
                  <Button
                    title={R.strings.pets.addPet()}
                    buttonStyle={R.styles.buttons.primaryButton}
                    titleStyle={R.styles.buttons.primaryButtonTitle}
                    onPress={(): void => this.setState({ isPetCreationMenuOpen: true })}
                  />
                  <Image source={R.images.paw} style={styles.topPaw} />
                  <Image source={R.images.paw} style={styles.bottomPaw} />
                </View>
              )}
            </View>
            <AdBanner containerStyle={{ width: '100%' }} bannerSize="fullBanner" />
          </ScrollContainer>
          <Modal
            supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
            animationType="fade"
            visible={isPetCreationMenuOpen}
            presentationStyle="overFullScreen"
            transparent
          >
            <PetCreationMenu closeAction={(): void => this.setState({ isPetCreationMenuOpen: false })} navigation={navigation} />
          </Modal>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    pets: state.pets.pets,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PetsScreen)
