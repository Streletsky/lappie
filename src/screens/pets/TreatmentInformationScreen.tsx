import React, { Component, ReactElement } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import { TouchableOpacity, View, StyleSheet, Image, Text, Alert } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import moment from 'moment'
import { Divider } from 'react-native-elements'
import ProgressBar from 'react-native-progress/Bar'
import SafeAreaView from 'react-native-safe-area-view'

import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import R from '../../res/R'
import AppIcons from '../../components/AppIcons'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import TreatmentEditForm from '../../components/TreatmentEditForm'
import ScrollContainer from '../../components/ScrollContainer'
import ITreatment from '../../types/Treatment'
import {
  getPetDefaultPhoto,
  getTreatmentPeriod,
  formatTime,
  isTreatmentExpired,
  isTreatmentPlanned,
  isTreatmentActive,
} from '../../utilities/helpers'
import constants from '../../utilities/constants'
import RemoveButton from '../../components/RemoveButton'
import ActionTrackerCard from '../../components/ActionTrackerCard'

const styles = StyleSheet.create({
  fieldsContainer: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
    width: '100%',
  },
  label: {
    ...R.styles.fonts.body,
    marginTop: 6,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  treatment: ITreatment
  isTreatmentModalOpen: boolean
}

class TreatmentInformationScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openTreatmentModal = navigation.getParam('openTreatmentModal', (): void => {})

    return {
      title: R.strings.pets.health.treatmentInfo(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openTreatmentModal}>
          <AppIcons name="edit" size={24} color={R.colors.emerald} />
        </TouchableOpacity>
      ),
    }
  }

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)
    const treatmentId = navigation.getParam('treatmentId', undefined)
    const treatment = _.find(pet.treatments, { id: treatmentId })!

    this.state = {
      pet,
      treatment,
      isTreatmentModalOpen: false,
    }
  }

  private closeTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: false })
  }

  private openTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: true })
  }

  private saveTreatment = (treatment: ITreatment): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    pet.treatments = pet.treatments.map(
      (v): ITreatment => {
        if (v.id !== treatment.id) {
          return v
        }

        return {
          ...v,
          ...treatment,
        }
      },
    )

    this.setState({ pet })
    this.setState({ treatment })
    handleSavePet(pet)

    this.closeTreatmentModal()
  }

  private removeTreatment = (): void => {
    const { navigation, handleSavePet } = this.props
    const { pet, treatment } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            pet.treatments = pet.treatments.filter((v): boolean => treatment.id !== v.id)
            handleSavePet(pet)
            navigation.navigate('PetHealthScreen', { petId: pet.id })
          },
        },
      ],
      { cancelable: false },
    )
  }

  private isTreatmentDone = (date?: Date): boolean => {
    const checkedDate = moment(date)
    const today = moment(new Date())

    return (date && checkedDate.isSame(today, 'd')) === true
  }

  private markTreatmentDate = (timeOfDayNumber: number): void => {
    const { treatment } = this.state

    switch (timeOfDayNumber) {
      case 1:
        treatment.timeOfDay1CheckedAt = treatment.timeOfDay1CheckedAt ? undefined : new Date()
        break
      case 2:
        treatment.timeOfDay2CheckedAt = treatment.timeOfDay2CheckedAt ? undefined : new Date()
        break
      case 3:
        treatment.timeOfDay3CheckedAt = treatment.timeOfDay3CheckedAt ? undefined : new Date()
        break
      default:
        break
    }

    this.saveTreatment(treatment)
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    navigation.setParams({ openTreatmentModal: this.openTreatmentModal })
  }

  public render(): ReactElement {
    const { pet, treatment, isTreatmentModalOpen } = this.state

    const periodicity = _.find(constants.extendedPeriodicityDropdownItems, { value: treatment.periodicity })
    const periodicityText = (periodicity && periodicity.text) || R.strings.ui.notSpecified()
    const today = moment(new Date())
    const startDate = moment(treatment.startDate)
    const endDate = moment(treatment.endDate)
    const daysSinceStart = today.diff(startDate, 'days')
    const periodLength = endDate.diff(startDate, 'days')
    const isExpired = isTreatmentExpired(treatment)
    const isPlanned = isTreatmentPlanned(treatment)
    const isActive = isTreatmentActive(treatment)

    let title = ''
    if (isExpired) {
      title = R.strings.pets.expired()
    } else if (isPlanned) {
      title = R.strings.pets.comingUpIn(Math.abs(daysSinceStart))
    } else if (isActive) {
      title = daysSinceStart > 45 ? R.strings.pets.monthCount(Math.ceil(daysSinceStart / 30)) : R.strings.pets.daysCount(daysSinceStart)
    }

    return (
      <View style={R.styles.rootView}>
        <TreatmentEditForm
          pet={pet}
          onSave={this.saveTreatment}
          isModalOpen={isTreatmentModalOpen}
          onCancel={this.closeTreatmentModal}
          treatment={treatment}
        />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={{ paddingVertical: R.styles.constants.containerPaddingVertical }}>
              <View>
                <View style={[R.styles.columnCenter]}>
                  <View style={R.styles.rowCenter}>
                    <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.largeAvatar} />
                    <AppIcons name="treatment" size={20} color={R.colors.emerald} style={{ marginLeft: 10 }} />
                  </View>
                  <Text style={[R.styles.fonts.hero, { marginTop: 13, textAlign: 'center' }]}>{treatment.name}</Text>
                  <Text style={[R.styles.fonts.body, { marginTop: 6 }]}>{getTreatmentPeriod(treatment)}</Text>
                  <Text style={[R.styles.fonts.button, { marginTop: 26 }, isTreatmentExpired(treatment) && { color: R.colors.porpoise }]}>
                    {title}
                  </Text>
                </View>
                <View>
                  {isActive && (
                    <View style={{ marginTop: 16, width: '100%', paddingHorizontal: R.styles.constants.containerPaddingHorizontal }}>
                      <ProgressBar
                        progress={treatment.endDate ? daysSinceStart / periodLength : 0}
                        color={R.colors.emerald}
                        borderWidth={0}
                        unfilledColor={R.colors.ghostWhite}
                        width={null}
                      />
                    </View>
                  )}
                  <View style={styles.fieldsContainer}>
                    <View>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.application()}</Text>
                      <Text style={styles.label}>{periodicityText}</Text>
                    </View>
                    {isActive && (
                      <View>
                        {(treatment.periodicity === 1 || treatment.periodicity === 2 || treatment.periodicity === 3) &&
                          treatment.timeOfDay1 && (
                            <View style={{ marginTop: 7 }}>
                              <ActionTrackerCard
                                title={formatTime(treatment.timeOfDay1)}
                                onPress={(): void => this.markTreatmentDate(1)}
                                isDone={this.isTreatmentDone(treatment.timeOfDay1CheckedAt)}
                              />
                            </View>
                          )}
                        {(treatment.periodicity === 1 || treatment.periodicity === 2) && treatment.timeOfDay2 && (
                          <View>
                            <ActionTrackerCard
                              title={formatTime(treatment.timeOfDay2)}
                              onPress={(): void => this.markTreatmentDate(2)}
                              isDone={this.isTreatmentDone(treatment.timeOfDay2CheckedAt)}
                            />
                          </View>
                        )}
                        {treatment.periodicity === 1 && treatment.timeOfDay3 && (
                          <View>
                            <ActionTrackerCard
                              title={formatTime(treatment.timeOfDay3)}
                              onPress={(): void => this.markTreatmentDate(3)}
                              isDone={this.isTreatmentDone(treatment.timeOfDay3CheckedAt)}
                            />
                          </View>
                        )}
                      </View>
                    )}
                  </View>
                  <Divider style={[R.styles.divider]} />
                  <View style={styles.fieldsContainer}>
                    <View>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.notes()}</Text>
                      <Text style={styles.label}>{treatment.description}</Text>
                    </View>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
              </View>
              <RemoveButton title={R.strings.pets.health.treatmentRemove()} onPress={this.removeTreatment} />
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TreatmentInformationScreen)
