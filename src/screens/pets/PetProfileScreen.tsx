import React, { Component, ReactElement } from 'react'
import { View, Text, Image, StyleSheet, Alert, TouchableOpacity, Modal } from 'react-native'
import { Divider } from 'react-native-elements'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import _ from 'lodash'
import { NavigationEventSubscription } from 'react-navigation'
import { LinearGradient } from 'expo-linear-gradient'
import SafeAreaView from 'react-native-safe-area-view'

import AppIcons from '../../components/AppIcons'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import IPet from '../../types/Pet'
import R from '../../res/R'
import ScrollContainer from '../../components/ScrollContainer'
import BottomModal from '../../components/BottomModal'
import PetEditForm from '../../components/PetEditForm'
import IAppState from '../../types/AppState'
import { IPetsAction, savePet, addPet, removePet } from '../../actions/actionCreators'
import featureChecker from '../../utilities/featureChecker'
import constants from '../../utilities/constants'
import { getPetDefaultPhoto, getPetAge, getPetType, getCurrentPetWeight, showWeight, showHealth, showDocs } from '../../utilities/helpers'
import LinkCard from '../../components/LinkCard'
import RemoveButton from '../../components/RemoveButton'
import BetaPlaceholder from '../../components/BetaPlaceholder'

const cardHeight = featureChecker.getScreenWidth() > constants.bigScreenSize ? 108 : 80
const avatarHeight = featureChecker.getScreenWidth() > constants.bigScreenSize ? 190 : 120

const styles = StyleSheet.create({
  topInfoSection: {
    alignItems: 'center',
    paddingTop: R.styles.constants.containerPaddingVertical,
  },
  image: {
    width: avatarHeight,
    height: avatarHeight,
    borderRadius: avatarHeight / 2,
    marginBottom: 16,
  },
  profileNavigationSection: {
    flex: 1,
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
  weightLinkCard: {
    ...R.styles.borderRadiuses.large,
    minHeight: 50,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    backgroundColor: R.colors.white,
    paddingHorizontal: 10,
    marginTop: 16,
  },
  weightLabel: {
    ...R.styles.fonts.body,
    color: R.colors.porpoise,
    marginLeft: 4,
  },
  weightAmount: {
    ...R.styles.fonts.headline,
    color: R.colors.black,
  },
  imageLinkCard: {
    ...R.styles.borderRadiuses.large,
    height: cardHeight,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginTop: 8,
  },
  imageLinkCardTitle: {
    ...R.styles.fonts.hero,
    color: 'white',
  },
  imageLinkImage: {
    position: 'absolute',
    bottom: 0,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
  handleRemovePet(pet: IPet): void
}

interface IStateFromProps {
  selectedWeightUnitIndex: number
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  initialPet: IPet
  isPetModalOpen: boolean
  isBetaUpOpen: boolean
}

class PetProfileScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openPetModal = navigation.getParam('openPetModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.petProfile(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openPetModal}>
          <AppIcons name="edit" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      initialPet: pet,
      isPetModalOpen: false,
      isBetaUpOpen: false,
    }
  }

  public removePet = (): void => {
    const { handleRemovePet, navigation } = this.props
    const { pet } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            handleRemovePet(pet)
            navigation.navigate('PetsScreen')
          },
        },
      ],
      { cancelable: false },
    )
  }

  private savePet = (): void => {
    const { handleSavePet } = this.props
    const { pet } = this.state

    handleSavePet(pet)

    this.setState({ isPetModalOpen: false, initialPet: pet })
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet, initialPet: pet })
  }

  private closePetModal = (): void => {
    const { initialPet } = this.state

    this.setState({ pet: initialPet, isPetModalOpen: false })
  }

  private openPetModal = (): void => {
    this.setState({ isPetModalOpen: true })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openPetModal: this.openPetModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isPetModalOpen, isBetaUpOpen } = this.state
    const { navigation, selectedWeightUnitIndex } = this.props

    return (
      <View style={R.styles.rootView}>
        <BottomModal
          onSave={this.savePet}
          title={R.strings.pets.editPet()}
          onCancel={this.closePetModal}
          saveButtonEnabled={(pet.name && pet.name.length > 0) === true}
          visible={isPetModalOpen}
        >
          <ActionSheetProvider>
            <View>
              <PetEditForm pet={pet} setParentState={(data): void => this.setState({ pet: data })} />
            </View>
          </ActionSheetProvider>
        </BottomModal>
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View style={R.styles.pushContentOpposite}>
              <View>
                <View style={styles.topInfoSection}>
                  <Image source={pet.photo ? { uri: pet.photo } : getPetDefaultPhoto(pet.type)} style={styles.image} />
                  <Text style={[R.styles.fonts.hero, { marginBottom: 6 }]}>{pet.name}</Text>
                  <View style={R.styles.rowCenter}>
                    <Text style={R.styles.fonts.callout}>{`${getPetAge(pet) || ''} ${pet.breed || getPetType(pet) || ''}`}</Text>
                    <AppIcons
                      name={pet.sex === 0 ? 'gender-male' : 'gender-female'}
                      size={20}
                      color={pet.sex === 0 ? R.colors.aegean : R.colors.scarlet}
                      style={{ marginLeft: 5 }}
                    />
                  </View>
                </View>
                <View style={styles.profileNavigationSection}>
                  {showWeight(pet) && (
                    <TouchableOpacity onPress={(): boolean => navigation.navigate('WeightOverviewScreen', { petId: pet.id })}>
                      <View style={styles.weightLinkCard}>
                        <View style={[R.styles.rowCenter, { flex: 1, justifyContent: 'flex-start' }]}>
                          <AppIcons name="weight" size={17} color={R.colors.porpoise} />
                          <Text style={styles.weightLabel}>{R.strings.pets.weight()}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                          <Text style={styles.weightAmount}>{getCurrentPetWeight(pet, selectedWeightUnitIndex)}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'flex-end' }}>
                          <AppIcons name="chevron" size={16} color={R.colors.darkCloud} />
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                  {showHealth(pet) && (
                    <TouchableOpacity
                      onPress={(): boolean =>
                        navigation.navigate('PetHealthScreen', {
                          petId: pet.id,
                        })
                      }
                    >
                      <LinearGradient
                        style={[styles.imageLinkCard, { backgroundColor: R.colors.honey }]}
                        colors={[...R.colors.gradient.fill.honeycomb]}
                        start={R.colors.gradient.direction.vertical.start}
                        end={R.colors.gradient.direction.vertical.end}
                      >
                        <Text style={styles.imageLinkCardTitle}>{R.strings.pets.health.health()}</Text>
                        <AppIcons name="chevron" size={16} color={R.colors.white} />
                        <Image
                          style={[styles.imageLinkImage, { right: '10%' }]}
                          source={constants.petHealthCardImage[pet.type]}
                          height={cardHeight - 10}
                          resizeMode="contain"
                        />
                      </LinearGradient>
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity onPress={(): void => this.setState({ isBetaUpOpen: true })}>
                    <LinearGradient
                      style={[styles.imageLinkCard, { backgroundColor: R.colors.honey }]}
                      colors={[...R.colors.gradient.fill.berry]}
                      start={R.colors.gradient.direction.vertical.start}
                      end={R.colors.gradient.direction.vertical.end}
                    >
                      <Text style={styles.imageLinkCardTitle}>{R.strings.pets.health.checkup()}</Text>
                      <AppIcons name="chevron" size={16} color={R.colors.white} />
                      <Image
                        style={[styles.imageLinkImage, { right: '15%', bottom: 0 }]}
                        source={R.images.careCardImage}
                        height={cardHeight - 30}
                        resizeMode="contain"
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                  {showDocs(pet) && (
                    <View>
                      <LinkCard
                        title={R.strings.pets.documents()}
                        onPress={(): void => this.setState({ isBetaUpOpen: true })}
                        noRoundingBottom
                      />
                      <Divider style={[R.styles.divider]} />
                    </View>
                  )}
                  <LinkCard
                    title={R.strings.pets.photos()}
                    onPress={(): void => this.setState({ isBetaUpOpen: true })}
                    noRoundingTop={showDocs(pet)}
                    containerStyle={[showDocs(pet) ? { marginTop: 0 } : {}]}
                  />
                  <LinkCard title={R.strings.pets.shareExport()} onPress={(): void => this.setState({ isBetaUpOpen: true })} />
                </View>
              </View>
              <RemoveButton title={R.strings.pets.removePet()} onPress={this.removePet} />
            </View>
          </ScrollContainer>
          <Modal
            supportedOrientations={['portrait', 'portrait-upside-down']}
            animationType="fade"
            visible={isBetaUpOpen}
            presentationStyle="overFullScreen"
            transparent
          >
            <BetaPlaceholder closeAction={(): void => this.setState({ isBetaUpOpen: false })} />
          </Modal>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return pet.id ? dispatch(savePet(pet)) : dispatch(addPet(pet))
  },
  handleRemovePet: (pet: IPet): IPetsAction => dispatch(removePet(pet)),
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    selectedWeightUnitIndex: state.settings.selectedWeightUnitIndex,
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PetProfileScreen)
