import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationStackOptions } from 'react-navigation-stack'
import _ from 'lodash'
import SafeAreaView from 'react-native-safe-area-view'

import IPet from '../../types/Pet'
import R from '../../res/R'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import { savePet, IPetsAction } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import AppIcons from '../../components/AppIcons'
import IWeight from '../../types/Weight'
import { getPetDefaultPhoto, getPetAge, formatDateShort, getWeightText } from '../../utilities/helpers'
import WeightEditForm from '../../components/WeightEditForm'
import RemoveButton from '../../components/RemoveButton'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  selectedWeightUnitIndex: number
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isWeightModalOpen: boolean
  petWeight: IWeight
  initialPetWeight: IWeight
}

class WeightDetailsScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openWeightModal = navigation.getParam('openWeightModal', (): void => {})

    return {
      title: R.strings.pets.weightDetails(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openWeightModal}>
          <AppIcons name="edit" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const weightId = navigation.getParam('weightId', undefined)
    const pet = fetchPet(petId)
    const petWeight = _.find(pet.weightEntries, ['id', weightId])!

    console.log(petWeight)

    navigation.setParams({ pet })

    this.state = {
      pet,
      petWeight,
      isWeightModalOpen: false,
      initialPetWeight: petWeight,
    }
  }

  private savePet = (pet: IPet): void => {
    const { handleSavePet } = this.props

    handleSavePet(pet)
  }

  private onCloseWeightModal = (): void => {
    const { initialPetWeight } = this.state

    this.setState({
      petWeight: initialPetWeight,
      isWeightModalOpen: false,
    })
  }

  private openWeightModal = (): void => {
    const { petWeight } = this.state

    this.setState({ isWeightModalOpen: true, initialPetWeight: petWeight })
  }

  private onSaveWeightModal = (weight: IWeight): void => {
    const { pet } = this.state

    pet.weightEntries = pet.weightEntries.map(
      (w): IWeight => {
        if (w.id !== weight.id) {
          return w
        }

        return {
          ...w,
          ...weight,
        }
      },
    )

    this.savePet(pet)

    this.setState({
      petWeight: weight,
      initialPetWeight: weight,
      isWeightModalOpen: false,
    })
  }

  private removeWeightEntry = (): void => {
    const { pet, petWeight } = this.state
    const { navigation } = this.props

    pet.weightEntries = pet.weightEntries.filter((weight): boolean => petWeight.id !== weight.id)

    this.savePet(pet)

    navigation.navigate('WeightOverviewScreen', { petId: pet.id })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    navigation.setParams({ openWeightModal: this.openWeightModal })
  }

  public render(): ReactElement {
    const { selectedWeightUnitIndex } = this.props
    const { isWeightModalOpen, pet, petWeight } = this.state

    return (
      <View style={R.styles.rootView}>
        <WeightEditForm
          pet={pet}
          weight={petWeight}
          onCancel={this.onCloseWeightModal}
          isModalOpen={isWeightModalOpen}
          onSave={this.onSaveWeightModal}
        />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={R.styles.pushContentOpposite}>
              <View style={R.styles.defaultContainer}>
                <View style={{ marginTop: R.styles.constants.containerPaddingVertical }}>
                  <View style={R.styles.rowCenter}>
                    <Image
                      source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)}
                      style={[R.styles.smallAvatar, { width: 64, height: 64, borderRadius: 32 }]}
                    />
                  </View>
                  <View style={[R.styles.rowCenter, { marginTop: 9 }]}>
                    <Text style={[R.styles.fonts.hero]}>{getWeightText(petWeight, selectedWeightUnitIndex)}</Text>
                  </View>
                  <View style={[R.styles.rowCenter, { marginTop: 9 }]}>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.black }]}>{formatDateShort(petWeight.date)}</Text>
                  </View>
                  <View style={[R.styles.rowCenter, { marginTop: 5 }]}>
                    <Text style={[R.styles.fonts.button, { color: R.colors.black }]}>{getPetAge(pet) || '-'}</Text>
                  </View>
                </View>
              </View>
              <RemoveButton title={R.strings.pets.removeWeight()} onPress={this.removeWeightEntry} />
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    selectedWeightUnitIndex: state.settings.selectedWeightUnitIndex,
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WeightDetailsScreen)
