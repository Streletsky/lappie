import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Text, Alert, TouchableOpacity, Modal } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import {
  formatDate,
  getVaccinationRemainingTime,
  isVaccinationExpired,
  isVaccinationActive,
  isTreatmentActive,
  getTreatmentRemainingTime,
  showVaccination,
  showDeworming,
  showAllergies,
  showChip,
  showSterilization,
} from '../../utilities/helpers'
import ToggleCard from '../../components/ToggleCard'
import AppIcons from '../../components/AppIcons'
import LinkCard from '../../components/LinkCard'
import IAppState from '../../types/AppState'
import ChipEditForm from '../../components/ChipEditForm'
import ExpandableLinkCard from '../../components/ExpandableLinkCard'
import IVaccination from '../../types/Vaccination'
import IDeworming from '../../types/Deworming'
import BetaPlaceholder from '../../components/BetaPlaceholder'

const styles = StyleSheet.create({
  cardInfo: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    alignContent: 'center',
    justifyContent: 'center',
  },
  cardInfoWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  initialPet: IPet
  isChipModalOpen: boolean
  isBetaUpOpen: boolean
}

class PetHealthScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.health.healthOf(pet.name),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      initialPet: pet,
      isChipModalOpen: false,
      isBetaUpOpen: false,
    }
  }

  private savePet = (pet: IPet): void => {
    const { handleSavePet } = this.props

    handleSavePet(pet)
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet, initialPet: pet })
  }

  private setSterilization = (value: boolean): void => {
    const { pet } = this.state

    pet.sterilization = value

    this.setState({ pet })
    this.savePet(pet)
  }

  private switchChip = (switchCallback: (result: boolean) => void): void => {
    const { pet } = this.state

    this.setState({ initialPet: pet })

    if (!pet.chip) {
      this.setState({
        pet: { ...pet, chip: true },
        isChipModalOpen: true,
      })
    } else {
      Alert.alert(
        R.strings.ui.confirmAction(),
        R.strings.pets.health.doYouEraseChipData(),
        [
          {
            text: R.strings.ui.cancel(),
            style: 'cancel',
            onPress: (): void => {
              switchCallback(false)
            },
          },
          {
            text: R.strings.ui.ok(),
            onPress: (): void => {
              const petToSave = {
                ...pet,
                health: { ...pet, chip: false, chipData: { serialNumber: undefined, date: undefined, clinic: undefined } },
              }
              this.setState({
                pet: petToSave,
              })
              this.savePet(petToSave)
              switchCallback(true)
            },
          },
        ],
        { cancelable: false },
      )
    }
  }

  private onCloseChipModal = (): void => {
    const { initialPet } = this.state

    this.setState({ pet: initialPet, isChipModalOpen: false })
  }

  private openChipModal = (): void => {
    const { pet } = this.state

    this.setState({ initialPet: pet, isChipModalOpen: true })
  }

  private onSaveChipModal = (): void => {
    const { pet } = this.state

    this.setState({ isChipModalOpen: false })
    this.savePet(pet)
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isChipModalOpen, isBetaUpOpen } = this.state
    const { navigation } = this.props
    const expiredVaccinations = pet.vaccinations.filter(isVaccinationExpired)
    const activeVaccinations = pet.vaccinations.filter(isVaccinationActive)
    const activeTreatments = pet.treatments.filter(isTreatmentActive)
    const activeDewormings = pet.dewormings.filter(isVaccinationActive)
    const expiredewormings = pet.dewormings.filter(isVaccinationExpired)

    let titleVaccination: IVaccination | undefined
    let vaccinationExpired = false
    if (expiredVaccinations.length > 0) {
      titleVaccination = _.first(_.sortBy(expiredVaccinations, 'endDate'))
      vaccinationExpired = true
    } else {
      titleVaccination = _.first(_.sortBy(activeVaccinations, 'endDate'))
    }

    const activeTreatment = _.first(_.sortBy(activeTreatments, 'endDate'))

    let titleDeworming: IDeworming | undefined
    let dewormingExpired = false
    if (expiredewormings.length > 0) {
      titleDeworming = _.first(_.sortBy(expiredewormings, 'endDate'))
      dewormingExpired = true
    } else {
      titleDeworming = _.first(_.sortBy(activeDewormings, 'endDate'))
    }

    return (
      <View style={R.styles.rootView}>
        <ChipEditForm
          pet={pet}
          setParentState={(data): void => this.setState({ pet: data })}
          onSave={this.onSaveChipModal}
          isModalOpen={isChipModalOpen}
          onCancel={this.onCloseChipModal}
        />
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View style={R.styles.defaultContainer}>
              {showVaccination(pet) && (
                <ExpandableLinkCard
                  color={R.colors.aegean}
                  title={R.strings.pets.health.vaccination()}
                  icon="vaccine"
                  onCardPress={(): boolean => navigation.navigate('VaccinationsOverviewScreen', { petId: pet.id })}
                  placeholder={R.strings.pets.health.noActiveVaccination()}
                  expired={vaccinationExpired}
                >
                  {pet.vaccinations && pet.vaccinations.length > 0 && titleVaccination ? (
                    <View>
                      <Text style={[R.styles.fonts.body, { color: R.colors.black }]}>
                        {`${titleVaccination.name}, ${getVaccinationRemainingTime(titleVaccination)}`}
                      </Text>
                    </View>
                  ) : (
                    undefined
                  )}
                </ExpandableLinkCard>
              )}
              {showDeworming(pet) && (
                <ExpandableLinkCard
                  color={R.colors.cedar}
                  title={R.strings.pets.health.deworming()}
                  icon="deworm"
                  onCardPress={(): boolean => navigation.navigate('DewormingsOverviewScreen', { petId: pet.id })}
                  placeholder={R.strings.pets.health.noActiveDeworming()}
                  expired={dewormingExpired}
                >
                  {pet.dewormings && pet.dewormings.length > 0 && titleDeworming ? (
                    <View>
                      <Text style={[R.styles.fonts.body, { color: R.colors.black }]}>
                        {`${titleDeworming.name}, ${getVaccinationRemainingTime(titleDeworming)}`}
                      </Text>
                    </View>
                  ) : (
                    undefined
                  )}
                </ExpandableLinkCard>
              )}
              <ExpandableLinkCard
                color={R.colors.emerald}
                title={R.strings.pets.health.treatment()}
                icon="treatment"
                onCardPress={(): boolean => navigation.navigate('TreatmentsOverviewScreen', { petId: pet.id })}
                placeholder={R.strings.pets.health.noActiveTreatment()}
              >
                {pet.treatments && pet.treatments.length > 0 && activeTreatment ? (
                  <View>
                    <Text style={[R.styles.fonts.body, { color: R.colors.black }]}>
                      {`${activeTreatment.name}, ${getTreatmentRemainingTime(activeTreatment)}`}
                    </Text>
                  </View>
                ) : (
                  undefined
                )}
              </ExpandableLinkCard>
              <LinkCard
                title={R.strings.pets.health.visitPlan()}
                onPress={(): boolean => navigation.navigate('VisitsOverviewScreen', { petId: pet.id })}
              />
              {showAllergies(pet) && (
                <LinkCard title={R.strings.pets.health.allergies()} onPress={(): void => this.setState({ isBetaUpOpen: true })} />
              )}
              {showChip(pet) && (
                <ToggleCard value={pet.chip} title={R.strings.pets.health.chip()} onAsyncSwitchPress={this.switchChip}>
                  <TouchableOpacity style={styles.cardInfoWrapper} onPress={this.openChipModal}>
                    <View style={styles.cardInfo}>
                      <Text style={R.styles.fonts.body}>{pet.chipData.serialNumber}</Text>
                      {pet.chipData.date && (
                        <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{formatDate(pet.chipData.date)}</Text>
                      )}
                    </View>
                    <View>
                      <AppIcons name="edit" size={24} color={R.colors.porpoise} />
                    </View>
                  </TouchableOpacity>
                </ToggleCard>
              )}
              {showSterilization(pet) && (
                <ToggleCard
                  value={pet.sterilization}
                  title={R.strings.pets.health.sterilization()}
                  onSyncSwitchPress={this.setSterilization}
                />
              )}
            </View>
          </ScrollContainer>
          <Modal
            supportedOrientations={['portrait', 'portrait-upside-down']}
            animationType="fade"
            visible={isBetaUpOpen}
            presentationStyle="overFullScreen"
            transparent
          >
            <BetaPlaceholder closeAction={(): void => this.setState({ isBetaUpOpen: false })} />
          </Modal>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PetHealthScreen)
