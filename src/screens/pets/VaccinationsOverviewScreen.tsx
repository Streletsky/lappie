import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import { Divider } from 'react-native-elements'
import moment from 'moment'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import {
  getVaccinationRemainingTime,
  getVaccinationEndDate,
  isVaccinationEndUpcoming,
  isVaccinationActive,
  isVaccinationPlanned,
  isVaccinationExpired,
} from '../../utilities/helpers'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import VaccinationEditForm from '../../components/VaccinationEditForm'
import IVaccination from '../../types/Vaccination'
import LinkCardWithInfo from '../../components/LinkCardWithInfo'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isVaccinationModalOpen: boolean
}

class VaccinationsOverviewScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openVaccinationModal = navigation.getParam('openVaccinationModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.health.vaccinationTitle(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openVaccinationModal}>
          <AppIcons name="plus" size={24} color={R.colors.aegean} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      isVaccinationModalOpen: false,
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  private closeVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: false })
  }

  private openVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: true })
  }

  private saveVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props
    const petToSave = { ...pet, vaccinations: [...pet.vaccinations, vaccination] }

    handleSavePet(petToSave)
    this.setState({ pet: petToSave })
    this.closeVaccinationModal()
  }

  private goToVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state
    const { navigation } = this.props

    navigation.navigate('VaccinationInformationScreen', { petId: pet.id, vaccinationId: vaccination.id })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openVaccinationModal: this.openVaccinationModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isVaccinationModalOpen } = this.state
    const today = moment(new Date())
    const activeVaccinations = pet.vaccinations.filter(isVaccinationActive)
    const plannedVaccinations = pet.vaccinations.filter(isVaccinationPlanned)
    const expiredVaccinations = pet.vaccinations.filter(isVaccinationExpired)

    return (
      <View style={R.styles.rootView}>
        <VaccinationEditForm
          pet={pet}
          onSave={this.saveVaccination}
          isModalOpen={isVaccinationModalOpen}
          onCancel={this.closeVaccinationModal}
        />
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View
              style={[
                R.styles.pushContentOpposite,
                R.styles.defaultContainer,
                { paddingVertical: R.styles.constants.containerPaddingVertical },
              ]}
            >
              {(!pet.vaccinations || pet.vaccinations.length === 0) && (
                <TouchableOpacity
                  style={{ ...R.styles.borderRadiuses.large, backgroundColor: R.colors.white, padding: 15 }}
                  onPress={this.openVaccinationModal}
                >
                  <View>
                    <Text style={[R.styles.fonts.callout]}>
                      <Text style={[R.styles.fonts.callout, { fontWeight: 'bold' }]}>{pet.name}</Text>
                      {R.strings.pets.health.vaccinationPlaceholder()}
                    </Text>
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={[R.styles.fonts.button, { color: R.colors.aegean }]}>{`+ ${R.strings.pets.health.vaccinationAdd()}`}</Text>
                  </View>
                </TouchableOpacity>
              )}
              <View>
                {activeVaccinations && activeVaccinations.length > 0 && (
                  <View>
                    <View>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.active()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {activeVaccinations.map(
                        (vaccination, index): ReactElement => (
                          <View key={vaccination.id}>
                            <LinkCardWithInfo
                              title={vaccination.name}
                              subtitle={getVaccinationEndDate(vaccination)}
                              bottomText={getVaccinationRemainingTime(vaccination)}
                              onPress={(): void => this.goToVaccination(vaccination)}
                              noRoundingBottom={activeVaccinations.length > 1 && index === 0}
                              noRoundingTop={activeVaccinations.length > 1 && index === activeVaccinations.length - 1}
                              noRounding={activeVaccinations.length > 1 && index !== 0 && index !== activeVaccinations.length - 1}
                              isActive
                              isUpcoming={isVaccinationEndUpcoming(vaccination)}
                            />
                            {index !== activeVaccinations.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
                {plannedVaccinations && plannedVaccinations.length > 0 && (
                  <View>
                    <View style={[activeVaccinations.length > 0 && { marginTop: 24 }]}>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.planned()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {plannedVaccinations.map(
                        (vaccination, index): ReactElement => (
                          <View key={vaccination.id}>
                            <LinkCardWithInfo
                              title={vaccination.name}
                              subtitle={getVaccinationEndDate(vaccination)}
                              bottomText={R.strings.pets.comingUpIn(moment(vaccination.startDate).diff(today, 'days'))}
                              onPress={(): void => this.goToVaccination(vaccination)}
                              noRoundingBottom={plannedVaccinations.length > 1 && index === 0}
                              noRoundingTop={plannedVaccinations.length > 1 && index === plannedVaccinations.length - 1}
                              noRounding={plannedVaccinations.length > 1 && index !== 0 && index !== plannedVaccinations.length - 1}
                            />
                            {index !== plannedVaccinations.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
              </View>
              {expiredVaccinations && expiredVaccinations.length > 0 && (
                <View style={[activeVaccinations.length > 0 && { marginTop: 35 }]}>
                  <View>
                    <Text style={[R.styles.fonts.title, { color: R.colors.porpoise }]}>{R.strings.pets.inactive()}</Text>
                  </View>
                  <View style={{ marginTop: 8 }}>
                    {expiredVaccinations.map(
                      (vaccination, index): ReactElement => (
                        <View key={vaccination.id}>
                          <LinkCardWithInfo
                            title={vaccination.name}
                            subtitle={getVaccinationEndDate(vaccination)}
                            onPress={(): void => this.goToVaccination(vaccination)}
                            noRoundingBottom={expiredVaccinations.length > 1 && index === 0}
                            noRoundingTop={expiredVaccinations.length > 1 && index === expiredVaccinations.length - 1}
                            noRounding={expiredVaccinations.length > 1 && index !== 0 && index !== expiredVaccinations.length - 1}
                          />
                          {index !== expiredVaccinations.length - 1 && <Divider style={R.styles.divider} />}
                        </View>
                      ),
                    )}
                  </View>
                </View>
              )}
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VaccinationsOverviewScreen)
