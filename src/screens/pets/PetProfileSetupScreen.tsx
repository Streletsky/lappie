import React, { Component, ReactElement } from 'react'
import { View } from 'react-native'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Button } from 'react-native-elements'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { addPet, IPetsAction, savePet } from '../../actions/actionCreators'
import PetEditForm from '../../components/PetEditForm'
import PetType from '../../types/PetType'
import ScrollContainer from '../../components/ScrollContainer'
import KeyboardShift from '../../components/KeyboardShift'

interface IDispatchFromProps {
  handleSavePet(pet: IPet, petWasSaved: boolean): void
}

interface IComponentState {
  pet: IPet
  petWasSaved: boolean
}

class PetProfileSetupScreen extends Component<IDispatchFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = (): NavigationStackOptions => {
    return {
      title: R.strings.pets.addPet(),
    }
  }

  public constructor(props: IDispatchFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation } = this.props
    const type = navigation.getParam('type', PetType.Cat)

    this.state = {
      petWasSaved: false,
      pet: {
        type,
        birthday: undefined,
        breed: undefined,
        name: '',
        id: uuid(),
        photo: undefined,
        modifiedAt: new Date(),
        createdAt: new Date(),
        sex: 0,
        ageOptionSelected: type === PetType.Bird || type === PetType.Rodent,
        weightEntries: [],
        ageEnteredOn: new Date(),
        ageMonths: 0,
        chip: false,
        sterilization: false,
        chipData: {
          date: new Date(),
          serialNumber: '',
          clinic: '',
        },
        vaccinations: [],
        treatments: [],
        dewormings: [],
        visits: [],
      },
    }
  }

  private nextScreen = (): void => {
    const { navigation, handleSavePet } = this.props
    const { pet, petWasSaved } = this.state

    this.setState({ petWasSaved: true })

    handleSavePet(pet, petWasSaved)

    let nextScreen = 'PetWeightSetupScreen'

    switch (pet.type) {
      case PetType.Rodent:
      case PetType.Bird:
        nextScreen = 'PetHealthSetupScreen'
        break
      case PetType.Fish:
        nextScreen = 'PetsScreen'
        break
      default:
        nextScreen = 'PetWeightSetupScreen'
    }

    navigation.navigate(nextScreen, { petId: pet.id })
  }

  public render(): ReactElement {
    const { pet } = this.state

    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.ghostWhite }]}>
        <ScrollContainer>
          <KeyboardShift>
            <ActionSheetProvider>
              <View style={R.styles.pushContentOpposite}>
                <PetEditForm pet={pet} setParentState={(data): void => this.setState({ pet: data })} addPadding />
                <View style={R.styles.bottomButtonWrapper}>
                  <Button
                    title={R.strings.ui.next()}
                    buttonStyle={pet.name ? R.styles.buttons.primaryButton : R.styles.buttons.primaryButtonDisabled}
                    titleStyle={pet.name ? R.styles.buttons.primaryButtonTitle : R.styles.buttons.primaryButtonTitleDisabled}
                    onPress={this.nextScreen}
                    disabled={!pet.name}
                  />
                </View>
              </View>
            </ActionSheetProvider>
          </KeyboardShift>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet, petWasSaved): IPetsAction => {
    if (!petWasSaved) {
      return dispatch(addPet(pet))
    }

    return dispatch(savePet(pet))
  },
})

export default connect(
  undefined,
  mapDispatchToProps,
)(PetProfileSetupScreen)
