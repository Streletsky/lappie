import React, { Component, ReactElement } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import { TouchableOpacity, View, StyleSheet, Image, Text, Alert } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import moment from 'moment'
import { Divider } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'

import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import R from '../../res/R'
import AppIcons from '../../components/AppIcons'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import VaccinationEditForm from '../../components/VaccinationEditForm'
import ScrollContainer from '../../components/ScrollContainer'
import IVaccination from '../../types/Vaccination'
import {
  getPetDefaultPhoto,
  getVaccinationRemainingTime,
  isVaccinationEndUpcoming,
  isVaccinationExpired,
  isVaccinationPlanned,
  isVaccinationActive,
  formatDateShort,
} from '../../utilities/helpers'
import constants from '../../utilities/constants'
import RemoveButton from '../../components/RemoveButton'

const styles = StyleSheet.create({
  fieldsContainer: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
  label: {
    ...R.styles.fonts.body,
    marginTop: 6,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  vaccination: IVaccination
  isVaccinationModalOpen: boolean
}

class VaccinationInformationScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openVaccinationModal = navigation.getParam('openVaccinationModal', (): void => {})

    return {
      title: R.strings.pets.health.vaccinationInfo(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openVaccinationModal}>
          <AppIcons name="edit" size={24} color={R.colors.aegean} />
        </TouchableOpacity>
      ),
    }
  }

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)
    const vaccinationId = navigation.getParam('vaccinationId', undefined)
    const vaccination = _.find(pet.vaccinations, { id: vaccinationId })!

    this.state = {
      pet,
      vaccination,
      isVaccinationModalOpen: false,
    }
  }

  private closeVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: false })
  }

  private openVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: true })
  }

  private saveVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    pet.vaccinations = pet.vaccinations.map(
      (v): IVaccination => {
        if (v.id !== vaccination.id) {
          return v
        }

        return {
          ...v,
          ...vaccination,
        }
      },
    )

    this.setState({ pet })
    this.setState({ vaccination })
    handleSavePet(pet)

    this.closeVaccinationModal()
  }

  public removeVaccination = (): void => {
    const { navigation, handleSavePet } = this.props
    const { pet, vaccination } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            pet.vaccinations = pet.vaccinations.filter((v): boolean => vaccination.id !== v.id)
            handleSavePet(pet)
            navigation.navigate('VaccinationsOverviewScreen', { petId: pet.id })
          },
        },
      ],
      { cancelable: false },
    )
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    navigation.setParams({ openVaccinationModal: this.openVaccinationModal })
  }

  public render(): ReactElement {
    const { pet, vaccination, isVaccinationModalOpen } = this.state

    const periodicityText = _.find(constants.periodicityDropdownItems, { value: vaccination.periodicity })!
    const today = new Date()
    const startDate = moment(vaccination.startDate)
    const endDate = moment(vaccination.startDate).add(vaccination.periodicity, 'y')
    const comingUpIn = startDate.diff(today, 'days')
    const isExpired = isVaccinationExpired(vaccination)
    const isPlanned = isVaccinationPlanned(vaccination)
    const isActive = isVaccinationActive(vaccination)
    const isUpcoming = isVaccinationEndUpcoming(vaccination)

    let title = ''
    if (isExpired) {
      title = R.strings.pets.inactive()
    } else if (isPlanned) {
      title = R.strings.pets.comingUpIn(comingUpIn)
    } else if (isActive) {
      title = getVaccinationRemainingTime(vaccination)
    }

    return (
      <View style={R.styles.rootView}>
        <VaccinationEditForm
          pet={pet}
          onSave={this.saveVaccination}
          isModalOpen={isVaccinationModalOpen}
          onCancel={this.closeVaccinationModal}
          vaccination={vaccination}
        />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={{ paddingVertical: R.styles.constants.containerPaddingVertical }}>
              <View>
                <View style={[R.styles.columnCenter, { paddingHorizontal: R.styles.constants.containerPaddingHorizontal }]}>
                  <View style={R.styles.rowCenter}>
                    <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
                    <AppIcons name="vaccine" size={20} color={R.colors.aegean} style={{ marginLeft: 10 }} />
                  </View>
                  <Text style={[R.styles.fonts.hero, { marginTop: 13 }]}>{vaccination.name}</Text>
                  <View style={[R.styles.rowCenter, { marginTop: 14 }]}>
                    {isActive && (
                      <View style={[R.styles.greenDot, { marginRight: 5 }, isUpcoming && { backgroundColor: R.colors.scarlet }]} />
                    )}
                    <Text style={[R.styles.fonts.button, isExpired && { color: R.colors.porpoise }]}>{title}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider, { marginTop: 17 }]} />
                <View style={styles.fieldsContainer}>
                  <View>
                    <View>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.periodicity()}</Text>
                      <Text style={styles.label}>{periodicityText.text}</Text>
                    </View>
                  </View>
                  <View style={[R.styles.rowCenter, { marginTop: 20 }]}>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.applied()}</Text>
                      <Text style={styles.label}>{formatDateShort(startDate)}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.endDate()}</Text>
                      <Text style={styles.label}>{formatDateShort(endDate)}</Text>
                    </View>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
                <View style={styles.fieldsContainer}>
                  <View>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.notes()}</Text>
                    <Text style={styles.label}>{vaccination.notes}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
              </View>
              <RemoveButton title={R.strings.pets.health.vaccinationRemove()} onPress={this.removeVaccination} />
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VaccinationInformationScreen)
