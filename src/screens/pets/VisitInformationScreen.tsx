import React, { Component, ReactElement } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import { TouchableOpacity, View, StyleSheet, Image, Text, Alert } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import moment from 'moment'
import { Divider } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'

import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import R from '../../res/R'
import AppIcons from '../../components/AppIcons'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import VisitEditForm from '../../components/VisitEditForm'
import ScrollContainer from '../../components/ScrollContainer'
import IVisit from '../../types/Visit'
import { getPetDefaultPhoto, isVisitExpired, isVisitPlanned, formatDateShort } from '../../utilities/helpers'
import RemoveButton from '../../components/RemoveButton'

const styles = StyleSheet.create({
  fieldsContainer: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
  label: {
    ...R.styles.fonts.body,
    marginTop: 6,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  visit: IVisit
  isVisitModalOpen: boolean
}

class VisitInformationScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openVisitModal = navigation.getParam('openVisitModal', (): void => {})

    return {
      title: R.strings.pets.health.visitInfo(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openVisitModal}>
          <AppIcons name="edit" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)
    const visitId = navigation.getParam('visitId', undefined)
    const visit = _.find(pet.visits, { id: visitId })!

    this.state = {
      pet,
      visit,
      isVisitModalOpen: false,
    }
  }

  private closeVisitModal = (): void => {
    this.setState({ isVisitModalOpen: false })
  }

  private openVisitModal = (): void => {
    this.setState({ isVisitModalOpen: true })
  }

  private saveVisit = (visit: IVisit): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    pet.visits = pet.visits.map(
      (v): IVisit => {
        if (v.id !== visit.id) {
          return v
        }

        return {
          ...v,
          ...visit,
        }
      },
    )

    this.setState({ pet })
    this.setState({ visit })
    handleSavePet(pet)

    this.closeVisitModal()
  }

  public removeVisit = (): void => {
    const { navigation, handleSavePet } = this.props
    const { pet, visit } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            pet.visits = pet.visits.filter((v): boolean => visit.id !== v.id)
            handleSavePet(pet)
            navigation.navigate('VisitsOverviewScreen', { petId: pet.id })
          },
        },
      ],
      { cancelable: false },
    )
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    navigation.setParams({ openVisitModal: this.openVisitModal })
  }

  public render(): ReactElement {
    const { pet, visit, isVisitModalOpen } = this.state

    const today = new Date()
    const startDate = moment(visit.date)
    const comingUpIn = startDate.diff(today, 'days')
    const isExpired = isVisitExpired(visit)
    const isPlanned = isVisitPlanned(visit)

    let title = ''
    if (isExpired) {
      title = R.strings.pets.inactive()
    } else if (isPlanned) {
      title = R.strings.pets.comingUpIn(comingUpIn)
    }

    return (
      <View style={R.styles.rootView}>
        <VisitEditForm pet={pet} onSave={this.saveVisit} isModalOpen={isVisitModalOpen} onCancel={this.closeVisitModal} visit={visit} />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={{ paddingVertical: R.styles.constants.containerPaddingVertical }}>
              <View>
                <View style={[R.styles.columnCenter, { paddingHorizontal: R.styles.constants.containerPaddingHorizontal }]}>
                  <View style={R.styles.rowCenter}>
                    <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.largeAvatar} />
                  </View>
                  <Text style={[R.styles.fonts.hero, { marginTop: 13 }]}>{visit.name}</Text>
                  <Text style={[R.styles.fonts.body, { marginTop: 5 }]}>{formatDateShort(visit.date)}</Text>
                  <View style={[R.styles.rowCenter, { marginTop: 14 }]}>
                    <Text style={[R.styles.fonts.button, isExpired && { color: R.colors.porpoise }]}>{title}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider, { marginTop: 17 }]} />
                <View style={styles.fieldsContainer}>
                  <View style={{ flex: 1 }}>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.doctors.clinic()}</Text>
                    <Text style={styles.label}>{visit.clinic}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
                <View style={styles.fieldsContainer}>
                  <View>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.notes()}</Text>
                    <Text style={styles.label}>{visit.notes}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
              </View>
              <RemoveButton title={R.strings.pets.health.visitRemove()} onPress={this.removeVisit} />
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VisitInformationScreen)
