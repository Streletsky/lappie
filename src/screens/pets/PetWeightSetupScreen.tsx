import React, { Component, ReactElement } from 'react'
import { StyleSheet, View } from 'react-native'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Button } from 'react-native-elements'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import IPet from '../../types/Pet'
import R from '../../res/R'
import ScrollContainer from '../../components/ScrollContainer'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import IWeight from '../../types/Weight'
import WeightSetup from '../../components/WeightSetup'

const styles = StyleSheet.create({
  bottomButtonsWrapper: {
    ...R.styles.bottomButtonWrapper,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  petWeight: IWeight
}

class PetWeightSetupScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.setupProfile(pet.name),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      petWeight: { id: uuid(), date: new Date(), weightUnit1: 0, weightUnit2: 0 },
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  private nextScreen = (skip: boolean): boolean => {
    const { navigation, handleSavePet } = this.props
    const { pet, petWeight } = this.state

    pet.weightEntries = []

    if (!skip) {
      if (petWeight.weightUnit1 + petWeight.weightUnit2 > 0) {
        pet.weightEntries.push(petWeight)
      }

      handleSavePet(pet)
    }

    return navigation.navigate('PetHealthSetupScreen', { petId: pet.id })
  }

  private saveWeight = (petWeight: IWeight): void => {
    this.setState({ petWeight })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, petWeight } = this.state

    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.ghostWhite }]}>
        <ScrollContainer>
          <View style={R.styles.pushContentOpposite}>
            <WeightSetup pet={pet} petWeight={petWeight} setParentState={this.saveWeight} />
            <View style={styles.bottomButtonsWrapper}>
              <Button
                title={R.strings.ui.skip()}
                buttonStyle={[R.styles.buttons.primaryButtonDisabled, { width: 115 }]}
                titleStyle={R.styles.buttons.primaryButtonTitleDisabled}
                onPress={(): boolean => this.nextScreen(true)}
              />
              <View style={R.styles.dotsWrapper}>
                <View style={R.styles.theDot} />
                <View style={[R.styles.theDot, { backgroundColor: R.colors.pewter }]} />
              </View>
              <Button
                title={R.strings.ui.next()}
                buttonStyle={[R.styles.buttons.primaryButton, { width: 115 }]}
                titleStyle={R.styles.buttons.primaryButtonTitle}
                onPress={(): boolean => this.nextScreen(false)}
                disabled={petWeight.weightUnit1 + petWeight.weightUnit2 <= 0}
              />
            </View>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PetWeightSetupScreen)
