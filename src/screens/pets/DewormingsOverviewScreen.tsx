import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import { Divider } from 'react-native-elements'
import moment from 'moment'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import {
  getVaccinationRemainingTime,
  getVaccinationEndDate,
  isVaccinationEndUpcoming,
  isVaccinationActive,
  isVaccinationPlanned,
  isVaccinationExpired,
} from '../../utilities/helpers'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import DewormingEditForm from '../../components/DewormingEditForm'
import IDeworming from '../../types/Deworming'
import LinkCardWithInfo from '../../components/LinkCardWithInfo'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isDewormingModalOpen: boolean
}

class DewormingsOverviewScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openDewormingModal = navigation.getParam('openDewormingModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.health.dewormingTitle(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openDewormingModal}>
          <AppIcons name="plus" size={24} color={R.colors.cedar} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      isDewormingModalOpen: false,
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  private closeDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: false })
  }

  private openDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: true })
  }

  private saveDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props
    const petToSave = { ...pet, dewormings: [...pet.dewormings, deworming] }

    handleSavePet(petToSave)
    this.setState({ pet: petToSave })
    this.closeDewormingModal()
  }

  private goToDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state
    const { navigation } = this.props

    navigation.navigate('DewormingInformationScreen', { petId: pet.id, dewormingId: deworming.id })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openDewormingModal: this.openDewormingModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isDewormingModalOpen } = this.state
    const today = moment(new Date())
    const activeDewormings = pet.dewormings.filter(isVaccinationActive)
    const plannedDewormings = pet.dewormings.filter(isVaccinationPlanned)
    const expiredDewormings = pet.dewormings.filter(isVaccinationExpired)

    return (
      <View style={R.styles.rootView}>
        <DewormingEditForm pet={pet} onSave={this.saveDeworming} isModalOpen={isDewormingModalOpen} onCancel={this.closeDewormingModal} />
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View
              style={[
                R.styles.pushContentOpposite,
                R.styles.defaultContainer,
                { paddingVertical: R.styles.constants.containerPaddingVertical },
              ]}
            >
              {(!pet.dewormings || pet.dewormings.length === 0) && (
                <TouchableOpacity
                  style={{ ...R.styles.borderRadiuses.large, backgroundColor: R.colors.white, padding: 15 }}
                  onPress={this.openDewormingModal}
                >
                  <View>
                    <Text style={[R.styles.fonts.callout]}>
                      <Text style={[R.styles.fonts.callout, { fontWeight: 'bold' }]}>{pet.name}</Text>
                      {R.strings.pets.health.dewormingPlaceholder()}
                    </Text>
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={[R.styles.fonts.button, { color: R.colors.cedar }]}>{`+ ${R.strings.pets.health.dewormingAdd()}`}</Text>
                  </View>
                </TouchableOpacity>
              )}
              <View>
                {activeDewormings && activeDewormings.length > 0 && (
                  <View>
                    <View>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.active()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {activeDewormings.map(
                        (deworming, index): ReactElement => (
                          <View key={deworming.id}>
                            <LinkCardWithInfo
                              title={deworming.name}
                              subtitle={getVaccinationEndDate(deworming)}
                              bottomText={getVaccinationRemainingTime(deworming)}
                              onPress={(): void => this.goToDeworming(deworming)}
                              noRoundingBottom={activeDewormings.length > 1 && index === 0}
                              noRoundingTop={activeDewormings.length > 1 && index === activeDewormings.length - 1}
                              noRounding={activeDewormings.length > 1 && index !== 0 && index !== activeDewormings.length - 1}
                              isActive
                              isUpcoming={isVaccinationEndUpcoming(deworming)}
                            />
                            {index !== activeDewormings.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
                {plannedDewormings && plannedDewormings.length > 0 && (
                  <View>
                    <View style={[activeDewormings.length > 0 && { marginTop: 24 }]}>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.planned()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {plannedDewormings.map(
                        (deworming, index): ReactElement => (
                          <View key={deworming.id}>
                            <LinkCardWithInfo
                              title={deworming.name}
                              subtitle={getVaccinationEndDate(deworming)}
                              bottomText={R.strings.pets.comingUpIn(moment(deworming.startDate).diff(today, 'days'))}
                              onPress={(): void => this.goToDeworming(deworming)}
                              noRoundingBottom={plannedDewormings.length > 1 && index === 0}
                              noRoundingTop={plannedDewormings.length > 1 && index === plannedDewormings.length - 1}
                              noRounding={plannedDewormings.length > 1 && index !== 0 && index !== plannedDewormings.length - 1}
                            />
                            {index !== plannedDewormings.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
              </View>
              {expiredDewormings && expiredDewormings.length > 0 && (
                <View style={[activeDewormings.length > 0 && { marginTop: 35 }]}>
                  <View>
                    <Text style={[R.styles.fonts.title, { color: R.colors.porpoise }]}>{R.strings.pets.inactive()}</Text>
                  </View>
                  <View style={{ marginTop: 8 }}>
                    {expiredDewormings.map(
                      (deworming, index): ReactElement => (
                        <View key={deworming.id}>
                          <LinkCardWithInfo
                            title={deworming.name}
                            subtitle={getVaccinationEndDate(deworming)}
                            onPress={(): void => this.goToDeworming(deworming)}
                            noRoundingBottom={expiredDewormings.length > 1 && index === 0}
                            noRoundingTop={expiredDewormings.length > 1 && index === expiredDewormings.length - 1}
                            noRounding={expiredDewormings.length > 1 && index !== 0 && index !== expiredDewormings.length - 1}
                          />
                          {index !== expiredDewormings.length - 1 && <Divider style={R.styles.divider} />}
                        </View>
                      ),
                    )}
                  </View>
                </View>
              )}
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DewormingsOverviewScreen)
