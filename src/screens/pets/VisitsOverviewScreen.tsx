import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import { Divider } from 'react-native-elements'
import moment from 'moment'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import VisitEditForm from '../../components/VisitEditForm'
import IVisit from '../../types/Visit'
import LinkCardWithInfo from '../../components/LinkCardWithInfo'
import { formatDateShort, isVisitPlanned, isVisitExpired } from '../../utilities/helpers'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isVisitModalOpen: boolean
}

class VisitsOverviewScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openVisitModal = navigation.getParam('openVisitModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.health.visitTitle(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openVisitModal}>
          <AppIcons name="plus" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      isVisitModalOpen: false,
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  private closeVisitModal = (): void => {
    this.setState({ isVisitModalOpen: false })
  }

  private openVisitModal = (): void => {
    this.setState({ isVisitModalOpen: true })
  }

  private saveVisit = (visit: IVisit): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props
    const petToSave = { ...pet, visits: [...pet.visits, visit] }

    handleSavePet(petToSave)
    this.setState({ pet: petToSave })
    this.closeVisitModal()
  }

  private goToVisit = (visit: IVisit): void => {
    const { pet } = this.state
    const { navigation } = this.props

    navigation.navigate('VisitInformationScreen', { petId: pet.id, visitId: visit.id })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openVisitModal: this.openVisitModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isVisitModalOpen } = this.state
    const today = moment(new Date())
    const plannedVisits = pet.visits.filter(isVisitPlanned)
    const expiredVisits = pet.visits.filter(isVisitExpired)

    return (
      <View style={R.styles.rootView}>
        <VisitEditForm pet={pet} onSave={this.saveVisit} isModalOpen={isVisitModalOpen} onCancel={this.closeVisitModal} />
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View
              style={[
                R.styles.pushContentOpposite,
                R.styles.defaultContainer,
                { paddingVertical: R.styles.constants.containerPaddingVertical },
              ]}
            >
              <View>
                {plannedVisits && plannedVisits.length > 0 && (
                  <View>
                    <View>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.planned()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {plannedVisits.map(
                        (visit, index): ReactElement => (
                          <View key={visit.id}>
                            <LinkCardWithInfo
                              title={visit.name}
                              subtitle={formatDateShort(visit.date)}
                              bottomText={R.strings.pets.comingUpIn(moment(visit.date).diff(today, 'days'))}
                              onPress={(): void => this.goToVisit(visit)}
                              noRoundingBottom={plannedVisits.length > 1 && index === 0}
                              noRoundingTop={plannedVisits.length > 1 && index === plannedVisits.length - 1}
                              noRounding={plannedVisits.length > 1 && index !== 0 && index !== plannedVisits.length - 1}
                            />
                            {index !== plannedVisits.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
              </View>
              {expiredVisits && expiredVisits.length > 0 && (
                <View>
                  <View style={[plannedVisits.length > 0 && { marginTop: 35 }]}>
                    <Text style={[R.styles.fonts.title, { color: R.colors.porpoise }]}>{R.strings.pets.finished()}</Text>
                  </View>
                  <View style={{ marginTop: 8 }}>
                    {expiredVisits.map(
                      (visit, index): ReactElement => (
                        <View key={visit.id}>
                          <LinkCardWithInfo
                            title={visit.name}
                            subtitle={formatDateShort(visit.date)}
                            onPress={(): void => this.goToVisit(visit)}
                            noRoundingBottom={expiredVisits.length > 1 && index === 0}
                            noRoundingTop={expiredVisits.length > 1 && index === expiredVisits.length - 1}
                            noRounding={expiredVisits.length > 1 && index !== 0 && index !== expiredVisits.length - 1}
                          />
                          {index !== expiredVisits.length - 1 && <Divider style={R.styles.divider} />}
                        </View>
                      ),
                    )}
                  </View>
                </View>
              )}
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VisitsOverviewScreen)
