import React, { Component, ReactElement } from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import { TouchableOpacity, View, StyleSheet, Image, Text, Alert } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import moment from 'moment'
import { Divider } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'

import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import R from '../../res/R'
import AppIcons from '../../components/AppIcons'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import DewormingEditForm from '../../components/DewormingEditForm'
import ScrollContainer from '../../components/ScrollContainer'
import IDeworming from '../../types/Deworming'
import {
  getPetDefaultPhoto,
  getVaccinationRemainingTime,
  isVaccinationEndUpcoming,
  isVaccinationExpired,
  isVaccinationPlanned,
  isVaccinationActive,
  formatDateShort,
} from '../../utilities/helpers'
import constants from '../../utilities/constants'
import RemoveButton from '../../components/RemoveButton'

const styles = StyleSheet.create({
  fieldsContainer: {
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
    paddingVertical: R.styles.constants.containerPaddingVertical,
  },
  label: {
    ...R.styles.fonts.body,
    marginTop: 6,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  deworming: IDeworming
  isDewormingModalOpen: boolean
}

class DewormingInformationScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openDewormingModal = navigation.getParam('openDewormingModal', (): void => {})

    return {
      title: R.strings.pets.health.dewormingInfo(),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openDewormingModal}>
          <AppIcons name="edit" size={24} color={R.colors.cedar} />
        </TouchableOpacity>
      ),
    }
  }

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)
    const dewormingId = navigation.getParam('dewormingId', undefined)
    const deworming = _.find(pet.dewormings, { id: dewormingId })!

    this.state = {
      pet,
      deworming,
      isDewormingModalOpen: false,
    }
  }

  private closeDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: false })
  }

  private openDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: true })
  }

  private saveDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props

    pet.dewormings = pet.dewormings.map(
      (v): IDeworming => {
        if (v.id !== deworming.id) {
          return v
        }

        return {
          ...v,
          ...deworming,
        }
      },
    )

    this.setState({ pet })
    this.setState({ deworming })
    handleSavePet(pet)

    this.closeDewormingModal()
  }

  public removeDeworming = (): void => {
    const { navigation, handleSavePet } = this.props
    const { pet, deworming } = this.state

    Alert.alert(
      R.strings.ui.confirmAction(),
      R.strings.ui.doYouRemove(),
      [
        {
          text: R.strings.ui.cancel(),
          style: 'cancel',
        },
        {
          text: R.strings.ui.ok(),
          onPress: (): void => {
            pet.dewormings = pet.dewormings.filter((v): boolean => deworming.id !== v.id)
            handleSavePet(pet)
            navigation.navigate('DewormingsOverviewScreen', { petId: pet.id })
          },
        },
      ],
      { cancelable: false },
    )
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    navigation.setParams({ openDewormingModal: this.openDewormingModal })
  }

  public render(): ReactElement {
    const { pet, deworming, isDewormingModalOpen } = this.state

    const periodicityText = _.find(constants.periodicityDropdownItems, { value: deworming.periodicity })!
    const typeText = _.find(constants.dewormingDropdownItems, { value: deworming.type })!
    const today = new Date()
    const startDate = moment(deworming.startDate)
    const endDate = moment(deworming.startDate).add(deworming.periodicity, 'y')
    const comingUpIn = startDate.diff(today, 'days')
    const isExpired = isVaccinationExpired(deworming)
    const isPlanned = isVaccinationPlanned(deworming)
    const isActive = isVaccinationActive(deworming)
    const isUpcoming = isVaccinationEndUpcoming(deworming)

    let title = ''
    if (isExpired) {
      title = R.strings.pets.inactive()
    } else if (isPlanned) {
      title = R.strings.pets.comingUpIn(comingUpIn)
    } else if (isActive) {
      title = getVaccinationRemainingTime(deworming)
    }

    return (
      <View style={R.styles.rootView}>
        <DewormingEditForm
          pet={pet}
          onSave={this.saveDeworming}
          isModalOpen={isDewormingModalOpen}
          onCancel={this.closeDewormingModal}
          deworming={deworming}
        />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
          <ScrollContainer>
            <View style={{ paddingVertical: R.styles.constants.containerPaddingVertical }}>
              <View>
                <View style={[R.styles.columnCenter, { paddingHorizontal: R.styles.constants.containerPaddingHorizontal }]}>
                  <View style={R.styles.rowCenter}>
                    <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
                    <AppIcons name="deworm" size={20} color={R.colors.cedar} style={{ marginLeft: 10 }} />
                  </View>
                  <Text style={[R.styles.fonts.hero, { marginTop: 13 }]}>{deworming.name}</Text>
                  <View style={[R.styles.rowCenter, { marginTop: 14 }]}>
                    {isActive && (
                      <View style={[R.styles.greenDot, { marginRight: 5 }, isUpcoming && { backgroundColor: R.colors.scarlet }]} />
                    )}
                    <Text style={[R.styles.fonts.button, isExpired && { color: R.colors.porpoise }]}>{title}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider, { marginTop: 17 }]} />
                <View style={styles.fieldsContainer}>
                  <View style={[R.styles.rowCenter]}>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.periodicity()}</Text>
                      <Text style={styles.label}>{periodicityText.text}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.type()}</Text>
                      <Text style={styles.label}>{typeText.text}</Text>
                    </View>
                  </View>
                  <View style={[R.styles.rowCenter, { marginTop: 20 }]}>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.startDate()}</Text>
                      <Text style={styles.label}>{formatDateShort(startDate)}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.endDate()}</Text>
                      <Text style={styles.label}>{formatDateShort(endDate)}</Text>
                    </View>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
                <View style={styles.fieldsContainer}>
                  <View>
                    <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{R.strings.pets.notes()}</Text>
                    <Text style={styles.label}>{deworming.notes}</Text>
                  </View>
                </View>
                <Divider style={[R.styles.divider]} />
              </View>
              <RemoveButton title={R.strings.pets.health.dewormingRemove()} onPress={this.removeDeworming} />
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DewormingInformationScreen)
