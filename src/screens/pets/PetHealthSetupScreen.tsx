import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Text, Alert, TouchableOpacity, Modal } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Button, Divider } from 'react-native-elements'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import ExpandableCard from '../../components/ExpandableCard'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import {
  formatDate,
  getVaccinationRemainingTime,
  showVaccination,
  showSterilization,
  showChip,
  showAllergies,
  showDeworming,
} from '../../utilities/helpers'
import ToggleCard from '../../components/ToggleCard'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import ChipEditForm from '../../components/ChipEditForm'
import IVaccination from '../../types/Vaccination'
import VaccinationEditForm from '../../components/VaccinationEditForm'
import ITreatment from '../../types/Treatment'
import TreatmentEditForm from '../../components/TreatmentEditForm'
import LinkCard from '../../components/LinkCard'
import IDeworming from '../../types/Deworming'
import DewormingEditForm from '../../components/DewormingEditForm'
import VisitEditForm from '../../components/VisitEditForm'
import IVisit from '../../types/Visit'
import BetaPlaceholder from '../../components/BetaPlaceholder'

const styles = StyleSheet.create({
  topSectionWrapper: {
    marginTop: R.styles.constants.containerPaddingVertical,
    alignItems: 'center',
    alignContent: 'center',
  },
  topSectionText: {
    ...R.styles.fonts.body,
    color: R.colors.porpoise,
    textAlign: 'center',
  },
  cardInfo: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  cardInfoWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  bottomButtonsWrapper: {
    ...R.styles.bottomButtonWrapper,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  initialPet: IPet
  isChipModalOpen: boolean
  isVaccinationModalOpen: boolean
  isDewormingModalOpen: boolean
  isTreatmentModalOpen: boolean
  isVisitModalOpen: boolean
  isBetaUpOpen: boolean
}

class PetHealthSetupScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.setupProfile(pet.name),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      initialPet: pet,
      isChipModalOpen: false,
      isVaccinationModalOpen: false,
      isDewormingModalOpen: false,
      isTreatmentModalOpen: false,
      isVisitModalOpen: false,
      isBetaUpOpen: false,
    }
  }

  private savePet = (): void => {
    const { pet } = this.state
    const { handleSavePet, navigation } = this.props

    handleSavePet(pet)

    navigation.navigate('PetsScreen')
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet, initialPet: pet })
  }

  private setSterilization = (value: boolean): void => {
    const { pet } = this.state

    this.setState({
      pet: { ...pet, sterilization: value },
    })
  }

  private switchChip = (switchCallback: (result: boolean) => void): void => {
    const { pet } = this.state

    this.setState({ initialPet: pet })

    if (!pet.chip) {
      this.setState({
        pet: { ...pet, chip: true },
        isChipModalOpen: true,
      })
    } else {
      Alert.alert(
        R.strings.ui.confirmAction(),
        R.strings.pets.health.doYouEraseChipData(),
        [
          {
            text: R.strings.ui.cancel(),
            style: 'cancel',
            onPress: (): void => {
              switchCallback(false)
            },
          },
          {
            text: R.strings.ui.ok(),
            onPress: (): void => {
              const petToSave = {
                ...pet,
                health: { ...pet, chip: false, chipData: { serialNumber: undefined, date: undefined, clinic: undefined } },
              }
              this.setState({
                pet: petToSave,
              })
              switchCallback(true)
            },
          },
        ],
        { cancelable: false },
      )
    }
  }

  private closeChipModal = (): void => {
    const { initialPet } = this.state

    this.setState({ pet: initialPet, isChipModalOpen: false })
  }

  private closeVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: false })
  }

  private closeDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: false })
  }

  private closeTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: false })
  }

  private closeVisitModal = (): void => {
    this.setState({ isVisitModalOpen: false })
  }

  private openChipModal = (): void => {
    const { pet } = this.state

    this.setState({ initialPet: pet, isChipModalOpen: true })
  }

  private openVaccinationModal = (): void => {
    this.setState({ isVaccinationModalOpen: true })
  }

  private openDewormingModal = (): void => {
    this.setState({ isDewormingModalOpen: true })
  }

  private openTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: true })
  }

  private openVisitModal = (): void => {
    this.setState({ isVisitModalOpen: true })
  }

  private saveVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state

    this.setState({ pet: { ...pet, vaccinations: [...pet.vaccinations, vaccination] } })
    this.closeVaccinationModal()
  }

  private removeVaccination = (vaccination: IVaccination): void => {
    const { pet } = this.state

    this.setState({
      pet: { ...pet, vaccinations: pet.vaccinations.filter((t): boolean => vaccination.id !== t.id) },
    })
  }

  private saveDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state

    this.setState({ pet: { ...pet, dewormings: [...pet.dewormings, deworming] } })
    this.closeDewormingModal()
  }

  private removeDeworming = (deworming: IDeworming): void => {
    const { pet } = this.state

    this.setState({
      pet: { ...pet, dewormings: pet.dewormings.filter((t): boolean => deworming.id !== t.id) },
    })
  }

  private saveTreatment = (treatment: ITreatment): void => {
    const { pet } = this.state

    this.setState({ pet: { ...pet, treatments: [...pet.treatments, treatment] } })
    this.closeTreatmentModal()
  }

  private removeTreatment = (treatment: ITreatment): void => {
    const { pet } = this.state

    this.setState({
      pet: { ...pet, treatments: pet.treatments.filter((t): boolean => treatment.id !== t.id) },
    })
  }

  private saveVisit = (visit: IVisit): void => {
    const { pet } = this.state

    this.setState({ pet: { ...pet, visits: [...pet.visits, visit] } })
    this.closeVisitModal()
  }

  private removeVisit = (visit: IVisit): void => {
    const { pet } = this.state

    this.setState({
      pet: { ...pet, visits: pet.visits.filter((t): boolean => visit.id !== t.id) },
    })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const {
      pet,
      isChipModalOpen,
      isVaccinationModalOpen,
      isTreatmentModalOpen,
      isDewormingModalOpen,
      isVisitModalOpen,
      isBetaUpOpen,
    } = this.state
    const { navigation } = this.props

    return (
      <View style={R.styles.rootView}>
        <ChipEditForm
          pet={pet}
          setParentState={(data): void => this.setState({ pet: data })}
          onSave={(): void => this.setState({ isChipModalOpen: false })}
          isModalOpen={isChipModalOpen}
          onCancel={this.closeChipModal}
        />
        <VaccinationEditForm
          pet={pet}
          onSave={this.saveVaccination}
          isModalOpen={isVaccinationModalOpen}
          onCancel={this.closeVaccinationModal}
        />
        <TreatmentEditForm pet={pet} onSave={this.saveTreatment} isModalOpen={isTreatmentModalOpen} onCancel={this.closeTreatmentModal} />
        <DewormingEditForm pet={pet} onSave={this.saveDeworming} isModalOpen={isDewormingModalOpen} onCancel={this.closeDewormingModal} />
        <VisitEditForm pet={pet} onSave={this.saveVisit} isModalOpen={isVisitModalOpen} onCancel={this.closeVisitModal} />
        <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.ghostWhite }]}>
          <ScrollContainer>
            <View style={[R.styles.pushContentOpposite, R.styles.defaultContainer]}>
              <View style={styles.topSectionWrapper}>
                <Text style={R.styles.fonts.hero}>{R.strings.pets.health.healthOf(pet.name)}</Text>
                <Text style={[styles.topSectionText, { marginTop: 10 }]}>{R.strings.pets.health.addHealthRecords()}</Text>
                <Text style={styles.topSectionText}>{R.strings.pets.health.ableToAdd()}</Text>
                {showVaccination(pet) && (
                  <ExpandableCard
                    color={R.colors.aegean}
                    title={R.strings.pets.health.vaccination()}
                    icon="vaccine"
                    onHeaderPress={this.openVaccinationModal}
                  >
                    {pet.vaccinations && pet.vaccinations.length > 0
                      ? pet.vaccinations.map(
                          (vaccination: IVaccination, index: number): ReactElement => (
                            <View key={vaccination.name}>
                              <View style={R.styles.expandableCardContent}>
                                <View>
                                  <Text style={R.styles.fonts.body}>{vaccination.name}</Text>
                                  <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>
                                    {getVaccinationRemainingTime(vaccination)}
                                  </Text>
                                </View>
                                <TouchableOpacity onPress={(): void => this.removeVaccination(vaccination)}>
                                  <AppIcons name="close" size={11} color={R.colors.black} />
                                </TouchableOpacity>
                              </View>
                              {pet.vaccinations.length > 1 && index !== pet.vaccinations.length - 1 && (
                                <Divider style={[R.styles.divider, { marginVertical: 10 }]} />
                              )}
                            </View>
                          ),
                        )
                      : undefined}
                  </ExpandableCard>
                )}
                {showDeworming(pet) && (
                  <ExpandableCard
                    color={R.colors.cedar}
                    title={R.strings.pets.health.deworming()}
                    icon="deworm"
                    onHeaderPress={this.openDewormingModal}
                  >
                    {pet.dewormings && pet.dewormings.length > 0
                      ? pet.dewormings.map(
                          (deworming: IDeworming, index: number): ReactElement => (
                            <View key={deworming.name}>
                              <View style={R.styles.expandableCardContent}>
                                <View>
                                  <Text style={R.styles.fonts.body}>{deworming.name}</Text>
                                  <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>
                                    {getVaccinationRemainingTime(deworming)}
                                  </Text>
                                </View>
                                <TouchableOpacity onPress={(): void => this.removeDeworming(deworming)}>
                                  <AppIcons name="close" size={11} color={R.colors.black} />
                                </TouchableOpacity>
                              </View>
                              {pet.dewormings.length > 1 && index !== pet.dewormings.length - 1 && (
                                <Divider style={[R.styles.divider, { marginVertical: 10 }]} />
                              )}
                            </View>
                          ),
                        )
                      : undefined}
                  </ExpandableCard>
                )}
                <ExpandableCard
                  color={R.colors.emerald}
                  title={R.strings.pets.health.treatment()}
                  icon="treatment"
                  onHeaderPress={this.openTreatmentModal}
                >
                  {pet.treatments && pet.treatments.length > 0
                    ? pet.treatments.map(
                        (treatment: ITreatment, index: number): ReactElement => (
                          <View key={treatment.name}>
                            <View style={R.styles.expandableCardContent}>
                              <View>
                                <Text style={R.styles.fonts.body}>{treatment.name}</Text>
                              </View>
                              <TouchableOpacity onPress={(): void => this.removeTreatment(treatment)}>
                                <AppIcons name="close" size={11} color={R.colors.black} />
                              </TouchableOpacity>
                            </View>
                            {pet.treatments.length > 1 && index !== pet.treatments.length - 1 && (
                              <Divider style={[R.styles.divider, { marginVertical: 10 }]} />
                            )}
                          </View>
                        ),
                      )
                    : undefined}
                </ExpandableCard>
                <LinkCard
                  title={R.strings.pets.health.visitPlan()}
                  onPress={this.openVisitModal}
                  rightElement={<AppIcons name="plus" size={24} color={R.colors.fossil} />}
                />
                {showAllergies(pet) && (
                  <LinkCard
                    title={R.strings.pets.health.allergies()}
                    onPress={(): void => this.setState({ isBetaUpOpen: true })}
                    rightElement={<AppIcons name="plus" size={24} color={R.colors.fossil} />}
                  />
                )}
                {showChip(pet) && (
                  <ToggleCard value={pet.chip} title={R.strings.pets.health.chip()} onAsyncSwitchPress={this.switchChip}>
                    <TouchableOpacity style={styles.cardInfoWrapper} onPress={this.openChipModal}>
                      <View style={styles.cardInfo}>
                        <Text style={R.styles.fonts.body}>{pet.chipData.serialNumber}</Text>
                        {pet.chipData.date && (
                          <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{formatDate(pet.chipData.date)}</Text>
                        )}
                      </View>
                      <View>
                        <AppIcons name="edit" size={24} color={R.colors.porpoise} />
                      </View>
                    </TouchableOpacity>
                  </ToggleCard>
                )}
                {showSterilization(pet) && (
                  <ToggleCard
                    value={pet.sterilization}
                    title={R.strings.pets.health.sterilization()}
                    onSyncSwitchPress={this.setSterilization}
                  />
                )}
              </View>
              <View style={styles.bottomButtonsWrapper}>
                <Button
                  title={R.strings.ui.skip()}
                  buttonStyle={[R.styles.buttons.primaryButtonDisabled, { width: 115 }]}
                  titleStyle={R.styles.buttons.primaryButtonTitleDisabled}
                  onPress={(): boolean => navigation.navigate('PetsScreen')}
                />
                <View style={R.styles.dotsWrapper}>
                  <View style={[R.styles.theDot, { backgroundColor: R.colors.pewter }]} />
                  <View style={R.styles.theDot} />
                </View>
                <Button
                  title={R.strings.pets.addPet()}
                  buttonStyle={[R.styles.buttons.primaryButton, { width: 115 }]}
                  titleStyle={R.styles.buttons.primaryButtonTitle}
                  onPress={this.savePet}
                />
              </View>
            </View>
          </ScrollContainer>
          <Modal
            supportedOrientations={['portrait', 'portrait-upside-down']}
            animationType="fade"
            visible={isBetaUpOpen}
            presentationStyle="overFullScreen"
            transparent
          >
            <BetaPlaceholder closeAction={(): void => this.setState({ isBetaUpOpen: false })} />
          </Modal>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PetHealthSetupScreen)
