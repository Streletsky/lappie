import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Text, Dimensions, TouchableOpacity, Image } from 'react-native'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { LineChart, ChartData, ChartConfig } from 'react-native-chart-kit'
import { NavigationStackOptions } from 'react-navigation-stack'
import _ from 'lodash'
import moment from 'moment'
import { ListItem } from 'react-native-elements'
import { NavigationEventSubscription } from 'react-navigation'
import SafeAreaView from 'react-native-safe-area-view'

import { interpolateBasis, quantize } from 'd3-interpolate'
import IPet from '../../types/Pet'
import R from '../../res/R'
import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import { savePet, IPetsAction } from '../../actions/actionCreators'
import IAppState from '../../types/AppState'
import AppIcons from '../../components/AppIcons'
import SectionRadioButton from '../../components/SectionRadioButton'
import IWeight from '../../types/Weight'
import WeightEditForm from '../../components/WeightEditForm'
import { getPetDefaultPhoto, formatDateShort, getCurrentPetWeight, getWeightText, convertWeightFromLb } from '../../utilities/helpers'

const styles = StyleSheet.create({
  topContainer: {
    ...R.styles.borderRadiuses.large,
    backgroundColor: R.colors.white,
    paddingBottom: 15,
    paddingTop: 20,
    marginTop: 20,
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
})

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  selectedWeightUnitIndex: number
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isWeightModalOpen: boolean
  selectedPeriodIndex: number
}

class WeightOverviewScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openWeightModal = navigation.getParam('openWeightModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.weightOf(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openWeightModal}>
          <AppIcons name="plus" size={24} color={R.colors.amber} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      isWeightModalOpen: false,
      selectedPeriodIndex: 0,
    }
  }

  private savePet = (pet: IPet): void => {
    const { handleSavePet } = this.props

    handleSavePet(pet)
  }

  private onCloseWeightModal = (): void => {
    this.setState({ isWeightModalOpen: false })
  }

  private openWeightModal = (): void => {
    this.setState({ isWeightModalOpen: true })
  }

  private onSaveWeightModal = (weight: IWeight): void => {
    const { pet } = this.state

    pet.weightEntries.push(weight)
    this.savePet(pet)

    this.setState({
      isWeightModalOpen: false,
    })
  }

  private setPeriod = (selectedPeriodIndex: number): void => {
    this.setState({ selectedPeriodIndex })
  }

  private buildChartData = (): ChartData => {
    const { selectedWeightUnitIndex } = this.props
    const { pet, selectedPeriodIndex } = this.state
    const labels: string[] = []
    const weightData = []

    const allMeasurements = _.sortBy(pet.weightEntries, 'date')
    const firstValidEntry = _.first(_.filter(allMeasurements, (weight): boolean => weight && weight.weightUnit1 + weight.weightUnit2 > 0))

    if (!firstValidEntry) {
      return {
        labels,
        datasets: [
          {
            data: [],
            color: (): string => R.colors.cobalt,
            strokeWidth: 1,
          },
        ],
      }
    }

    if (selectedPeriodIndex === 0) {
      const currentMonth = moment()
        .subtract(6, 'months')
        .startOf('month')

      for (let i = 0; i < 6; i++) {
        currentMonth.add(1, 'months')

        const endOfMonth = moment(currentMonth)
          .add(1, 'months')
          .subtract(1, 'days')

        const lastWeightEntryDuringMonth = _.last(
          _.sortBy(
            _.filter(pet.weightEntries, (weight): boolean => {
              const momentDate = moment(weight.date)
              return momentDate.isBetween(currentMonth, endOfMonth) && weight.weightUnit1 + weight.weightUnit2 > 0
            }),
            'date',
          ),
        )

        labels.push(currentMonth.format('MMM'))
        weightData.push(lastWeightEntryDuringMonth && convertWeightFromLb(selectedWeightUnitIndex, lastWeightEntryDuringMonth))
      }
    } else if (selectedPeriodIndex === 1) {
      const currentYear = moment()
        .subtract(6, 'years')
        .startOf('year')

      for (let i = 0; i < 6; i++) {
        currentYear.add(1, 'years')

        const endOfYear = moment(currentYear)
          .add(1, 'years')
          .subtract(1, 'days')

        const lastWeightEntryDuringYear = _.last(
          _.sortBy(
            _.filter(pet.weightEntries, (weight): boolean => {
              const momentDate = moment(weight.date)
              return momentDate.isBetween(currentYear, endOfYear) && weight.weightUnit1 + weight.weightUnit2 > 0
            }),
            'date',
          ),
        )

        labels.push(currentYear.format('YYYY'))

        weightData.push(lastWeightEntryDuringYear && convertWeightFromLb(selectedWeightUnitIndex, lastWeightEntryDuringYear))
      }
    }

    const data = _.map(weightData, (weightEntry): number => (weightEntry ? weightEntry.weightUnit1 + weightEntry.weightUnit2 / 10 : 0))

    /* const linearInterpolator = interpolateBasis(data.filter((d): boolean => d > 0))
    const interpolatedData = quantize(linearInterpolator, 6).map((d: number): number => +d.toFixed(2))

    for (let i = 0; i < weightData.length; i++) {
      const w = weightData[i]
      if (w) {
        interpolatedData[i] = w.weightUnit1 + w.weightUnit2 / 10
      }
    } */

    return {
      labels,
      datasets: [
        {
          data,
          color: (): string => R.colors.cobalt,
          strokeWidth: 1,
        },
      ],
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openWeightModal: this.openWeightModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { isWeightModalOpen, selectedPeriodIndex, pet } = this.state
    const { navigation, selectedWeightUnitIndex } = this.props
    const buttons = ['Monthly', 'Yearly']

    const allMeasurements = _.sortBy(pet.weightEntries, 'date').reverse()
    const lastValidEntry = _.first(_.filter(allMeasurements, (weight): boolean => weight && weight.weightUnit1 + weight.weightUnit2 > 0))

    const chartConfig: ChartConfig = {
      color: (): string => R.colors.porpoise,
      labelColor: (): string => R.colors.porpoise,
      backgroundColor: R.colors.white,
      backgroundGradientFrom: R.colors.white,
      backgroundGradientTo: R.colors.white,
      decimalPlaces: 1,
    }

    const chartWidth = Dimensions.get('window').width - R.styles.constants.containerPaddingHorizontal * 4 - 2

    return (
      <View style={R.styles.rootView}>
        <WeightEditForm pet={pet} onCancel={this.onCloseWeightModal} isModalOpen={isWeightModalOpen} onSave={this.onSaveWeightModal} />
        <SafeAreaView style={[R.styles.safeArea, R.styles.defaultContainer]}>
          <ScrollContainer>
            <View style={styles.topContainer}>
              <View style={R.styles.rowCenter}>
                {lastValidEntry && (
                  <Image
                    source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)}
                    style={[R.styles.smallAvatar, { width: 64, height: 64, borderRadius: 32 }]}
                  />
                )}
                {!lastValidEntry && <AppIcons name="weight" size={27} color={R.colors.black} style={{ marginTop: 10 }} />}
              </View>
              <View style={[R.styles.rowCenter, { marginTop: 9 }]}>
                {!lastValidEntry && <Text style={R.styles.fonts.hero}>{R.strings.pets.notMeasured()}</Text>}
                {lastValidEntry && <Text style={R.styles.fonts.hero}>{getCurrentPetWeight(pet, selectedWeightUnitIndex)}</Text>}
              </View>
              <View style={[{ marginTop: 22 }, !lastValidEntry && { opacity: 0.5 }]}>
                <SectionRadioButton
                  disabled={!lastValidEntry}
                  handleSelection={this.setPeriod}
                  selectedIndex={selectedPeriodIndex}
                  buttons={buttons}
                  width="100%"
                />
              </View>
              <View style={{ marginTop: 10 }}>
                <LineChart
                  data={this.buildChartData()}
                  width={chartWidth}
                  height={185}
                  chartConfig={chartConfig}
                  fromZero
                  style={{ ...R.styles.defaultBorder, ...(!lastValidEntry && { opacity: 0.5 }) }}
                />
                {!lastValidEntry && (
                  <TouchableOpacity style={{ position: 'absolute', paddingTop: 10, paddingHorizontal: 32 }} onPress={this.openWeightModal}>
                    <View style={R.styles.rowCenter}>
                      <AppIcons name="plus" size={44} color={R.colors.amber} />
                    </View>
                    <View style={R.styles.rowCenter}>
                      <Text style={[R.styles.fonts.callout, { marginTop: 15, textAlign: 'center' }]}>
                        {R.strings.pets.noMeasurementsHelp()}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              </View>
            </View>
            <View>
              {allMeasurements && allMeasurements.length > 0 ? (
                <View style={{ paddingBottom: R.styles.constants.containerPaddingVertical }}>
                  <View style={{ marginTop: 28 }}>
                    <Text style={R.styles.fonts.title}>{R.strings.pets.measurements()}</Text>
                  </View>
                  {allMeasurements.map(
                    (measurement): ReactElement => (
                      <View key={measurement.id}>
                        <ListItem
                          title={getWeightText(measurement, selectedWeightUnitIndex)}
                          subtitle={formatDateShort(measurement.date)}
                          onPress={(): boolean => navigation.navigate('WeightDetailsScreen', { petId: pet.id, weightId: measurement.id })}
                          titleStyle={R.styles.fonts.body}
                          subtitleStyle={[R.styles.fonts.callout, { color: R.colors.porpoise }]}
                          containerStyle={[R.styles.listItem, { marginTop: 10 }]}
                          rightElement={<AppIcons name="chevron" size={16} color={R.colors.fossil} />}
                        />
                      </View>
                    ),
                  )}
                </View>
              ) : (
                undefined
              )}
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    selectedWeightUnitIndex: state.settings.selectedWeightUnitIndex,
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id })!,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WeightOverviewScreen)
