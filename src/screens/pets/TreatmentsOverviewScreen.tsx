import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { NavigationStackOptions } from 'react-navigation-stack'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { NavigationEventSubscription } from 'react-navigation'
import _ from 'lodash'
import { Divider } from 'react-native-elements'
import moment from 'moment'
import SafeAreaView from 'react-native-safe-area-view'

import { INavigatableComponentProps } from '../../types/NavigatableComponentProps'
import ScrollContainer from '../../components/ScrollContainer'
import R from '../../res/R'
import IPet from '../../types/Pet'
import { IPetsAction, savePet } from '../../actions/actionCreators'
import AppIcons from '../../components/AppIcons'
import IAppState from '../../types/AppState'
import TreatmentEditForm from '../../components/TreatmentEditForm'
import ITreatment from '../../types/Treatment'
import { getTreatmentPeriod, isTreatmentActive, isTreatmentPlanned, isTreatmentExpired, formatDateShort } from '../../utilities/helpers'
import LinkCardWithInfo from '../../components/LinkCardWithInfo'

interface IDispatchFromProps {
  handleSavePet(pet: IPet): void
}

interface IStateFromProps {
  fetchPet(id: string): IPet
}

interface IComponentState {
  pet: IPet
  isTreatmentModalOpen: boolean
}

class TreatmentsOverviewScreen extends Component<IDispatchFromProps & IStateFromProps & INavigatableComponentProps, IComponentState> {
  public static navigationOptions = ({ navigation }: INavigatableComponentProps): NavigationStackOptions => {
    const openTreatmentModal = navigation.getParam('openTreatmentModal', (): void => {})
    const pet = navigation.getParam('pet', { name: '' })

    return {
      title: R.strings.pets.health.treatmentTitle(pet.name),
      headerRight: (): ReactElement => (
        <TouchableOpacity style={R.styles.headerRightButton} onPress={openTreatmentModal}>
          <AppIcons name="plus" size={24} color={R.colors.emerald} />
        </TouchableOpacity>
      ),
    }
  }

  private focusListener?: NavigationEventSubscription

  public constructor(props: IDispatchFromProps & IStateFromProps & INavigatableComponentProps) {
    super(props)

    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })

    this.state = {
      pet,
      isTreatmentModalOpen: false,
    }
  }

  private getPet = (): void => {
    const { navigation, fetchPet } = this.props
    const petId = navigation.getParam('petId', undefined)
    const pet = fetchPet(petId)

    navigation.setParams({ pet })
    this.setState({ pet })
  }

  private closeTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: false })
  }

  private openTreatmentModal = (): void => {
    this.setState({ isTreatmentModalOpen: true })
  }

  private saveTreatment = (treatment: ITreatment): void => {
    const { pet } = this.state
    const { handleSavePet } = this.props
    const petToSave = { ...pet, treatments: [...pet.treatments, treatment] }

    handleSavePet(petToSave)
    this.setState({ pet: petToSave })
    this.closeTreatmentModal()
  }

  private goToTreatment = (treatment: ITreatment): void => {
    const { pet } = this.state
    const { navigation } = this.props

    navigation.navigate('TreatmentInformationScreen', { petId: pet.id, treatmentId: treatment.id })
  }

  public componentDidMount = (): void => {
    const { navigation } = this.props

    this.focusListener = navigation.addListener('didFocus', this.getPet)
    navigation.setParams({ openTreatmentModal: this.openTreatmentModal })
  }

  public componentWillUnmount = (): void => {
    if (this.focusListener) {
      this.focusListener.remove()
    }
  }

  public render(): ReactElement {
    const { pet, isTreatmentModalOpen } = this.state
    const today = moment(new Date())
    const activeTreatments = pet.treatments.filter(isTreatmentActive)
    const plannedTreatments = pet.treatments.filter(isTreatmentPlanned)
    const expiredTreatments = pet.treatments.filter(isTreatmentExpired)

    return (
      <View style={R.styles.rootView}>
        <TreatmentEditForm pet={pet} onSave={this.saveTreatment} isModalOpen={isTreatmentModalOpen} onCancel={this.closeTreatmentModal} />
        <SafeAreaView style={R.styles.safeArea}>
          <ScrollContainer>
            <View
              style={[
                R.styles.pushContentOpposite,
                R.styles.defaultContainer,
                { paddingVertical: R.styles.constants.containerPaddingVertical },
              ]}
            >
              <View>
                {(!pet.treatments || pet.treatments.length === 0) && (
                  <TouchableOpacity
                    style={{ ...R.styles.borderRadiuses.large, backgroundColor: R.colors.white, padding: 15 }}
                    onPress={this.openTreatmentModal}
                  >
                    <View>
                      <Text style={[R.styles.fonts.callout]}>
                        <Text style={[R.styles.fonts.callout, { fontWeight: 'bold' }]}>{pet.name}</Text>
                        {R.strings.pets.health.treatmentPlaceholder()}
                      </Text>
                    </View>
                    <View style={{ marginTop: 10 }}>
                      <Text style={[R.styles.fonts.button, { color: R.colors.emerald }]}>
                        {`+ ${R.strings.pets.health.treatmentAdd()}`}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
                {activeTreatments && activeTreatments.length > 0 && (
                  <View>
                    <View>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.active()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {activeTreatments.map(
                        (treatment, index): ReactElement => (
                          <View key={treatment.id}>
                            <LinkCardWithInfo
                              title={treatment.name}
                              subtitle={getTreatmentPeriod(treatment)}
                              onPress={(): void => this.goToTreatment(treatment)}
                              noRoundingBottom={activeTreatments.length > 1 && index === 0}
                              noRoundingTop={activeTreatments.length > 1 && index === activeTreatments.length - 1}
                              noRounding={activeTreatments.length > 1 && index !== 0 && index !== activeTreatments.length - 1}
                              isActive
                            />
                            {index !== activeTreatments.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
                {plannedTreatments && plannedTreatments.length > 0 && (
                  <View>
                    <View style={[activeTreatments.length > 0 && { marginTop: 24 }]}>
                      <Text style={R.styles.fonts.title}>{R.strings.pets.planned()}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                      {plannedTreatments.map(
                        (treatment, index): ReactElement => (
                          <View key={treatment.id}>
                            <LinkCardWithInfo
                              title={treatment.name}
                              subtitle={formatDateShort(treatment.startDate)}
                              bottomText={R.strings.pets.comingUpIn(moment(treatment.startDate).diff(today, 'days'))}
                              onPress={(): void => this.goToTreatment(treatment)}
                              noRoundingBottom={plannedTreatments.length > 1 && index === 0}
                              noRoundingTop={plannedTreatments.length > 1 && index === plannedTreatments.length - 1}
                              noRounding={plannedTreatments.length > 1 && index !== 0 && index !== plannedTreatments.length - 1}
                            />
                            {index !== plannedTreatments.length - 1 && <Divider style={R.styles.divider} />}
                          </View>
                        ),
                      )}
                    </View>
                  </View>
                )}
              </View>
              {expiredTreatments && expiredTreatments.length > 0 && (
                <View>
                  <View style={[(activeTreatments.length > 0 || plannedTreatments.length > 0) && { marginTop: 35 }]}>
                    <Text style={[R.styles.fonts.title, { color: R.colors.porpoise }]}>{R.strings.pets.finished()}</Text>
                  </View>
                  <View style={{ marginTop: 8 }}>
                    {expiredTreatments.map(
                      (treatment, index): ReactElement => (
                        <View key={treatment.id}>
                          <LinkCardWithInfo
                            title={treatment.name}
                            subtitle={formatDateShort(treatment.endDate)}
                            onPress={(): void => this.goToTreatment(treatment)}
                            noRoundingBottom={expiredTreatments.length > 1 && index === 0}
                            noRoundingTop={expiredTreatments.length > 1 && index === expiredTreatments.length - 1}
                            noRounding={expiredTreatments.length > 1 && index !== 0 && index !== expiredTreatments.length - 1}
                          />
                          {index !== expiredTreatments.length - 1 && <Divider style={R.styles.divider} />}
                        </View>
                      ),
                    )}
                  </View>
                </View>
              )}
            </View>
          </ScrollContainer>
        </SafeAreaView>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleSavePet: (pet: IPet): IPetsAction => {
    return dispatch(savePet(pet))
  },
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    fetchPet: (id: string): IPet => _.find(state.pets.pets, { id }),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TreatmentsOverviewScreen)
