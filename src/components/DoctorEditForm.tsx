import React, { ReactElement, PureComponent } from 'react'
import { View } from 'react-native'
import { Divider } from 'react-native-elements'

import IDoctor from '../types/Doctor'
import R from '../res/R'
import TextInputField from './TextInputField'
import TextInputFieldMultiline from './TextInputFieldMultiline'

interface IComponentProps {
  doctor: IDoctor
  setParentState(doctor: IDoctor): void
}

class DoctorEditForm extends PureComponent<IComponentProps> {
  public render(): ReactElement {
    const { doctor, setParentState } = this.props

    return (
      <View>
        <View style={{ marginTop: R.styles.constants.containerPaddingVertical }}>
          <TextInputField
            label={R.strings.doctors.name()}
            onChangeText={(text): void => {
              setParentState({ ...doctor, name: text })
            }}
            value={doctor.name}
            maxLength={40}
            autoCompleteType="name"
            returnKeyType="done"
            isRequired
          />
          <Divider style={R.styles.divider} />
          <TextInputField
            label={R.strings.doctors.clinic()}
            onChangeText={(text): void => {
              setParentState({ ...doctor, clinic: text })
            }}
            value={doctor.clinic}
            maxLength={40}
            returnKeyType="done"
          />
          <Divider style={R.styles.divider} />
          <TextInputField
            label={R.strings.doctors.phone()}
            onChangeText={(text): void => {
              setParentState({ ...doctor, phone: text })
            }}
            value={doctor.phone}
            maxLength={20}
            autoCompleteType="tel"
            textContentType="telephoneNumber"
            keyboardType="phone-pad"
            returnKeyType="done"
            isRequired
          />
        </View>
        <View style={{ marginTop: 17 }}>
          <TextInputFieldMultiline
            label={R.strings.doctors.address()}
            onChangeText={(text): void => {
              setParentState({ ...doctor, address: text })
            }}
            value={doctor.address}
            maxLength={180}
            autoCompleteType="street-address"
          />
          <Divider style={R.styles.divider} />
          <TextInputFieldMultiline
            label={R.strings.doctors.notes()}
            onChangeText={(text): void => {
              setParentState({ ...doctor, notes: text })
            }}
            value={doctor.notes}
            maxLength={350}
          />
        </View>
      </View>
    )
  }
}

export default DoctorEditForm
