import React, { Component, ReactElement } from 'react'
import { StyleSheet, View, Text, Animated } from 'react-native'

import IPet from '../types/Pet'
import IWeight from '../types/Weight'
import featureChecker from '../utilities/featureChecker'
import { getPetDefaultPhoto } from '../utilities/helpers'
import WeightPicker from './WeightPicker'
import R from '../res/R'
import constants from '../utilities/constants'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    marginTop: R.styles.constants.containerPaddingVertical,
    alignItems: 'center',
    alignContent: 'center',
  },
  image: {
    borderRadius: 85,
    resizeMode: 'cover',
  },
  imageSection: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: R.styles.constants.containerPaddingVertical,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  weightPickerWrapper: {
    width: '100%',
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
})

interface IComponentProps {
  pet: IPet
  petWeight: IWeight
  setParentState(weight: IWeight): void
}

interface IComponentState {
  imageSize: Animated.Value
  windowWidth: number
  windowHeight: number
}

export default class WeightSetup extends Component<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      imageSize: new Animated.Value(featureChecker.getScreenWidth() > constants.bigScreenSize ? 170 : 80),
      windowWidth: featureChecker.getScreenWidth(),
      windowHeight: featureChecker.getScreenHeight(),
    }
  }

  private recalculateDimensions = (): void => {
    const windowWidth = featureChecker.getScreenWidth()
    const windowHeight = featureChecker.getScreenHeight()

    this.setState({ windowWidth, windowHeight })
  }

  public render(): ReactElement {
    const { setParentState, pet, petWeight } = this.props
    const { imageSize, windowWidth, windowHeight } = this.state

    const isBigScreen = windowWidth > constants.bigScreenSize || windowHeight > constants.bigScreenSize
    const imageSectionMinHeight = isBigScreen ? 300 : 120

    const imageMinSize = isBigScreen ? 170 : 80
    const imageMaxSize = isBigScreen ? 300 : 120

    return (
      <View style={styles.container} onLayout={this.recalculateDimensions}>
        <Text style={R.styles.fonts.hero}>{R.strings.pets.weightOf(pet.name)}</Text>
        <View style={[styles.imageSection, { minHeight: imageSectionMinHeight }]}>
          <Animated.Image
            source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)}
            style={[
              styles.image,
              {
                height: imageMinSize,
                width: imageMinSize,
                borderRadius: imageMinSize / 2,
                transform: [
                  {
                    scale: imageSize.interpolate({
                      inputRange: [imageMinSize, imageMaxSize],
                      outputRange: [1, imageMaxSize / imageMinSize],
                    }),
                  },
                ],
              },
            ]}
          />
        </View>
        <View style={styles.weightPickerWrapper}>
          <WeightPicker
            imageMaxSize={imageMaxSize}
            imageMinSize={imageMinSize}
            pet={pet}
            weight={petWeight}
            resultImageSize={imageSize}
            setParentState={setParentState}
          />
        </View>
      </View>
    )
  }
}
