import React, { ReactElement, Component } from 'react'
import {
  ViewStyle,
  Text,
  View,
  TextInput,
  StyleSheet,
  Keyboard,
  TouchableOpacity,
  NativeSyntheticEvent,
  TextInputFocusEventData,
} from 'react-native'
import { InputProps } from 'react-native-elements'

import R from '../res/R'
import featureChecker from '../utilities/featureChecker'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    minHeight: R.styles.constants.listItemMinHeight,
    flexDirection: 'row',
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 17,
    paddingRight: 14,
    alignItems: 'center',
  },
  input: {
    ...R.styles.fonts.body,
    paddingTop: 0,
    paddingBottom: 0,
    flex: 1,
    minHeight: 21,
    lineHeight: 20,
    color: R.colors.black,
    textAlignVertical: featureChecker.isAndroid() ? 'center' : 'top',
  },
})

interface IComponentProps extends InputProps {
  containerStyle?: ViewStyle
  label: string
  textAlign: 'center' | 'auto' | 'left' | 'right' | 'justify'
  isRequired: boolean
}

interface IComponentState {
  isFocused: boolean
}

class TextInputField extends Component<IComponentProps, IComponentState> {
  private textInput?: TextInput

  public static defaultProps = {
    editable: true,
    textAlign: 'right',
    isRequired: false,
  }

  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      isFocused: false,
    }
  }

  private focusInput = (): void => {
    const { isFocused } = this.state

    if (!isFocused) {
      setTimeout((): void => {
        if (this.textInput) {
          this.textInput.focus()
        }
      }, 0)
    }
  }

  private handleFocus = (event: NativeSyntheticEvent<TextInputFocusEventData>): void => {
    const { onFocus } = this.props

    this.setState({ isFocused: true })

    if (onFocus) {
      onFocus(event)
    }
  }

  private handleBlur = (event: NativeSyntheticEvent<TextInputFocusEventData>): void => {
    const { onBlur } = this.props

    this.setState({ isFocused: false })

    if (onBlur) {
      onBlur(event)
    }
  }

  private handleKeyboardDidHide = (): void => {
    if (this.textInput) {
      this.textInput.blur()
    }
  }

  public componentDidMount = (): void => {
    Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide)
  }

  public componentWillUnmount = (): void => {
    Keyboard.removeListener('keyboardDidHide', this.handleKeyboardDidHide)
  }

  public render(): ReactElement {
    const { isFocused } = this.state
    const { label, editable, textAlign, containerStyle, isRequired, ...otherProps } = this.props

    return (
      <TouchableOpacity onPress={this.focusInput}>
        <View
          style={[
            styles.container,
            containerStyle,
            isFocused ? { borderBottomColor: R.colors.cobalt } : { borderBottomColor: R.colors.darkCloud },
          ]}
        >
          <Text
            style={[
              R.styles.fonts.body,
              isFocused ? { color: R.colors.cobalt } : { color: R.colors.porpoise },
              !editable && { color: R.colors.porpoise },
            ]}
          >
            {label}
          </Text>
          {isRequired && <View style={R.styles.requiredIndicator} />}
          <TextInput
            ref={(input): void => {
              this.textInput = input || undefined
            }}
            {...otherProps}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            editable={editable}
            disableFullscreenUI
            pointerEvents="auto"
            underlineColorAndroid="transparent"
            style={[styles.input, { textAlign }]}
            clearButtonMode="while-editing"
          />
        </View>
      </TouchableOpacity>
    )
  }
}

export default TextInputField
