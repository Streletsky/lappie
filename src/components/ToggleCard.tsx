import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps } from 'react-native'

import R from '../res/R'
import ToggleSwitch from './ToggleSwitch'

const styles = StyleSheet.create({
  container: {
    ...R.styles.borderRadiuses.large,
    marginTop: R.styles.constants.cardMargin,
    width: '100%',
    backgroundColor: R.colors.white,
  },
  cardHeader: {
    ...R.styles.borderRadiuses.large,
    minHeight: R.styles.constants.listItemMinHeight,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    backgroundColor: R.colors.white,
    paddingHorizontal: 16,
  },
  childrenWrapper: {
    flexDirection: 'column',
    justifyContent: 'center',
    minHeight: R.styles.constants.listItemMinHeight,
    paddingHorizontal: 16,
    marginBottom: 8,
  },
})

interface IComponentProps extends ViewProps {
  value: boolean
  onSyncSwitchPress?(value: boolean): void
  title: string
  children?: ReactElement | ReactElement[]
  onAsyncSwitchPress?(callback: (result: boolean) => void): void
}

const ToggleCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { value, onSyncSwitchPress, title, children, onAsyncSwitchPress } = props

  return (
    <View style={styles.container}>
      <View style={[styles.cardHeader, value && children && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
        <Text style={R.styles.fonts.headline}>{title}</Text>
        <ToggleSwitch value={value} onSyncPress={onSyncSwitchPress} onAsyncPress={onAsyncSwitchPress} />
      </View>
      {value && children && <View style={styles.childrenWrapper}>{children}</View>}
    </View>
  )
}

export default ToggleCard
