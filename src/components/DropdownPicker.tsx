import React, { Component, ReactElement } from 'react'
import { View, Text, ViewStyle, TouchableOpacity, Picker } from 'react-native'
import { Overlay } from 'react-native-elements'

import { WheelPicker } from 'react-native-wheel-picker-android'
import R from '../res/R'
import AppIcons from './AppIcons'
import IDropdownItem from '../types/DropdownItem'
import featureChecker from '../utilities/featureChecker'

interface IComponentProps {
  setParentState(value: string | number | undefined): void
  onOpen?(): void
  title: string
  selectedValue?: string | number
  containerStyle?: ViewStyle
  items: IDropdownItem[]
  isRequired: boolean
}

interface IComponentState {
  isPickerOpened: boolean
}

export default class DropdownPicker extends Component<IComponentProps, IComponentState> {
  public static defaultProps = {
    isRequired: false,
  }

  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      isPickerOpened: false,
    }
  }

  private renderPicker = (): ReactElement => {
    const { selectedValue, items, setParentState } = this.props
    const selectedIndex = selectedValue ? items.findIndex((item): boolean => item.value === selectedValue) : 0

    return (
      <View style={[R.styles.rowCenter, { paddingVertical: 10 }]}>
        <View>
          {featureChecker.isIos() && (
            <Picker selectedValue={selectedValue} onValueChange={setParentState}>
              {items.map(
                (item): ReactElement => (
                  <Picker.Item label={item.text} value={item.value} key={item.value} />
                ),
              )}
            </Picker>
          )}
          {featureChecker.isAndroid() && (
            <WheelPicker
              hideIndicator
              itemTextSize={22}
              itemTextFontFamily={R.styles.fontFamily.fontFamily}
              selectedItemTextSize={22}
              selectedItemTextFontFamily={R.styles.fontFamily.fontFamily}
              selectedItem={selectedIndex}
              data={items.map((item): string => item.text)}
              onItemSelected={(index): void => setParentState(items[index].value)}
            />
          )}
        </View>
      </View>
    )
  }

  private onFieldPress = (): void => {
    const { isPickerOpened } = this.state
    const { onOpen } = this.props

    this.setState({ isPickerOpened: !isPickerOpened })

    if (onOpen && !isPickerOpened) {
      onOpen()
    }
  }

  public render(): ReactElement {
    const { title, containerStyle, selectedValue, items, isRequired } = this.props
    const { isPickerOpened } = this.state
    const selectedIndex = items.findIndex((item): boolean => item.value === selectedValue)
    const selectedItemText = selectedIndex >= 0 ? items[selectedIndex].text : undefined

    return (
      <View style={containerStyle}>
        <TouchableOpacity onPress={this.onFieldPress}>
          <View style={R.styles.genericField}>
            <View style={R.styles.rowCenter}>
              <Text style={R.styles.inputLabelStyle}>{title}</Text>
              {isRequired && <View style={R.styles.requiredIndicator} />}
            </View>
            <View style={R.styles.rowCenter}>
              <Text>{selectedItemText}</Text>
              <AppIcons name="dropdown" size={16} color={R.colors.porpoise} style={{ marginLeft: 9 }} />
            </View>
          </View>
        </TouchableOpacity>
        <Overlay
          animationType="fade"
          isVisible={isPickerOpened}
          overlayStyle={R.styles.overlay}
          onBackdropPress={(): void => this.setState({ isPickerOpened: false })}
        >
          {this.renderPicker()}
        </Overlay>
      </View>
    )
  }
}
