import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'

const styles = StyleSheet.create({
  title: {
    ...R.styles.fonts.star,
    color: R.colors.black,
    textAlign: 'left',
    flex: 1,
  },
  cardWrapper: {
    paddingVertical: 14,
    ...R.styles.borderRadiuses.large,
    width: '100%',
    minHeight: 88,
    backgroundColor: R.colors.white,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  cardHeaderWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    alignContent: 'center',
  },
  childrenWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    alignContent: 'center',
    marginTop: 6,
  },
  rightArrowWrapper: {
    width: 44,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
})

interface IComponentProps extends ViewProps {
  color: string
  title: string
  icon: string
  placeholder: string
  onCardPress(): void
  expired?: boolean
  children?: ReactElement | ReactElement[]
}

const ExpandableLinkCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { color, title, icon, style, children, onCardPress, placeholder, expired } = props

  return (
    <TouchableOpacity onPress={onCardPress} style={[{ marginTop: R.styles.constants.cardMargin }, style]}>
      <View style={[styles.cardWrapper]}>
        <View style={{ width: 53, alignItems: 'center' }}>
          <AppIcons name={icon} size={32} color={color} />
        </View>
        <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
          <View style={styles.cardHeaderWrapper}>
            <Text style={styles.title}>{title}</Text>
          </View>
          {children && (
            <View style={styles.childrenWrapper}>
              <View style={[R.styles.greenDot, expired && { backgroundColor: R.colors.scarlet }]} />
              {children}
            </View>
          )}
          {!children && (
            <View style={styles.childrenWrapper}>
              <Text style={[R.styles.fonts.body, { color: R.colors.porpoise }]}>{placeholder}</Text>
            </View>
          )}
        </View>
        <View style={styles.rightArrowWrapper}>
          <AppIcons name="chevron" size={16} color={R.colors.darkCloud} />
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default ExpandableLinkCard
