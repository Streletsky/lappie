import React, { ReactElement } from 'react'
import { StyleSheet } from 'react-native'
import { BlurView } from 'expo-blur'

import { BottomTabBar } from 'react-navigation-tabs'
import R from '../res/R'
import featureChecker from '../utilities/featureChecker'

const styles = StyleSheet.create({
  blurView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  bottomTabBar: {
    backgroundColor: R.colors.white,
    borderTopColor: R.colors.darkCloud,
    borderTopWidth: 1,
  },
})

const BlurBottomTabBar = (props): ReactElement =>
  featureChecker.isIos() ? (
    <BlurView tint="default" intensity={95} style={styles.blurView}>
      <BottomTabBar {...props} style={styles.bottomTabBar} />
    </BlurView>
  ) : (
    <BottomTabBar {...props} />
  )

export default BlurBottomTabBar

/* Proper blur is not supported yet in android */
