import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Animated, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import { INavigatableComponentProps } from '../types/NavigatableComponentProps'
import featureChecker from '../utilities/featureChecker'
import R from '../res/R'
import PetType from '../types/PetType'

const styles = StyleSheet.create({
  item: {
    borderRadius: 40,
    width: 80,
    height: 80,
    alignSelf: 'center',
    shadowColor: R.colors.black,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 2,
    margin: 5,
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
  },
  text: {
    ...R.styles.fonts.headline,
    color: R.colors.white,
    marginTop: 48,
  },
  closeButton: {
    width: 44,
    height: 44,
    backgroundColor: R.colors.amber,
    alignItems: 'center',
    borderRadius: 22,
    justifyContent: 'center',
    paddingTop: featureChecker.isIos() ? 2 : 0, // cross icon position fix for iOS
    paddingLeft: featureChecker.isIos() ? 1 : 0,
  },
  menuWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: R.colors.white,
  },
})

interface IComponentProps {
  closeAction: () => void
}

interface IComponentState {
  topInner: Animated.Value
  topOuter: Animated.Value
  bottomInner: Animated.Value
  bottomOuter: Animated.Value
  left: Animated.Value
  right: Animated.Value
}

const AnimatedButton = Animated.createAnimatedComponent(View)

export default class PetCreationMenu extends Component<IComponentProps & INavigatableComponentProps, IComponentState> {
  private windowWidth: number = 0

  private windowHeight: number = 0

  private horizontalMargin: number = 0

  private verticalMargin: number = 0

  private verticalMarginOuter: number = 0

  public constructor(props: IComponentProps & INavigatableComponentProps) {
    super(props)

    this.getScreenDimensions()

    this.state = {
      topInner: new Animated.Value(this.windowHeight / 2 - 40),
      topOuter: new Animated.Value(this.windowHeight / 2 - 40),
      bottomInner: new Animated.Value(this.windowHeight / 2 - 40),
      bottomOuter: new Animated.Value(this.windowHeight / 2 - 40),
      left: new Animated.Value(this.windowWidth / 2 - 40),
      right: new Animated.Value(this.windowWidth / 2 - 40),
    }
  }

  private createPet = (type: PetType): void => {
    const { navigation, closeAction } = this.props

    closeAction()

    navigation.navigate('PetProfileSetupScreen', { type })
  }

  private getScreenDimensions = (): void => {
    this.windowWidth = featureChecker.getScreenWidth()
    this.windowHeight = featureChecker.getScreenHeight()
    this.horizontalMargin = featureChecker.isPortrait()
      ? featureChecker.getScreenHeight() > 828
        ? this.windowWidth / 2 - 160
        : 40
      : featureChecker.getScreenWidth() < 1024
      ? 140
      : this.windowHeight / 2 - 100
    this.verticalMargin = featureChecker.isPortrait()
      ? featureChecker.getScreenHeight() > 828
        ? this.windowHeight / 2 - 200
        : 180
      : featureChecker.getScreenWidth() < 1024
      ? 80
      : this.windowHeight / 2 - 150
    this.verticalMarginOuter = featureChecker.isPortrait()
      ? featureChecker.getScreenHeight() > 828
        ? this.windowHeight / 2 - 250
        : 100
      : featureChecker.getScreenWidth() < 1024
      ? 40
      : this.windowHeight / 2 - 200
  }

  private recalculateDimensions = (): void => {
    const { topInner, topOuter, bottomInner, bottomOuter, right, left } = this.state

    this.getScreenDimensions()

    Animated.parallel([
      Animated.spring(topInner, {
        toValue: this.verticalMargin,
      }),
      Animated.spring(topOuter, {
        toValue: this.verticalMarginOuter,
      }),
      Animated.spring(bottomInner, {
        toValue: this.verticalMargin,
      }),
      Animated.spring(bottomOuter, {
        toValue: this.verticalMarginOuter,
      }),
      Animated.spring(left, {
        toValue: this.horizontalMargin,
      }),
      Animated.spring(right, {
        toValue: this.horizontalMargin,
      }),
    ]).start()
  }

  public render(): ReactElement {
    const { topInner, topOuter, bottomInner, bottomOuter, left, right } = this.state
    const { closeAction } = this.props

    return (
      <View style={styles.menuWrapper} onLayout={this.recalculateDimensions}>
        <AnimatedButton style={{ position: 'absolute', top: topInner, left }}>
          <TouchableOpacity style={[styles.item, { backgroundColor: R.colors.cat }]} onPress={(): void => this.createPet(PetType.Cat)}>
            <ImageBackground source={R.images.typeCat} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.cat()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>

        <AnimatedButton style={{ position: 'absolute', top: topOuter }}>
          <TouchableOpacity style={[styles.item, { backgroundColor: R.colors.dog }]} onPress={(): void => this.createPet(PetType.Dog)}>
            <ImageBackground source={R.images.typeDog} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.dog()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>

        <AnimatedButton style={{ position: 'absolute', top: topInner, right }}>
          <TouchableOpacity
            style={[styles.item, { backgroundColor: R.colors.rabbit }]}
            onPress={(): void => this.createPet(PetType.Rabbit)}
          >
            <ImageBackground source={R.images.typeRabbit} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.rabbit()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>

        <TouchableOpacity style={styles.closeButton} onPress={closeAction}>
          <Ionicons name="ios-close" size={32} color={R.colors.white} />
        </TouchableOpacity>

        <AnimatedButton style={{ position: 'absolute', bottom: bottomInner, right }}>
          <TouchableOpacity
            style={[styles.item, { backgroundColor: R.colors.rodent }]}
            onPress={(): void => this.createPet(PetType.Rodent)}
          >
            <ImageBackground source={R.images.typeRodent} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.rodent()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>

        <AnimatedButton style={{ position: 'absolute', bottom: bottomOuter }}>
          <TouchableOpacity style={[styles.item, { backgroundColor: R.colors.fish }]} onPress={(): void => this.createPet(PetType.Fish)}>
            <ImageBackground source={R.images.typeFish} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.fish()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>

        <AnimatedButton style={{ position: 'absolute', bottom: bottomInner, left }}>
          <TouchableOpacity style={[styles.item, { backgroundColor: R.colors.bird }]} onPress={(): void => this.createPet(PetType.Bird)}>
            <ImageBackground source={R.images.typeBird} style={styles.image}>
              <Text style={styles.text}>{R.strings.pets.bird()}</Text>
            </ImageBackground>
          </TouchableOpacity>
        </AnimatedButton>
      </View>
    )
  }
}
