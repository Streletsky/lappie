import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity, ViewStyle } from 'react-native'
import { CheckBox } from 'react-native-elements'

import R from '../res/R'

const styles = StyleSheet.create({
  cardHeader: {
    ...R.styles.borderRadiuses.large,
    ...R.styles.defaultBorder,
    minHeight: R.styles.constants.listItemMinHeight,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    backgroundColor: R.colors.white,
    padding: 11,
    marginTop: 8,
  },
  checkboxContainer: {
    ...R.styles.reset,
    backgroundColor: 'transparent',
  },
})

interface IComponentProps extends ViewProps {
  title: string
  onPress(): void
  containerStyle?: ViewStyle
  isDone?: boolean
}

const ActionTrackerCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { title, onPress, containerStyle, isDone } = props

  return (
    <TouchableOpacity onPress={onPress} style={[{ width: '100%' }, containerStyle]}>
      <View style={[styles.cardHeader]}>
        <View>
          <CheckBox
            title={title}
            size={22}
            containerStyle={styles.checkboxContainer}
            textStyle={[
              R.styles.fonts.body,
              { marginLeft: 5 },
              isDone && { textDecorationLine: 'line-through', textDecorationStyle: 'solid' },
            ]}
            checked={isDone === true}
            iconType="ionicon"
            checkedIcon="ios-checkbox"
            uncheckedIcon="ios-square-outline"
            checkedColor={R.colors.black}
            onPress={onPress}
          />
        </View>
        {!isDone && <Text style={[R.styles.fonts.body, { color: R.colors.porpoise }]}>{R.strings.pets.markDone()}</Text>}
      </View>
    </TouchableOpacity>
  )
}

export default ActionTrackerCard
