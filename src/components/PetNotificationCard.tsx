import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity, ViewStyle, Image } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'
import IPetNotification from '../types/PetNotification'
import PetNotificationType from '../types/PetNotificationType'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import PetNotificationSubType from '../types/PetNotificationSubType'

const styles = StyleSheet.create({
  container: {
    ...R.styles.borderRadiuses.large,
    ...R.styles.cardShadowSmaller,
    width: '100%',
    flexDirection: 'column',
    alignContent: 'space-between',
    minHeight: 64,
    backgroundColor: R.colors.white,
    paddingTop: 11,
    paddingBottom: 13,
    paddingHorizontal: 13,
    marginTop: R.styles.constants.cardMargin,
  },
  cardHeaderWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  smallAvatar: {
    width: 32,
    height: 32,
    borderRadius: 16,
    resizeMode: 'cover',
    marginRight: 9,
  },
  cardTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 17,
  },
  cardTitleText: {
    flexWrap: 'wrap',
    alignItems: 'center',
    flex: 1,
  },
})

interface IComponentProps extends ViewProps {
  notification: IPetNotification
  pet: IPet
  onPress?: Function
  containerStyle?: ViewStyle
  customContent?: ReactElement | ReactElement[]
}

const PetNotificationCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { notification, onPress, containerStyle, pet, customContent } = props
  let color = R.colors.aegean
  let icon = 'vaccine'
  let headerTitle = R.strings.pets.health.vaccination()

  switch (notification.type) {
    case PetNotificationType.Vaccination:
      color = R.colors.aegean
      icon = 'vaccine'
      headerTitle = R.strings.pets.health.vaccination()
      break
    case PetNotificationType.Deworming:
      color = R.colors.cedar
      icon = 'deworm'
      headerTitle = R.strings.pets.health.deworming()
      break
    case PetNotificationType.Treatment:
      color = R.colors.poison
      icon = 'treatment'
      headerTitle = R.strings.pets.health.treatment()
      break
    default:
      break
  }

  return (
    <TouchableOpacity onPress={(): void => onPress()} style={[styles.container, containerStyle]}>
      <View style={[styles.cardHeaderWrapper]}>
        <View style={R.styles.rowCenter}>
          <AppIcons name={icon} size={20} color={color} />
          <Text style={[R.styles.fonts.headline, { color, marginLeft: 6 }]}>{headerTitle}</Text>
        </View>
        <AppIcons name="chevron" size={14} color={R.colors.fossil} />
      </View>
      <View style={styles.cardTitle}>
        <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={styles.smallAvatar} />
        <View style={styles.cardTitleText}>
          <Text style={[R.styles.fonts.headline]}>{notification.title}</Text>
        </View>
      </View>
      {notification.caption && (
        <View style={{ marginTop: 9 }}>
          <Text style={[R.styles.fonts.body]}>{notification.caption}</Text>
        </View>
      )}
      {notification.subType === PetNotificationSubType.NoActiveVaccination && (
        <View style={{ marginTop: 10 }}>
          <Text style={[R.styles.fonts.button, { color: R.colors.aegean }]}>{`+ ${R.strings.pets.health.vaccinationAdd()}`}</Text>
        </View>
      )}
      {notification.subType === PetNotificationSubType.NoActiveDeworming && (
        <View style={{ marginTop: 10 }}>
          <Text style={[R.styles.fonts.button, { color: R.colors.cedar }]}>{`+ ${R.strings.pets.health.dewormingAdd()}`}</Text>
        </View>
      )}
      {customContent}
    </TouchableOpacity>
  )
}

export default PetNotificationCard
