import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Text, Picker, TouchableOpacity, ViewStyle } from 'react-native'
import { Overlay } from 'react-native-elements'

import { WheelPicker } from 'react-native-wheel-picker-android'
import R from '../res/R'
import AppIcons from './AppIcons'
import IPet from '../types/Pet'
import { getPetAge } from '../utilities/helpers'
import featureChecker from '../utilities/featureChecker'

const styles = StyleSheet.create({
  agePickersWrapper: {
    height: 210,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  pickerWheelHeader: {
    ...R.styles.fonts.body,
    textAlign: 'center',
    marginVertical: 10,
  },
})

interface IComponentProps {
  setParentState(pet: IPet): void
  pet: IPet
  containerStyle?: ViewStyle
}

interface IComponentState {
  isAgePickerOpened: boolean
  ageMonths: number
  ageYears: number
}

export default class AgePicker extends Component<IComponentProps, IComponentState> {
  private years: number[] = []

  private months: number[] = []

  public constructor(props: IComponentProps) {
    super(props)

    const { pet } = props

    const years = pet.ageMonths && pet.ageMonths > 0 ? Math.floor(pet.ageMonths / 12) : 0
    const months = pet.ageMonths ? pet.ageMonths - years * 12 : 0
    this.state = {
      isAgePickerOpened: false,
      ageMonths: months,
      ageYears: years,
    }

    for (let month = 0; month <= 11; month++) {
      this.months.push(month)
    }

    for (let year = 0; year <= 19; year++) {
      this.years.push(year)
    }
  }

  private onAgePress = (): void => {
    const { isAgePickerOpened } = this.state

    this.setState({ isAgePickerOpened: !isAgePickerOpened })
  }

  public render(): ReactElement {
    const { setParentState, pet, containerStyle } = this.props
    const { isAgePickerOpened, ageMonths, ageYears } = this.state

    return (
      <View>
        <TouchableOpacity onPress={this.onAgePress}>
          <View style={[R.styles.genericField, containerStyle]}>
            <Text style={R.styles.inputLabelStyle}>{R.strings.pets.age()}</Text>
            <View style={R.styles.rowCenter}>
              <Text>{getPetAge(pet)}</Text>
              <AppIcons name="calendar" size={16} color={R.colors.porpoise} style={{ marginLeft: 9 }} />
            </View>
          </View>
        </TouchableOpacity>
        <Overlay
          animationType="fade"
          isVisible={isAgePickerOpened}
          overlayStyle={R.styles.overlay}
          onBackdropPress={(): void => this.setState({ isAgePickerOpened: false })}
        >
          <View style={styles.agePickersWrapper}>
            <View>
              <Text style={styles.pickerWheelHeader}>{R.strings.pets.years()}</Text>
              {featureChecker.isIos() && (
                <Picker
                  selectedValue={ageYears}
                  onValueChange={(value): void => {
                    this.setState({ ageYears: value })
                    setParentState({ ...pet, ageMonths: value * 12 + ageMonths, ageEnteredOn: new Date() })
                  }}
                >
                  {this.years.map(
                    (year: number): ReactElement => (
                      <Picker.Item label={year.toString()} value={year} key={year} />
                    ),
                  )}
                </Picker>
              )}
              {featureChecker.isAndroid() && (
                <WheelPicker
                  hideIndicator
                  itemTextSize={22}
                  itemTextFontFamily={R.styles.fontFamily.fontFamily}
                  selectedItemTextSize={22}
                  selectedItemTextFontFamily={R.styles.fontFamily.fontFamily}
                  selectedItem={ageYears}
                  data={this.years.map((w): string => w.toString())}
                  onItemSelected={(value): void => {
                    this.setState({ ageYears: value })
                    setParentState({ ...pet, ageMonths: value * 12 + ageMonths, ageEnteredOn: new Date() })
                  }}
                />
              )}
            </View>
            <View>
              <Text style={styles.pickerWheelHeader}>{R.strings.pets.months()}</Text>
              {featureChecker.isIos() && (
                <Picker
                  selectedValue={ageMonths}
                  onValueChange={(value): void => {
                    this.setState({ ageMonths: value })
                    setParentState({ ...pet, ageMonths: ageYears * 12 + value, ageEnteredOn: new Date() })
                  }}
                >
                  {this.months.map(
                    (month: number): ReactElement => (
                      <Picker.Item label={month.toString()} value={month} key={month} />
                    ),
                  )}
                </Picker>
              )}
              {featureChecker.isAndroid() && (
                <WheelPicker
                  hideIndicator
                  itemTextSize={22}
                  itemTextFontFamily={R.styles.fontFamily.fontFamily}
                  selectedItemTextSize={22}
                  selectedItemTextFontFamily={R.styles.fontFamily.fontFamily}
                  selectedItem={ageMonths}
                  data={this.months.map((w): string => w.toString())}
                  onItemSelected={(value): void => {
                    this.setState({ ageMonths: value })
                    setParentState({ ...pet, ageMonths: ageYears * 12 + value, ageEnteredOn: new Date() })
                  }}
                />
              )}
            </View>
          </View>
        </Overlay>
      </View>
    )
  }
}
