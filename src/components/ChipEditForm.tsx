import React, { ReactElement, PureComponent } from 'react'
import { View, Image, Text } from 'react-native'
import { Divider } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import DatePicker from './DatePicker'
import BottomModal from './BottomModal'

interface IComponentProps {
  pet: IPet
  onSave(): void
  onCancel?(): void
  isModalOpen: boolean
  setParentState(pet: IPet): void
}

class ChipEditForm extends PureComponent<IComponentProps> {
  public render(): ReactElement {
    const { pet, isModalOpen, onSave, onCancel, setParentState } = this.props

    return (
      <BottomModal
        title={R.strings.pets.health.addChip()}
        onCancel={onCancel}
        saveButtonEnabled={(pet.chipData && pet.chipData.serialNumber && pet.chipData.serialNumber.length > 0) === true}
        visible={isModalOpen}
        onSave={onSave}
      >
        <View>
          <View style={R.styles.modalTopSection}>
            <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
            <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
          </View>
          <TextInputField
            label={R.strings.pets.health.serialNumber()}
            onChangeText={(text): void => {
              setParentState({ ...pet, chipData: { ...pet.chipData, serialNumber: text } })
            }}
            value={pet.chipData && pet.chipData.serialNumber}
            maxLength={20}
            returnKeyType="done"
            isRequired
          />
          <Divider style={R.styles.divider} />
          <TextInputField
            label={R.strings.doctors.clinic()}
            onChangeText={(text): void => {
              setParentState({ ...pet, chipData: { ...pet.chipData, clinic: text } })
            }}
            value={pet.chipData && pet.chipData.clinic}
            maxLength={40}
            returnKeyType="done"
          />
          <Divider style={R.styles.divider} />
          <DatePicker
            date={pet.chipData && pet.chipData.date}
            title={R.strings.pets.date()}
            setParentState={(date): void => setParentState({ ...pet, chipData: { ...pet.chipData, date } })}
          />
        </View>
      </BottomModal>
    )
  }
}

export default ChipEditForm
