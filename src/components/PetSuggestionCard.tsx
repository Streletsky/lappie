import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity, ViewStyle, Image } from 'react-native'

import { LinearGradient } from 'expo-linear-gradient'
import R from '../res/R'
import AppIcons from './AppIcons'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import IPetSuggestion from '../types/PetSuggestion'
import PetSuggestionType from '../types/PetSuggestionType'

const styles = StyleSheet.create({
  container: {
    ...R.styles.borderRadiuses.large,
    ...R.styles.cardShadow,
    width: '100%',
    marginBottom: R.styles.constants.cardMargin,
  },
  gradientWrapper: {
    ...R.styles.borderRadiuses.large,
    flex: 1,
    flexDirection: 'row',
    alignContent: 'space-between',
    minHeight: 64,
    backgroundColor: R.colors.white,
    paddingVertical: 14,
    paddingHorizontal: 14,
  },
  cardHeaderWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  smallAvatar: {
    width: 32,
    height: 32,
    borderRadius: 16,
    resizeMode: 'cover',
    marginRight: 9,
  },
  cardTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardTitleText: {
    flexWrap: 'wrap',
    alignItems: 'center',
    flex: 1,
    maxWidth: 240,
  },
})

interface IComponentProps extends ViewProps {
  suggestion: IPetSuggestion
  pet: IPet
  onPress?: () => void
  containerStyle?: ViewStyle
}

const PetSuggestionCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { suggestion, onPress, pet, containerStyle } = props
  let colors: string[] = []
  let icon = 'close'
  switch (suggestion.type) {
    case PetSuggestionType.AddFirstWeight:
      colors = R.colors.gradient.fill.mauve
      icon = 'plus'
      break
    case PetSuggestionType.CheckWater:
      colors = R.colors.gradient.fill.honeycomb
      icon = 'close'
      break
    case PetSuggestionType.PlayWithPet:
      colors = R.colors.gradient.fill.honeycomb
      icon = 'close'
      break
    default:
      break
  }

  return (
    <TouchableOpacity onPress={onPress} style={[styles.container, containerStyle]}>
      <View>
        <LinearGradient
          colors={colors}
          start={R.colors.gradient.direction.diagonalReverse.start}
          end={R.colors.gradient.direction.diagonalReverse.end}
          style={[styles.gradientWrapper]}
        >
          <View style={R.styles.columnCenter}>
            <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={styles.smallAvatar} />
          </View>
          <View style={styles.cardTitle}>
            <View style={styles.cardTitleText}>
              <Text style={[R.styles.fonts.body, { color: R.colors.white }]}>{suggestion.title}</Text>
            </View>
          </View>
          <View style={R.styles.columnCenter}>
            <AppIcons name={icon} size={24} color={R.colors.white} />
          </View>
        </LinearGradient>
      </View>
    </TouchableOpacity>
  )
}

export default PetSuggestionCard
