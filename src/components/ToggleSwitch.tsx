import React, { ReactElement } from 'react'
import { ViewProps, StyleSheet } from 'react-native'
import Switch from 'react-native-switch-pro'

import R from '../res/R'

const styles = StyleSheet.create({
  circle: {
    shadowColor: R.colors.black,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 2,
  },
})

interface IComponentProps extends ViewProps {
  disabled?: boolean
  value: boolean
  onSyncPress?(result: boolean): void
  onAsyncPress?(callback: () => void): void
}

const ToggleSwitch = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { value, style, onSyncPress, disabled, onAsyncPress } = props

  return (
    <Switch
      height={31}
      width={57}
      disabled={disabled}
      backgroundActive={R.colors.cobalt}
      backgroundInactive={R.colors.darkCloud}
      value={value}
      style={style}
      circleStyle={styles.circle}
      onSyncPress={onAsyncPress ? undefined : onSyncPress}
      onAsyncPress={onAsyncPress}
    />
  )
}

export default ToggleSwitch
