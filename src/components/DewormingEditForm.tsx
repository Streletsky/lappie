import React, { ReactElement, PureComponent } from 'react'
import { View, Image, Text } from 'react-native'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import { Divider } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import DatePicker from './DatePicker'
import BottomModal from './BottomModal'
import DropdownPicker from './DropdownPicker'
import constants from '../utilities/constants'
import TextInputFieldMultiline from './TextInputFieldMultiline'
import DewormingType from '../types/DewormingType'
import IDeworming from '../types/Deworming'

interface IComponentProps {
  pet: IPet
  onSave(deworming: IDeworming): void
  onCancel?(): void
  isModalOpen: boolean
  deworming?: IDeworming
}

interface IComponentState {
  deworming: IDeworming
}

class DewormingEditForm extends PureComponent<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    const { deworming } = this.props

    if (deworming) {
      this.state = { deworming }
    } else {
      this.state = {
        deworming: {
          id: uuid(),
          name: '',
          startDate: new Date(),
          periodicity: 1,
          notes: '',
          type: DewormingType.Pill,
        },
      }
    }
  }

  private onCancel = (): void => {
    const { onCancel } = this.props

    if (onCancel) {
      onCancel()
    }

    this.resetDeworming()
  }

  private onSave = (deworming: IDeworming): void => {
    const { onSave } = this.props

    if (onSave) {
      onSave(deworming)
    }

    this.resetDeworming()
  }

  private resetDeworming = (): void => {
    const { deworming } = this.props

    if (deworming) {
      this.setState({ deworming })
    } else {
      this.setState({
        deworming: {
          id: uuid(),
          name: '',
          startDate: new Date(),
          periodicity: 1,
          notes: '',
          type: DewormingType.Pill,
        },
      })
    }
  }

  public render(): ReactElement {
    const { pet, isModalOpen, deworming: existingDeworming } = this.props
    const { deworming } = this.state

    return (
      <BottomModal
        title={existingDeworming ? R.strings.pets.health.dewormingEdit() : R.strings.pets.health.dewormingAdd()}
        onCancel={this.onCancel}
        saveButtonEnabled={(deworming.name && deworming.name.length > 0 && deworming.startDate && deworming.periodicity > 0) === true}
        visible={isModalOpen}
        onSave={(): void => this.onSave(deworming)}
        accentColor={R.colors.cedar}
      >
        <View>
          <View>
            {pet && (
              <View style={R.styles.modalTopSection}>
                <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
                <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
              </View>
            )}
            <TextInputField
              label={R.strings.pets.name()}
              onChangeText={(text): void => {
                this.setState({ deworming: { ...deworming, name: text } })
              }}
              value={deworming.name}
              maxLength={40}
              returnKeyType="done"
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DropdownPicker
              title={R.strings.pets.type()}
              selectedValue={deworming.type}
              items={constants.dewormingDropdownItems}
              setParentState={(value): void => this.setState({ deworming: { ...deworming, type: value } })}
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DatePicker
              date={deworming.startDate}
              title={R.strings.pets.date()}
              setParentState={(date): void => this.setState({ deworming: { ...deworming, startDate: date } })}
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DropdownPicker
              title={R.strings.pets.periodicity()}
              selectedValue={deworming.periodicity}
              items={constants.periodicityDropdownItems}
              setParentState={(value): void => this.setState({ deworming: { ...deworming, periodicity: value } })}
              isRequired
            />
            <TextInputFieldMultiline
              label={R.strings.pets.notes()}
              onChangeText={(text): void => {
                this.setState({ deworming: { ...deworming, notes: text } })
              }}
              containerStyle={{ marginTop: 32 }}
              value={deworming.notes}
              maxLength={350}
            />
          </View>
        </View>
      </BottomModal>
    )
  }
}

export default DewormingEditForm
