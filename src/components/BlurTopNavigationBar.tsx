import React, { ReactElement } from 'react'
import { StyleSheet } from 'react-native'
import { BlurView } from 'expo-blur'
import { Header } from 'react-navigation-stack'

import featureChecker from '../utilities/featureChecker'

const styles = StyleSheet.create({
  blurView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
})

const BlurTopNavigationBar = (props): ReactElement =>
  featureChecker.isIos() ? (
    <BlurView tint="light" intensity={95} style={styles.blurView}>
      <Header {...props} />
    </BlurView>
  ) : (
    <Header {...props} />
  )

export default BlurTopNavigationBar

/* Proper blur is not supported yet in android */
