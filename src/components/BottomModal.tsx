import React, { Component, ReactElement } from 'react'
import { Animated, Modal, View, Text, StyleSheet } from 'react-native'
import { Button } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'

import R from '../res/R'
import ScrollContainer from './ScrollContainer'
import KeyboardShift from './KeyboardShift'
import featureChecker from '../utilities/featureChecker'

const styles = StyleSheet.create({
  contentWrapper: {
    backgroundColor: 'rgba(0,0,0, .6)',
    flex: 1,
    width: '100%',
    height: '100%',
  },
  animatedContainer: {
    flex: 1,
    position: 'absolute',
    height: '100%',
    width: '100%',
    zIndex: 100,
    backgroundColor: R.colors.ghostWhite,
    overflow: 'hidden',
    borderTopLeftRadius: 9,
    borderTopRightRadius: 9,
  },
  modalHeader: {
    height: 50,
    backgroundColor: R.colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: R.colors.darkCloud,
    borderBottomWidth: 1,
  },
})

interface IComponentProps {
  children: ReactElement | ReactElement[]
  title: string
  onSave?(): void
  onCancel?(): void
  saveButtonEnabled?: boolean
  saveButtonHidden?: boolean
  visible: boolean
  accentColor: string
}

interface IComponentState {
  top: Animated.Value
}

const AnimatedContainer = Animated.createAnimatedComponent(View)

export default class BottomModal extends Component<IComponentProps, IComponentState> {
  private screenHeight = featureChecker.getScreenHeight()

  public static defaultProps = {
    saveButtonEnabled: true,
    accentColor: R.colors.amber,
  }

  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      top: new Animated.Value(this.screenHeight),
    }
  }

  private close = (): void => {
    const { onCancel } = this.props

    this.animateClose(onCancel)
  }

  private save = (): void => {
    const { onSave, saveButtonEnabled } = this.props

    if (!saveButtonEnabled) return

    this.animateClose(onSave)
  }

  private animateShow = (): void => {
    const { top } = this.state

    Animated.spring(top, {
      toValue: featureChecker.isPortrait() ? 80 : 40,
    }).start()
  }

  private animateClose = (callback: Animated.EndCallback | undefined): void => {
    const { top } = this.state

    Animated.timing(top, {
      toValue: this.screenHeight,
      duration: 200,
    }).start(callback)
  }

  private onOrientationChange = (): void => {
    this.screenHeight = featureChecker.getScreenHeight()

    this.animateShow()
  }

  public render(): ReactElement {
    const { top } = this.state
    const { children, title, saveButtonEnabled, visible, accentColor, saveButtonHidden } = this.props

    return (
      <Modal
        animationType="none"
        visible={visible}
        presentationStyle="overFullScreen"
        transparent
        onShow={this.animateShow}
        onOrientationChange={this.onOrientationChange}
        supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
      >
        <View style={styles.contentWrapper}>
          <AnimatedContainer style={{ ...styles.animatedContainer, top }}>
            <SafeAreaView style={styles.modalHeader}>
              <Button
                title={R.strings.ui.cancel()}
                type="outline"
                buttonStyle={R.styles.buttons.textButton}
                titleStyle={[R.styles.fonts.button, { color: accentColor }]}
                onPress={this.close}
              />
              <Text style={R.styles.fonts.headline}>{title}</Text>
              <Button
                title={R.strings.ui.save()}
                buttonStyle={[
                  saveButtonEnabled
                    ? { ...R.styles.buttons.smallButton, backgroundColor: accentColor }
                    : R.styles.buttons.smallButtonDisabled,
                  { marginRight: 17 },
                  saveButtonHidden && { opacity: 0 },
                ]}
                titleStyle={saveButtonEnabled ? R.styles.buttons.smallButtonTitle : R.styles.buttons.smallButtonTitleDisabled}
                onPress={this.save}
                disabled={!saveButtonEnabled}
              />
            </SafeAreaView>
            <SafeAreaView style={R.styles.safeArea}>
              <ScrollContainer>
                <View style={{ paddingBottom: 80, flex: 1 }}>
                  <KeyboardShift isNeedExtraHeight>{children}</KeyboardShift>
                </View>
              </ScrollContainer>
            </SafeAreaView>
          </AnimatedContainer>
        </View>
      </Modal>
    )
  }
}
