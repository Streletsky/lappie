import React, { Component, ReactElement } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-elements'
import SafeAreaView from 'react-native-safe-area-view'

import R from '../res/R'
import ScrollContainer from './ScrollContainer'
import AppIcons from './AppIcons'

interface IComponentProps {
  closeAction: () => void
}

export default class BetaPlaceholder extends Component<IComponentProps> {
  public render(): ReactElement {
    const { closeAction } = this.props

    return (
      <SafeAreaView style={[R.styles.safeArea, { backgroundColor: R.colors.white }]}>
        <ScrollContainer>
          <View style={{ paddingHorizontal: 17, paddingVertical: 5 }}>
            <TouchableOpacity onPress={closeAction}>
              <AppIcons name="close" size={12} color={R.colors.black} style={{ textAlign: 'right' }} />
            </TouchableOpacity>
          </View>
          <View style={[R.styles.pushContentOpposite, { backgroundColor: R.colors.white }]}>
            <View style={{ paddingHorizontal: 40 }}>
              <View style={R.styles.columnCenter}>
                <Text style={{ fontSize: 48 }}>💫</Text>
                <Text style={[R.styles.fonts.headline, { marginTop: 15, textAlign: 'center' }]}>
                  Puff is in beta version and some features may not be available yet.
                </Text>
                <Text style={[R.styles.fonts.body, { marginTop: 10, textAlign: 'center' }]}>
                  Stay with us and get more functions with updates!
                </Text>
              </View>
              <View style={{ marginTop: 20 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                >
                  <Text style={{ fontSize: 32 }}>✂️</Text>
                  <Text style={[R.styles.fonts.headline, { marginLeft: 15 }]}>Care</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                >
                  <Text style={{ fontSize: 32 }}>🥘</Text>
                  <Text style={[R.styles.fonts.headline, { marginLeft: 15 }]}>Food</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                >
                  <Text style={{ fontSize: 32 }}>🦠</Text>
                  <Text style={[R.styles.fonts.headline, { marginLeft: 15 }]}>Allergies</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                >
                  <Text style={{ fontSize: 32 }}>📄</Text>
                  <Text style={[R.styles.fonts.headline, { marginLeft: 15 }]}>Documents</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                >
                  <Text style={{ fontSize: 32 }}>📤</Text>
                  <Text style={[R.styles.fonts.headline, { marginLeft: 15 }]}>Share and Export</Text>
                </View>
              </View>
              <View style={[R.styles.columnCenter, { marginTop: 10 }]}>
                <Text style={{ fontSize: 64 }}>🧶</Text>
                <Text style={R.styles.fonts.headline}>and much more</Text>
              </View>
            </View>
            <View style={R.styles.bottomButtonWrapper}>
              <Button
                title="Ok, got it!"
                buttonStyle={R.styles.buttons.primaryButton}
                titleStyle={R.styles.buttons.primaryButtonTitle}
                onPress={closeAction}
              />
            </View>
          </View>
        </ScrollContainer>
      </SafeAreaView>
    )
  }
}
