import React, { ReactElement, PureComponent } from 'react'
import { View, Image, Text } from 'react-native'
import { Divider } from 'react-native-elements'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import DatePicker from './DatePicker'
import IVisit from '../types/Visit'
import BottomModal from './BottomModal'
import TextInputFieldMultiline from './TextInputFieldMultiline'

interface IComponentProps {
  pet: IPet
  onSave(visit: IVisit): void
  onCancel?(): void
  isModalOpen: boolean
  visit?: IVisit
}

interface IComponentState {
  visit: IVisit
}

class VisitEditForm extends PureComponent<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    const { visit, pet } = this.props

    if (visit) {
      this.state = { visit }
    } else {
      this.state = {
        visit: {
          id: uuid(),
          name: `${R.strings.pets.health.visit()} ${pet.visits.length + 1}`,
          date: new Date(),
          clinic: '',
          notes: '',
        },
      }
    }
  }

  private onCancel = (): void => {
    const { onCancel } = this.props

    if (onCancel) {
      onCancel()
    }

    this.resetVisit()
  }

  private onSave = (visit: IVisit): void => {
    const { onSave } = this.props

    if (onSave) {
      onSave(visit)
    }

    this.resetVisit()
  }

  private resetVisit = (): void => {
    const { visit, pet } = this.props

    if (visit) {
      this.setState({ visit })
    } else {
      this.setState({
        visit: {
          id: uuid(),
          name: `${R.strings.pets.health.visit()} ${pet.visits.length + 1}`,
          date: new Date(),
          clinic: '',
          notes: '',
        },
      })
    }
  }

  public render(): ReactElement {
    const { pet, isModalOpen, visit: existingVisit } = this.props
    const { visit } = this.state

    return (
      <BottomModal
        title={existingVisit ? R.strings.pets.health.visitEdit() : R.strings.pets.health.visitAdd()}
        onCancel={this.onCancel}
        saveButtonEnabled={(visit.name && visit.name.length > 0 && visit.date && visit.clinic && visit.clinic.length > 0) === true}
        visible={isModalOpen}
        onSave={(): void => this.onSave(visit)}
        accentColor={R.colors.amber}
      >
        <View>
          <View>
            <View style={R.styles.modalTopSection}>
              <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
              <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
            </View>
            <TextInputField
              label={R.strings.pets.name()}
              onChangeText={(text): void => {
                this.setState({ visit: { ...visit, name: text } })
              }}
              value={visit.name}
              maxLength={40}
              returnKeyType="done"
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DatePicker
              date={visit.date}
              title={R.strings.pets.date()}
              setParentState={(date): void => this.setState({ visit: { ...visit, date } })}
              isRequired
            />
            <Divider style={R.styles.divider} />
            <TextInputField
              label={R.strings.doctors.clinic()}
              onChangeText={(text): void => {
                this.setState({ visit: { ...visit, clinic: text } })
              }}
              value={visit.clinic}
              maxLength={40}
              returnKeyType="done"
              isRequired
            />
            <TextInputFieldMultiline
              label={R.strings.pets.notes()}
              onChangeText={(text): void => {
                this.setState({ visit: { ...visit, notes: text } })
              }}
              containerStyle={{ marginTop: 32 }}
              value={visit.notes}
              maxLength={350}
            />
          </View>
        </View>
      </BottomModal>
    )
  }
}

export default VisitEditForm
