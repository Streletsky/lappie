import React, { Component, ReactElement } from 'react'
import { Animated, Dimensions, Keyboard, TextInput, UIManager, KeyboardEvent } from 'react-native'

import featureChecker from '../utilities/featureChecker'

interface IComponentProps {
  isNeedExtraHeight?: boolean
  children: ReactElement | ReactElement[]
}

interface IComponentState {
  shift: Animated.Value
}

export default class KeyboardShift extends Component<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      shift: new Animated.Value(0),
    }
  }

  private handleKeyboardDidShow = (event: KeyboardEvent): void => {
    const { isNeedExtraHeight } = this.props
    const { shift } = this.state
    const { height: windowHeight } = Dimensions.get('window')
    const keyboardHeight = event.endCoordinates.height
    const { State: TextInputState } = TextInput
    const currentlyFocusedField = TextInputState.currentlyFocusedField()

    let extraHeight = 18
    if (isNeedExtraHeight) {
      extraHeight = featureChecker.isPortrait() ? (featureChecker.isIos() ? 20 : 30) : featureChecker.isIos() ? 30 : 36
    }

    UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY): void => {
      const fieldHeight = height
      const fieldTop = pageY
      const gap = windowHeight - keyboardHeight - (fieldTop + fieldHeight) - extraHeight

      if (gap < 0) {
        Animated.timing(shift, {
          toValue: gap,
          duration: 300,
          useNativeDriver: true,
        }).start()
      }
    })
  }

  private handleKeyboardDidHide = (): void => {
    const { shift } = this.state

    Animated.timing(shift, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start()
  }

  public componentDidMount = (): void => {
    Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow)
    Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide)
  }

  public componentWillUnmount = (): void => {
    Keyboard.removeListener('keyboardDidShow', this.handleKeyboardDidShow)
    Keyboard.removeListener('keyboardDidHide', this.handleKeyboardDidHide)
  }

  public render(): ReactElement {
    const { children } = this.props
    const { shift } = this.state

    return <Animated.View style={{ flex: 1, transform: [{ translateY: shift }] }}>{children}</Animated.View>
  }
}
