import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity, ViewStyle } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'

const styles = StyleSheet.create({
  cardHeader: {
    ...R.styles.borderRadiuses.large,
    minHeight: R.styles.constants.listItemMinHeight,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    backgroundColor: R.colors.white,
    paddingHorizontal: 15,
    paddingVertical: 6,
    marginTop: R.styles.constants.cardMargin,
  },
})

interface IComponentProps extends ViewProps {
  title: string
  onPress(): void
  containerStyle?: ViewStyle | ViewStyle[]
  rightElement?: ReactElement
  noRounding?: boolean
  noRoundingTop?: boolean
  noRoundingBottom?: boolean
  regularFont?: boolean
  disabled?: boolean
  color?: string
}

const LinkCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { title, onPress, containerStyle, rightElement, noRounding, noRoundingTop, noRoundingBottom, regularFont, color, disabled } = props

  return (
    <TouchableOpacity onPress={onPress} style={{ width: '100%' }} disabled={disabled}>
      <View
        style={[
          styles.cardHeader,
          noRounding && R.styles.removeRounding,
          noRoundingTop && R.styles.removeRoundingTop,
          noRoundingBottom && R.styles.removeRoundingBottom,
          disabled && { opacity: 0.5 },
          containerStyle,
        ]}
      >
        <Text style={[regularFont ? R.styles.fonts.body : R.styles.fonts.headline, { color: color || R.colors.black }]}>{title}</Text>
        {rightElement || <AppIcons name="chevron" size={14} color={color || R.colors.fossil} />}
      </View>
    </TouchableOpacity>
  )
}

export default LinkCard
