import React, { ReactElement } from 'react'
import { View } from 'react-native'
import { Button } from 'react-native-elements'

import R from '../res/R'
import AppIcons from './AppIcons'

interface IComponentProps {
  title: string
  onPress(): void
}

const RemoveButton = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { title, onPress } = props

  return (
    <View style={R.styles.bottomButtonWrapper}>
      <Button
        onPress={onPress}
        title={title}
        type="outline"
        buttonStyle={R.styles.buttons.textButton}
        titleStyle={[R.styles.buttons.secondaryButtonTitle, { color: R.colors.scarlet }]}
        icon={<AppIcons name="trash" size={16} color={R.colors.scarlet} style={{ marginRight: 5 }} />}
      />
    </View>
  )
}

export default RemoveButton
