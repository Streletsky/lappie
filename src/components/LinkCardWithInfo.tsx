import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity, ViewStyle } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'

const styles = StyleSheet.create({
  container: {
    ...R.styles.borderRadiuses.large,
    width: '100%',
    flexDirection: 'column',
    alignContent: 'space-between',
    minHeight: 64,
    backgroundColor: R.colors.white,
    paddingTop: 12,
    paddingBottom: 14,
  },
  cardHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingRight: 15,
    paddingLeft: 20,
  },
})

interface IComponentProps extends ViewProps {
  title: string
  subtitle: string
  bottomText?: string
  onPress(): void
  containerStyle?: ViewStyle
  noRounding?: boolean
  noRoundingTop?: boolean
  noRoundingBottom?: boolean
  isActive?: boolean
  isUpcoming?: boolean
}

const LinkCardWithInfo = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { title, subtitle, bottomText, onPress, containerStyle, noRounding, noRoundingTop, noRoundingBottom, isActive, isUpcoming } = props

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        noRounding && R.styles.removeRounding,
        noRoundingTop && R.styles.removeRoundingTop,
        noRoundingBottom && R.styles.removeRoundingBottom,
        containerStyle,
      ]}
    >
      <View style={[styles.cardHeader, isActive && { paddingLeft: 0 }]}>
        {(isActive || isUpcoming) && (
          <View style={[R.styles.greenDot, { marginLeft: 13, marginRight: 13 }, isUpcoming && { backgroundColor: R.colors.scarlet }]} />
        )}
        <View style={{ flex: 1 }}>
          <Text style={R.styles.fonts.body}>{title}</Text>
          <Text style={[R.styles.fonts.callout, { color: R.colors.porpoise }]}>{subtitle}</Text>
        </View>
        <AppIcons name="chevron" size={14} color={R.colors.darkCloud} />
      </View>
      {bottomText && (
        <View style={[{ paddingLeft: 20, marginTop: 14 }, (isActive || isUpcoming) && { paddingLeft: 36 }]}>
          <Text style={R.styles.fonts.button}>{bottomText}</Text>
        </View>
      )}
    </TouchableOpacity>
  )
}

export default LinkCardWithInfo
