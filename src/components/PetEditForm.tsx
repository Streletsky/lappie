import React, { ReactElement, PureComponent } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { CheckBox, Divider } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import ImagePickerWithActionSheet from './ImagePickerWithActionSheet'
import constants from '../utilities/constants'
import PetTypeItem from './PetTypeItem'
import SectionRadioButton from './SectionRadioButton'
import { getPetDefaultPhoto } from '../utilities/helpers'
import PetType from '../types/PetType'
import DatePicker from './DatePicker'
import AgePicker from './AgePicker'

const styles = StyleSheet.create({
  petPhotoSection: {
    flexBasis: 100,
    alignItems: 'center',
    paddingTop: R.styles.constants.containerPaddingVertical,
    marginBottom: 13,
  },
  typesWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 24,
    paddingTop: 10,
    paddingBottom: 13,
  },
  iosDatePicker: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  checkboxContainer: {
    ...R.styles.reset,
    paddingLeft: 11,
    paddingRight: R.styles.constants.containerPaddingHorizontal,
    backgroundColor: 'transparent',
    paddingTop: 11,
    paddingBottom: 17,
  },
})

interface IComponentProps {
  pet: IPet
  setParentState(pet: IPet): void
  addPadding: boolean
}

export default class PetEditForm extends PureComponent<IComponentProps> {
  public static defaultProps = {
    addPadding: false,
  }

  private renderAgeField = (): ReactElement | null => {
    const { pet, setParentState, addPadding } = this.props

    if (pet.type === PetType.Fish) return null

    if (pet.type === PetType.Bird || pet.type === PetType.Rodent || pet.ageOptionSelected) {
      return (
        <AgePicker
          setParentState={setParentState}
          pet={pet}
          containerStyle={
            addPadding
              ? {
                  ...R.styles.borderRadiuses.large,
                  borderTopLeftRadius: 0,
                  borderTopRightRadius: 0,
                  borderBottomWidth: 0,
                  paddingLeft: 11,
                }
              : undefined
          }
        />
      )
    }

    return (
      <DatePicker
        date={pet.birthday}
        title={R.strings.pets.birth()}
        setParentState={(date): void => setParentState({ ...pet, birthday: date })}
        maximumDate={new Date()}
        containerStyle={
          addPadding
            ? {
                ...R.styles.borderRadiuses.large,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomWidth: 0,
                paddingLeft: 11,
              }
            : undefined
        }
      />
    )
  }

  public render(): ReactElement {
    const { pet, setParentState, addPadding } = this.props

    return (
      <View style={addPadding && { ...R.styles.defaultContainer }}>
        <View style={styles.petPhotoSection}>
          <ImagePickerWithActionSheet
            defaultImage={getPetDefaultPhoto(pet.type)}
            image={pet.photo}
            setParentState={(image): void => setParentState({ ...pet, photo: image })}
          />
        </View>
        <View>
          <View>
            <TextInputField
              label={R.strings.pets.name()}
              onChangeText={(text): void => {
                setParentState({ ...pet, name: text })
              }}
              value={pet.name}
              maxLength={40}
              autoCompleteType="name"
              returnKeyType="done"
              isRequired
              containerStyle={addPadding ? { ...R.styles.borderRadiuses.large, paddingLeft: 11 } : undefined}
            />
          </View>
          <View style={styles.typesWrapper}>
            {constants.petTypes.map(
              (type): ReactElement => {
                return (
                  <PetTypeItem
                    type={type}
                    key={type}
                    active={type === pet.type}
                    onPress={(selectedType): void => setParentState({ ...pet, type: selectedType })}
                  />
                )
              },
            )}
          </View>
          <View>
            <TextInputField
              label={R.strings.pets.breed()}
              onChangeText={(text): void => {
                setParentState({ ...pet, breed: text })
              }}
              value={pet.breed}
              maxLength={40}
              returnKeyType="done"
              containerStyle={
                addPadding
                  ? {
                      ...R.styles.borderRadiuses.large,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 0,
                      paddingLeft: 11,
                    }
                  : undefined
              }
            />
            <Divider style={R.styles.divider} />
          </View>
          {pet.type !== PetType.Fish && (
            <View>
              <View
                style={[
                  R.styles.genericField,
                  addPadding && {
                    paddingLeft: 11,
                  },
                ]}
              >
                <Text style={R.styles.inputLabelStyle}>{R.strings.pets.gender()}</Text>
                <SectionRadioButton
                  handleSelection={(index): void => setParentState({ ...pet, sex: index })}
                  selectedIndex={pet.sex || 0}
                  buttons={[R.strings.pets.male(), R.strings.pets.female()]}
                  width={136}
                />
              </View>
              <Divider style={R.styles.divider} />
            </View>
          )}
          <View>{this.renderAgeField()}</View>
          {(pet.type === PetType.Cat || pet.type === PetType.Dog || pet.type === PetType.Rabbit) && (
            <CheckBox
              title={R.strings.pets.idkDob()}
              size={22}
              containerStyle={styles.checkboxContainer}
              textStyle={[R.styles.fonts.callout, { color: R.colors.porpoise, marginLeft: 0 }]}
              checked={pet.ageOptionSelected}
              iconType="ionicon"
              checkedIcon="ios-checkbox"
              uncheckedIcon="ios-square-outline"
              checkedColor={R.colors.cobalt}
              onPress={(): void => {
                setParentState({ ...pet, ageOptionSelected: !pet.ageOptionSelected })
              }}
            />
          )}
        </View>
      </View>
    )
  }
}
