import React, { ReactElement, PureComponent } from 'react'
import { View, Image, Text } from 'react-native'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import { Divider } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import DatePicker from './DatePicker'
import BottomModal from './BottomModal'
import DropdownPicker from './DropdownPicker'
import constants from '../utilities/constants'
import ITreatment from '../types/Treatment'
import TextInputFieldMultiline from './TextInputFieldMultiline'
import TimePicker from './TimePicker'

interface IComponentProps {
  pet: IPet
  onSave(treatment: ITreatment): void
  onCancel?(): void
  isModalOpen: boolean
  treatment?: ITreatment
}

interface IComponentState {
  treatment: ITreatment
}

class TreatmentEditForm extends PureComponent<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    const { treatment, pet } = this.props

    if (treatment) {
      this.state = { treatment }
    } else {
      this.state = {
        treatment: {
          id: uuid(),
          name: `${R.strings.pets.health.treatment()} ${pet.treatments.length + 1}`,
          description: '',
          startDate: new Date(),
          endDate: undefined,
          periodicity: undefined,
        },
      }
    }
  }

  private onCancel = (): void => {
    const { onCancel } = this.props

    if (onCancel) {
      onCancel()
    }

    this.resetTreatment()
  }

  private onSave = (treatment: ITreatment): void => {
    const { onSave } = this.props

    if (onSave) {
      onSave(treatment)
    }

    this.resetTreatment()
  }

  private resetTreatment = (): void => {
    const { treatment, pet } = this.props

    if (treatment) {
      this.setState({ treatment })
    } else {
      this.setState({
        treatment: {
          id: uuid(),
          name: `${R.strings.pets.health.treatment()} ${pet.treatments.length + 1}`,
          description: '',
          startDate: new Date(),
          endDate: undefined,
          periodicity: undefined,
        },
      })
    }
  }

  public render(): ReactElement {
    const { pet, isModalOpen, treatment: existingTreatment } = this.props
    const { treatment } = this.state

    return (
      <BottomModal
        title={existingTreatment ? R.strings.pets.health.treatmentEdit() : R.strings.pets.health.treatmentAdd()}
        onCancel={this.onCancel}
        saveButtonEnabled={(treatment.name && treatment.name.length > 0) === true}
        visible={isModalOpen}
        onSave={(): void => this.onSave(treatment)}
        accentColor={R.colors.emerald}
      >
        <View>
          <View>
            <View style={R.styles.modalTopSection}>
              <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
              <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
            </View>
            <TextInputField
              label={R.strings.pets.name()}
              onChangeText={(text): void => {
                this.setState({ treatment: { ...treatment, name: text } })
              }}
              value={treatment.name}
              maxLength={40}
              returnKeyType="done"
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DatePicker
              date={treatment.startDate}
              title={R.strings.pets.starts()}
              setParentState={(date): void => this.setState({ treatment: { ...treatment, startDate: date } })}
              maximumDate={treatment.endDate}
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DatePicker
              date={treatment.endDate}
              title={R.strings.pets.ends()}
              setParentState={(date): void => this.setState({ treatment: { ...treatment, endDate: date } })}
              minimumDate={treatment.startDate}
            />
            <Divider style={R.styles.divider} />
            <DropdownPicker
              title={R.strings.pets.application()}
              selectedValue={treatment.periodicity}
              items={constants.extendedPeriodicityDropdownItems}
              setParentState={(value: number): void => this.setState({ treatment: { ...treatment, periodicity: value } })}
            />
            {(treatment.periodicity === 1 || treatment.periodicity === 2 || treatment.periodicity === 3) && (
              <View>
                <Divider style={R.styles.divider} />
                <TimePicker
                  date={treatment.timeOfDay1}
                  title={R.strings.pets.time()}
                  setParentState={(date): void => this.setState({ treatment: { ...treatment, timeOfDay1: date } })}
                />
              </View>
            )}
            {(treatment.periodicity === 1 || treatment.periodicity === 2) && (
              <View>
                <Divider style={R.styles.divider} />
                <TimePicker
                  date={treatment.timeOfDay2}
                  title={R.strings.pets.time()}
                  setParentState={(date): void => this.setState({ treatment: { ...treatment, timeOfDay2: date } })}
                />
              </View>
            )}
            {treatment.periodicity === 1 && (
              <View>
                <Divider style={R.styles.divider} />
                <TimePicker
                  date={treatment.timeOfDay3}
                  title={R.strings.pets.time()}
                  setParentState={(date): void => this.setState({ treatment: { ...treatment, timeOfDay3: date } })}
                />
              </View>
            )}
            <TextInputFieldMultiline
              label={R.strings.pets.notes()}
              onChangeText={(text): void => {
                this.setState({ treatment: { ...treatment, description: text } })
              }}
              containerStyle={{ marginTop: 23 }}
              value={treatment.description}
              maxLength={350}
            />
          </View>
        </View>
      </BottomModal>
    )
  }
}

export default TreatmentEditForm
