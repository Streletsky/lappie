import React, { ReactElement, PureComponent } from 'react'
import { View, Image, Text } from 'react-native'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import { Divider } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'
import IVaccination from '../types/Vaccination'
import DatePicker from './DatePicker'
import BottomModal from './BottomModal'
import DropdownPicker from './DropdownPicker'
import constants from '../utilities/constants'
import TextInputFieldMultiline from './TextInputFieldMultiline'

interface IComponentProps {
  pet: IPet
  onSave(vaccination: IVaccination): void
  onCancel?(): void
  isModalOpen: boolean
  vaccination?: IVaccination
}

interface IComponentState {
  vaccination: IVaccination
}

class VaccinationEditForm extends PureComponent<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    const { vaccination } = this.props

    if (vaccination) {
      this.state = { vaccination }
    } else {
      this.state = {
        vaccination: {
          id: uuid(),
          name: '',
          startDate: new Date(),
          periodicity: 1,
          notes: '',
        },
      }
    }
  }

  private onCancel = (): void => {
    const { onCancel } = this.props

    if (onCancel) {
      onCancel()
    }

    this.resetVaccination()
  }

  private onSave = (vaccination: IVaccination): void => {
    const { onSave } = this.props

    if (onSave) {
      onSave(vaccination)
    }

    this.resetVaccination()
  }

  private resetVaccination = (): void => {
    const { vaccination } = this.props

    if (vaccination) {
      this.setState({ vaccination })
    } else {
      this.setState({
        vaccination: {
          id: uuid(),
          name: '',
          startDate: new Date(),
          periodicity: 1,
          notes: '',
        },
      })
    }
  }

  public render(): ReactElement {
    const { pet, isModalOpen, vaccination: existingVaccination } = this.props
    const { vaccination } = this.state

    return (
      <BottomModal
        title={existingVaccination ? R.strings.pets.health.vaccinationEdit() : R.strings.pets.health.vaccinationAdd()}
        onCancel={this.onCancel}
        saveButtonEnabled={
          (vaccination.name && vaccination.name.length > 0 && vaccination.startDate && vaccination.periodicity > 0) === true
        }
        visible={isModalOpen}
        onSave={(): void => this.onSave(vaccination)}
        accentColor={R.colors.aegean}
      >
        <View>
          <View>
            {pet && (
              <View style={R.styles.modalTopSection}>
                <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
                <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
              </View>
            )}
            <TextInputField
              label={R.strings.pets.name()}
              onChangeText={(text): void => {
                this.setState({ vaccination: { ...vaccination, name: text } })
              }}
              value={vaccination.name}
              maxLength={40}
              returnKeyType="done"
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DatePicker
              date={vaccination.startDate}
              title={R.strings.pets.date()}
              setParentState={(date): void => this.setState({ vaccination: { ...vaccination, startDate: date } })}
              isRequired
            />
            <Divider style={R.styles.divider} />
            <DropdownPicker
              title={R.strings.pets.periodicity()}
              selectedValue={vaccination.periodicity}
              items={constants.periodicityDropdownItems}
              setParentState={(value: number): void => this.setState({ vaccination: { ...vaccination, periodicity: value } })}
              isRequired
            />
            <TextInputFieldMultiline
              label={R.strings.pets.notes()}
              onChangeText={(text): void => {
                this.setState({ vaccination: { ...vaccination, notes: text } })
              }}
              containerStyle={{ marginTop: 32 }}
              value={vaccination.notes}
              maxLength={350}
            />
          </View>
        </View>
      </BottomModal>
    )
  }
}

export default VaccinationEditForm
