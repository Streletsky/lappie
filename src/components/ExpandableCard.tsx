import React, { ReactElement } from 'react'
import { View, StyleSheet, Text, ViewProps, TouchableOpacity } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'

const styles = StyleSheet.create({
  title: {
    ...R.styles.fonts.star,
    color: R.colors.black,
    textAlign: 'left',
    flex: 1,
    marginLeft: 8,
  },
  container: {
    ...R.styles.borderRadiuses.large,
    width: '100%',
    minHeight: 64,
    backgroundColor: R.colors.white,
    marginTop: R.styles.constants.cardMargin,
  },
  cardHeaderWrapper: {
    ...R.styles.borderRadiuses.large,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingLeft: 17,
    minHeight: 64,
  },
  childrenWrapper: {
    ...R.styles.borderRadiuses.large,
    backgroundColor: R.colors.white,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    paddingBottom: 15,
  },
})

interface IComponentProps extends ViewProps {
  color: string
  title: string
  icon: string
  onHeaderPress(): void
  children?: ReactElement | ReactElement[]
}

const ExpandableCard = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { color, title, icon, style, children, onHeaderPress } = props

  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity onPress={onHeaderPress}>
        <View style={[styles.cardHeaderWrapper, children && { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
          <AppIcons name={icon} size={24} color={color} />
          <Text style={styles.title}>{title}</Text>
          <AppIcons name="plus" size={24} color={color} style={{ marginRight: 15 }} />
        </View>
      </TouchableOpacity>
      {children && <View style={styles.childrenWrapper}>{children}</View>}
    </View>
  )
}

export default ExpandableCard
