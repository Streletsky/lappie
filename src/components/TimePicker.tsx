import React, { Component, ReactElement } from 'react'
import { View, Text, ViewStyle, TouchableOpacity } from 'react-native'
import DateTimePicker, { Event } from '@react-native-community/datetimepicker'
import { Overlay } from 'react-native-elements'

import R from '../res/R'
import AppIcons from './AppIcons'
import { formatTime } from '../utilities/helpers'
import featureChecker from '../utilities/featureChecker'

interface IComponentProps {
  setParentState(date: Date): void
  title: string
  date?: Date
  containerStyle?: ViewStyle
  isRequired?: boolean
}

interface IComponentState {
  isPickerOpened: boolean
  selectedDate?: Date
}

export default class TimePicker extends Component<IComponentProps, IComponentState> {
  public static defaultProps = {
    isRequired: false,
  }

  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      isPickerOpened: false,
      selectedDate: props.date,
    }
  }

  private setDate = (event: Event, date?: Date): void => {
    const { setParentState } = this.props

    if (date) {
      this.setState({ selectedDate: date, isPickerOpened: featureChecker.isIos() })
      setParentState(date)
    }
  }

  private onDatePress = (): void => {
    const { isPickerOpened } = this.state

    this.setState({ isPickerOpened: !isPickerOpened })
  }

  private renderPicker = (): ReactElement => {
    const { selectedDate } = this.state

    return <DateTimePicker value={selectedDate || new Date()} mode="time" display="default" onChange={this.setDate} />
  }

  public render(): ReactElement {
    const { title, containerStyle, isRequired } = this.props
    const { selectedDate, isPickerOpened } = this.state
    const formattedDate = selectedDate && formatTime(selectedDate)
    const isIos = featureChecker.isIos()

    return (
      <View>
        <TouchableOpacity onPress={this.onDatePress}>
          <View style={[R.styles.genericField, containerStyle]}>
            <View style={R.styles.rowCenter}>
              <Text style={R.styles.inputLabelStyle}>{title}</Text>
              {isRequired && <View style={R.styles.requiredIndicator} />}
            </View>
            <View style={R.styles.rowCenter}>
              <Text>{formattedDate}</Text>
              <AppIcons name="calendar" size={16} color={R.colors.porpoise} style={{ marginLeft: 9 }} />
            </View>
          </View>
        </TouchableOpacity>
        {isPickerOpened && !isIos && this.renderPicker()}
        {isIos && (
          <Overlay
            animationType="fade"
            isVisible={isPickerOpened}
            overlayStyle={R.styles.overlay}
            onBackdropPress={(): void => this.setState({ isPickerOpened: false })}
          >
            {this.renderPicker()}
          </Overlay>
        )}
      </View>
    )
  }
}
