import React, { ReactElement, PureComponent } from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'
import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'

import R from '../res/R'
import IWeight from '../types/Weight'
import IPet from '../types/Pet'
import DatePicker from './DatePicker'
import WeightPicker from './WeightPicker'
import { getPetDefaultPhoto } from '../utilities/helpers'
import BottomModal from './BottomModal'

const styles = StyleSheet.create({
  weightPickerWrapper: {
    width: '100%',
    paddingHorizontal: R.styles.constants.containerPaddingHorizontal,
  },
})

interface IComponentProps {
  pet: IPet
  onSave(weight: IWeight): void
  onCancel?(): void
  isModalOpen: boolean
  weight?: IWeight
}

interface IComponentState {
  weight: IWeight
}

class WeightEditForm extends PureComponent<IComponentProps, IComponentState> {
  public constructor(props: IComponentProps) {
    super(props)

    const { weight } = this.props

    if (weight) {
      this.state = { weight }
    } else {
      this.state = {
        weight: {
          id: uuid(),
          date: new Date(),
          weightUnit1: 0,
          weightUnit2: 0,
        },
      }
    }
  }

  private onCancel = (): void => {
    const { onCancel } = this.props

    if (onCancel) {
      onCancel()
    }

    this.resetWeight()
  }

  private onSave = (weight: IWeight): void => {
    const { onSave } = this.props

    if (onSave) {
      onSave(weight)
    }

    this.resetWeight()
  }

  private resetWeight = (): void => {
    const { weight } = this.props

    if (weight) {
      this.setState({ weight })
    } else {
      this.setState({
        weight: {
          id: uuid(),
          date: new Date(),
          weightUnit1: 0,
          weightUnit2: 0,
        },
      })
    }
  }

  public render(): ReactElement {
    const { pet, isModalOpen } = this.props
    const { weight } = this.state

    return (
      <BottomModal
        title={R.strings.pets.addWeight()}
        onCancel={this.onCancel}
        visible={isModalOpen}
        onSave={(): void => this.onSave(weight)}
        saveButtonEnabled={weight.weightUnit1 + weight.weightUnit2 > 0}
      >
        <View>
          {pet && (
            <View style={R.styles.modalTopSection}>
              <Image source={(pet.photo && { uri: pet.photo }) || getPetDefaultPhoto(pet.type)} style={R.styles.smallAvatar} />
              <Text style={[R.styles.fonts.hero, { marginLeft: 10 }]}>{pet.name}</Text>
            </View>
          )}
          <DatePicker
            date={weight.date}
            title={R.strings.pets.date()}
            setParentState={(date): void => this.setState({ weight: { ...weight, date } })}
            isRequired
          />
          <View style={[styles.weightPickerWrapper, { marginTop: 35 }]}>
            <WeightPicker pet={pet} weight={weight} setParentState={(w): void => this.setState({ weight: w })} />
          </View>
        </View>
      </BottomModal>
    )
  }
}

export default WeightEditForm
