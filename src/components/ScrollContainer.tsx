import React, { ReactElement } from 'react'
import { ScrollView } from 'react-native'

interface IComponentProps {
  children: ReactElement | ReactElement[]
}

const ScrollContainer = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { children } = props

  return (
    <ScrollView
      contentContainerStyle={{ flexGrow: 1 }}
      style={{ flex: 1 }}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      keyboardShouldPersistTaps="handled"
    >
      {children}
    </ScrollView>
  )
}

export default ScrollContainer
