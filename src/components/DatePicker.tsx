import React, { Component, ReactElement } from 'react'
import { View, Text, ViewStyle, TouchableOpacity } from 'react-native'
import DateTimePicker, { Event } from '@react-native-community/datetimepicker'
import { Overlay } from 'react-native-elements'

import R from '../res/R'
import AppIcons from './AppIcons'
import { formatDate } from '../utilities/helpers'
import featureChecker from '../utilities/featureChecker'

interface IComponentProps {
  setParentState(date: Date): void
  title: string
  date?: Date
  containerStyle?: ViewStyle
  minimumDate?: Date
  maximumDate?: Date
  isRequired?: boolean
}

interface IComponentState {
  isPickerOpened: boolean
  selectedDate?: Date
}

export default class DatePicker extends Component<IComponentProps, IComponentState> {
  public static defaultProps = {
    isRequired: false,
  }

  public constructor(props: IComponentProps) {
    super(props)

    this.state = {
      isPickerOpened: false,
      selectedDate: props.date,
    }
  }

  private setDate = (event: Event, date?: Date): void => {
    const { setParentState } = this.props

    if (date) {
      this.setState({ selectedDate: date, isPickerOpened: featureChecker.isIos() })
      setParentState(date)
    }
  }

  private onDatePress = (): void => {
    const { isPickerOpened } = this.state

    this.setState({ isPickerOpened: !isPickerOpened })
  }

  private renderPicker = (): ReactElement => {
    const { maximumDate, minimumDate } = this.props
    const { selectedDate } = this.state

    const today = new Date()
    const maxDate = new Date(today.getFullYear() + 10, 12, 31)
    const minDate = new Date(today.getFullYear() - 30, 1, 1)

    return (
      <DateTimePicker
        value={selectedDate || new Date()}
        mode="date"
        display="default"
        onChange={this.setDate}
        minimumDate={minimumDate || minDate}
        maximumDate={maximumDate || maxDate}
      />
    )
  }

  public render(): ReactElement {
    const { title, containerStyle, isRequired } = this.props
    const { selectedDate, isPickerOpened } = this.state
    const formattedDate = selectedDate && formatDate(selectedDate)
    const isIos = featureChecker.isIos()

    return (
      <View>
        <TouchableOpacity onPress={this.onDatePress}>
          <View style={[R.styles.genericField, containerStyle]}>
            <View style={R.styles.rowCenter}>
              <Text style={R.styles.inputLabelStyle}>{title}</Text>
              {isRequired && <View style={R.styles.requiredIndicator} />}
            </View>
            <View style={R.styles.rowCenter}>
              <Text>{formattedDate}</Text>
              <AppIcons name="calendar" size={16} color={R.colors.porpoise} style={{ marginLeft: 9 }} />
            </View>
          </View>
        </TouchableOpacity>
        {isPickerOpened && !isIos && this.renderPicker()}
        {isIos && (
          <Overlay
            animationType="fade"
            isVisible={isPickerOpened}
            overlayStyle={R.styles.overlay}
            onBackdropPress={(): void => this.setState({ isPickerOpened: false })}
          >
            {this.renderPicker()}
          </Overlay>
        )}
      </View>
    )
  }
}
