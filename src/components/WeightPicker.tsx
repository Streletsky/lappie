import React, { Component, ReactElement } from 'react'
import { View, StyleSheet, Picker, Animated } from 'react-native'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { WheelPicker } from 'react-native-wheel-picker-android'

import IPet from '../types/Pet'
import IAppState from '../types/AppState'
import { setWeightUnitSetting, ISettingsAction } from '../actions/actionCreators'
import featureChecker from '../utilities/featureChecker'
import PetType from '../types/PetType'
import IWeight from '../types/Weight'
import constants from '../utilities/constants'
import R from '../res/R'
import SectionRadioButton from './SectionRadioButton'
import { convertWeightToLb, convertWeightFromLb } from '../utilities/helpers'

const styles = StyleSheet.create({
  weightPickersWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    marginTop: 20,
  },
})

interface IDispatchFromProps {
  handleWeightUnitSelection(index: number): void
}

interface IStateFromProps {
  selectedWeightUnitIndex: number
}

interface IComponentProps {
  setParentState(weight: IWeight): void
  pet: IPet
  weight: IWeight
  imageMinSize?: number
  imageMaxSize?: number
  resultImageSize?: Animated.Value
}

interface IComponentState {
  convertedWeight: IWeight
}

class WeightPicker extends Component<IComponentProps & IDispatchFromProps & IStateFromProps, IComponentState> {
  private weightUnits1: number[] = []

  private weightUnits2: number[] = []

  private weightUnits2Labels: string[] = []

  public constructor(props: IComponentProps & IDispatchFromProps & IStateFromProps) {
    super(props)

    this.state = {
      convertedWeight: convertWeightFromLb(props.selectedWeightUnitIndex, props.weight, props.pet.type),
    }

    this.setWeightLimits()
  }

  private setWeightLimits = (): void => {
    const { selectedWeightUnitIndex, pet } = this.props

    this.weightUnits1 = []
    this.weightUnits2 = []
    this.weightUnits2Labels = []

    if (selectedWeightUnitIndex === 0) {
      let upperLimit = 80

      switch (pet.type) {
        case PetType.Cat:
          upperLimit = 10
          break
        case PetType.Rabbit:
          upperLimit = 4
          break
        default:
          upperLimit = 80
          break
      }

      for (let unit = 0; unit <= upperLimit; unit++) {
        this.weightUnits1.push(unit)
      }
    } else {
      let upperLimit = 180

      switch (pet.type) {
        case PetType.Cat:
          upperLimit = 23
          break
        case PetType.Rabbit:
          upperLimit = 10
          break
        default:
          upperLimit = 180
          break
      }

      for (let unit = 0; unit <= upperLimit; unit++) {
        this.weightUnits1.push(unit)
      }
    }

    for (let unit = 0; unit <= 9; unit++) {
      this.weightUnits2.push(unit)
    }

    this.weightUnits2Labels = this.weightUnits2.map((w: number): string => (w === 0 ? '000' : (w * 100).toString()))
  }

  private convertWeight = (selectedUnitIndex: number, pet: IPet): void => {
    const { weight } = this.props

    this.setState({ convertedWeight: convertWeightFromLb(selectedUnitIndex, weight, pet.type) })
  }

  private setWeightUnit = (weightIndex: number): void => {
    const { handleWeightUnitSelection } = this.props

    handleWeightUnitSelection(weightIndex)
  }

  public componentDidUpdate = (prevProps: IStateFromProps & IComponentProps): void => {
    const { pet, selectedWeightUnitIndex, imageMinSize, imageMaxSize, resultImageSize, weight } = this.props
    const { convertedWeight } = this.state

    if (selectedWeightUnitIndex !== prevProps.selectedWeightUnitIndex) {
      this.setWeightLimits()
      this.convertWeight(selectedWeightUnitIndex, pet)
    }

    if (imageMaxSize && imageMinSize && resultImageSize) {
      if (weight.weightUnit1 !== prevProps.weight.weightUnit1) {
        const newValue =
          imageMinSize + (convertedWeight.weightUnit1 / this.weightUnits1[this.weightUnits1.length - 1]) * (imageMaxSize - imageMinSize)

        Animated.timing(resultImageSize, {
          toValue: newValue,
          duration: 200,
          useNativeDriver: true,
        }).start()
      }
    }
  }

  public render(): ReactElement {
    const { setParentState, selectedWeightUnitIndex, pet } = this.props
    const { convertedWeight } = this.state

    return (
      <View>
        <View style={R.styles.rowCenter}>
          <SectionRadioButton
            handleSelection={this.setWeightUnit}
            selectedIndex={selectedWeightUnitIndex}
            buttons={constants.weightUoms}
            width={136}
          />
        </View>
        <View style={styles.weightPickersWrapper}>
          <View>
            {featureChecker.isIos() && (
              <Picker
                style={{ width: 100 }}
                selectedValue={convertedWeight.weightUnit1}
                onValueChange={(value): void => {
                  this.setState({ convertedWeight: { ...convertedWeight, weightUnit1: value } })
                  setParentState(convertWeightToLb(selectedWeightUnitIndex, { ...convertedWeight, weightUnit1: value }, pet.type))
                }}
              >
                {this.weightUnits1.map(
                  (w: number): ReactElement => (
                    <Picker.Item label={w.toString()} value={w} key={w} />
                  ),
                )}
              </Picker>
            )}
            {featureChecker.isAndroid() && (
              <WheelPicker
                hideIndicator
                itemTextSize={22}
                itemTextFontFamily={R.styles.fontFamily.fontFamily}
                selectedItemTextSize={22}
                selectedItemTextFontFamily={R.styles.fontFamily.fontFamily}
                selectedItem={convertedWeight.weightUnit1}
                data={this.weightUnits1.map((w): string => w.toString())}
                onItemSelected={(value): void => {
                  this.setState({ convertedWeight: { ...convertedWeight, weightUnit1: value } })
                  setParentState(convertWeightToLb(selectedWeightUnitIndex, { ...convertedWeight, weightUnit1: value }, pet.type))
                }}
              />
            )}
          </View>
          <View>
            {featureChecker.isIos() && (
              <Picker
                style={{ width: 100 }}
                selectedValue={convertedWeight.weightUnit2}
                onValueChange={(value): void => {
                  this.setState({ convertedWeight: { ...convertedWeight, weightUnit2: value } })
                  setParentState(convertWeightToLb(selectedWeightUnitIndex, { ...convertedWeight, weightUnit2: value }, pet.type))
                }}
              >
                {this.weightUnits2.map(
                  (w: number): ReactElement => (
                    <Picker.Item label={w === 0 ? '000' : (w * 100).toString()} value={w} key={w} />
                  ),
                )}
              </Picker>
            )}
            {featureChecker.isAndroid() && (
              <WheelPicker
                hideIndicator
                itemTextSize={22}
                itemTextFontFamily={R.styles.fontFamily.fontFamily}
                selectedItemTextSize={22}
                selectedItemTextFontFamily={R.styles.fontFamily.fontFamily}
                selectedItem={convertedWeight.weightUnit2}
                data={this.weightUnits2Labels}
                onItemSelected={(value): void => {
                  this.setState({ convertedWeight: { ...convertedWeight, weightUnit2: value } })
                  setParentState(convertWeightToLb(selectedWeightUnitIndex, { ...convertedWeight, weightUnit2: value }, pet.type))
                }}
              />
            )}
          </View>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchFromProps => ({
  handleWeightUnitSelection: (index: number): ISettingsAction => dispatch(setWeightUnitSetting(index)),
})

const mapStateToProps = (state: IAppState): IStateFromProps => {
  return {
    selectedWeightUnitIndex: state.settings.selectedWeightUnitIndex,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WeightPicker)
