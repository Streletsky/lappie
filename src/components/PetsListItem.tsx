import React, { ReactElement } from 'react'
import { StyleSheet, Image, View, Text, TouchableOpacity } from 'react-native'

import R from '../res/R'
import AppIcons from './AppIcons'
import IPet from '../types/Pet'
import { getPetDefaultPhoto } from '../utilities/helpers'

const styles = StyleSheet.create({
  container: {
    ...R.styles.borderRadiuses.large,
    ...R.styles.cardShadowSmaller,
    backgroundColor: R.colors.white,
    justifyContent: 'center',
    flexBasis: 100,
    paddingLeft: 10,
    paddingRight: 15,
    paddingVertical: 10,
    marginBottom: 8,
  },
  pushOppositeHorizontal: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  image: {
    backgroundColor: R.colors.periwinkle,
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  headline: {
    marginLeft: 10,
    marginRight: 20,
  },
})

interface IComponentProps {
  pet: IPet
  onPress(): boolean
}

const PetsListItem = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { pet, onPress } = props
  const { photo, type, name } = pet

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.pushOppositeHorizontal}>
        <View style={R.styles.rowCenter}>
          <Image source={photo ? { uri: photo } : getPetDefaultPhoto(type)} style={styles.image} />
          <Text style={[R.styles.fonts.hero, styles.headline]}>{name}</Text>
        </View>
        <AppIcons name="chevron" size={14} color={R.colors.fossil} />
      </View>
    </TouchableOpacity>
  )
}

export default PetsListItem
