import React, { ReactElement, Component } from 'react'
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native'
import { Divider, Button } from 'react-native-elements'

import R from '../res/R'
import TextInputField from './TextInputField'
import FirebaseAuthService from '../services/FirebaseAuthService'

interface IComponentProps {
  closeAction: () => void
}

interface IComponentState {
  email?: string
  password?: string
}

class LoginForm extends Component<IComponentProps, IComponentState> {
  private authService: FirebaseAuthService

  public constructor(props: IComponentProps) {
    super(props)

    this.authService = new FirebaseAuthService()

    this.state = {
      email: undefined,
      password: undefined,
    }
  }

  private loginWithEmail = (email?: string, password?: string): void => {
    const { closeAction } = this.props

    if (!email || !password) return

    this.authService
      .signInWithEmail(email, password)
      .then((): void => {
        closeAction()
      })
      .catch((reason): void => {
        Alert.alert(
          R.strings.ui.confirmAction(),
          reason.toString(),
          [
            {
              text: R.strings.ui.ok(),
              onPress: (): void => {},
            },
          ],
          { cancelable: false },
        )
      })
  }

  private facebookSignUp = (): void => {}

  private googleSignUp = (): void => {
    const { closeAction } = this.props

    this.authService
      .userGoogleLogin()
      .then(closeAction)
      .catch((reason): void => {
        Alert.alert(
          R.strings.ui.confirmAction(),
          reason.toString(),
          [
            {
              text: R.strings.ui.ok(),
              onPress: (): void => {},
            },
          ],
          { cancelable: false },
        )
      })
  }

  public render(): ReactElement {
    const { email, password } = this.state

    return (
      <View>
        <View style={{ marginTop: R.styles.constants.containerPaddingVertical }}>
          <TextInputField
            label={R.strings.user.email()}
            onChangeText={(text): void => {
              this.setState({ email: text })
            }}
            value={email}
            maxLength={40}
            returnKeyType="done"
            isRequired
          />
          <Divider style={R.styles.divider} />
          <TextInputField
            label={R.strings.user.password()}
            onChangeText={(text): void => {
              this.setState({ password: text })
            }}
            value={password}
            maxLength={40}
            returnKeyType="done"
            secureTextEntry
            isRequired
          />
          <View style={{ marginTop: 25, paddingHorizontal: R.styles.constants.containerPaddingHorizontal }}>
            <Button
              title={R.strings.user.signin()}
              buttonStyle={[R.styles.buttons.primaryButton]}
              titleStyle={R.styles.buttons.primaryButtonTitle}
              onPress={(): void => {
                this.loginWithEmail(email, password)
              }}
              disabled={!email || !password}
            />
          </View>
          <View style={[R.styles.columnCenter, { marginTop: 24 }]}>
            <Text style={[R.styles.fonts.caption, { color: R.colors.black }]}>{R.strings.user.orSignin()}</Text>
          </View>
          <View style={[R.styles.rowCenter, { marginTop: 15 }]}>
            <TouchableOpacity onPress={this.facebookSignUp} style={[R.styles.socialButton, { marginRight: 24 }]}>
              <Image source={R.images.facebookLogo} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.googleSignUp} style={R.styles.socialButton}>
              <Image source={R.images.googleLogo} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

export default LoginForm
