import React, { Component, ReactElement } from 'react'
import { Image, View, Text, ImageSourcePropType, TouchableOpacity, ViewStyle, ImageStyle } from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import { connectActionSheet, ActionSheetOptions } from '@expo/react-native-action-sheet'
import * as Permissions from 'expo-permissions'

import R from '../res/R'
import featureChecker from '../utilities/featureChecker'
import constants from '../utilities/constants'

interface IImagePickerWithActionSheetProps {
  showActionSheetWithOptions(options: ActionSheetOptions, callback: (i: number) => void): void
}

interface IComponentProps {
  defaultImage: ImageSourcePropType
  image?: string
  setParentState(image: string): void
}

interface IComponentState {
  image?: string
  windowWidth: number
  windowHeight: number
}

class ImagePickerComponent extends Component<IComponentProps & IImagePickerWithActionSheetProps, IComponentState> {
  public constructor(props: IComponentProps & IImagePickerWithActionSheetProps) {
    super(props)

    this.state = {
      image: props.image,
      windowWidth: featureChecker.getScreenWidth(),
      windowHeight: featureChecker.getScreenHeight(),
    }
  }

  private pickImage = async (): Promise<void> => {
    const { setParentState } = this.props
    const { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL)

    if (status !== 'granted') {
      await Permissions.askAsync(Permissions.CAMERA_ROLL)
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
    })

    if (!result.cancelled) {
      setParentState(result.uri)
      this.setState({ image: result.uri })
    }
  }

  private takeImage = async (): Promise<void> => {
    const { setParentState } = this.props
    const { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA)

    if (status !== 'granted') {
      await Permissions.askAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA)
    }

    const result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [1, 1],
    })

    if (!result.cancelled) {
      setParentState(result.uri)
      this.setState({ image: result.uri })
    }
  }

  private onActionSelection = (index: number): void => {
    switch (index) {
      case 0:
        this.pickImage()
        break
      case 1:
        this.takeImage()
        break
      default:
        break
    }
  }

  private onOpenActionSheet = (): void => {
    const { showActionSheetWithOptions } = this.props
    const options = [R.strings.ui.selectPhoto(), R.strings.ui.takePhoto(), R.strings.ui.cancel()]
    const cancelButtonIndex = 2

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
      },
      this.onActionSelection,
    )
  }

  private recalculateDimensions = (): void => {
    this.setState({
      windowWidth: featureChecker.getScreenWidth(),
      windowHeight: featureChecker.getScreenHeight(),
    })
  }

  public render(): ReactElement {
    const { image, windowWidth, windowHeight } = this.state
    const { defaultImage } = this.props

    const photoSize = windowWidth > constants.bigScreenSize || windowHeight > constants.bigScreenSize ? 170 : 100

    const imageStyle: ImageStyle = {
      width: photoSize,
      height: photoSize,
    }

    const imageWrapper: ViewStyle = {
      width: photoSize,
      height: photoSize,
      borderRadius: photoSize / 2,
      overflow: 'hidden',
    }

    return (
      <View style={{ height: '100%' }} onLayout={this.recalculateDimensions}>
        <TouchableOpacity onPress={this.onOpenActionSheet} style={imageWrapper}>
          {image && (
            <View style={{ position: 'relative' }}>
              <Image source={{ uri: image }} style={imageStyle} resizeMode="cover" />
              <Text
                style={{
                  ...R.styles.fonts.caption,
                  position: 'absolute',
                  width: '100%',
                  textAlign: 'center',
                  bottom: 20,
                  color: R.colors.white,
                }}
              >
                {R.strings.ui.edit()}
              </Text>
            </View>
          )}
          {!image && (
            <View style={{ position: 'relative' }}>
              <Image source={defaultImage} style={imageStyle} />
              <Text
                style={{
                  ...R.styles.fonts.caption,
                  position: 'absolute',
                  width: '100%',
                  textAlign: 'center',
                  bottom: 20,
                  color: R.colors.black,
                }}
              >
                {R.strings.ui.selectPhoto()}
              </Text>
            </View>
          )}
        </TouchableOpacity>
      </View>
    )
  }
}

const ImagePickerConnected = connectActionSheet(ImagePickerComponent)

const ImagePickerWithActionSheet = (props: IComponentProps): ReactElement => {
  const { defaultImage, setParentState, image } = props

  return <ImagePickerConnected defaultImage={defaultImage} image={image} setParentState={setParentState} />
}

export default ImagePickerWithActionSheet
