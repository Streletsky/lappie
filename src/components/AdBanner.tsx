import React, { ReactElement, Component } from 'react'
import { AdMobBanner, setTestDeviceIDAsync } from 'expo-ads-admob'
import { View, ViewStyle } from 'react-native'

import constants from '../utilities/constants'
import featureChecker from '../utilities/featureChecker'

interface IComponentProps {
  containerStyle?: ViewStyle
  bannerSize: 'banner' | 'largeBanner' | 'mediumRectangle' | 'fullBanner' | 'leaderboard' | 'smartBannerPortrait' | 'smartBannerLandscape'
}

export default class AdBanner extends Component<IComponentProps> {
  private onBannerError = (error: string): void => {
    console.log(error)
  }

  public componentDidMount = (): void => {
    if (featureChecker.isDevEnv()) {
      setTestDeviceIDAsync('EMULATOR')
    }
  }

  public render(): ReactElement {
    const { containerStyle, bannerSize } = this.props

    return (
      <View style={containerStyle}>
        <AdMobBanner
          bannerSize={bannerSize}
          adUnitID={constants.admobUnitId}
          servePersonalizedAds
          onDidFailToReceiveAdWithError={this.onBannerError}
        />
      </View>
    )
  }
}
