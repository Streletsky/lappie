import React, { ReactElement } from 'react'
import { StyleSheet } from 'react-native'
import { ButtonGroup } from 'react-native-elements'

import R from '../res/R'

const styles = StyleSheet.create({
  selectedButton: {
    backgroundColor: R.colors.white,
    shadowColor: R.colors.black,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    elevation: 2,
  },
  container: {
    height: 32,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    padding: 2,
    backgroundColor: R.colors.cloud,
    borderWidth: 0,
    ...R.styles.borderRadiuses.large,
  },
})

interface IComponentProps {
  handleSelection(index: number): void
  selectedIndex: number
  buttons: string[]
  width: number | string
  disabled?: boolean
}

const SectionRadioButton = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { handleSelection, selectedIndex, buttons, width, disabled } = props

  return (
    <ButtonGroup
      onPress={handleSelection}
      selectedIndex={selectedIndex}
      buttons={buttons}
      buttonStyle={{
        ...R.styles.borderRadiuses.large,
      }}
      textStyle={{
        ...R.styles.fonts.caption,
        color: R.colors.black,
      }}
      selectedTextStyle={{
        ...R.styles.fonts.caption,
        color: R.colors.black,
      }}
      innerBorderStyle={{ width: 0 }}
      selectedButtonStyle={styles.selectedButton}
      containerStyle={[styles.container, { width }]}
      disabled={disabled}
    />
  )
}

export default SectionRadioButton
