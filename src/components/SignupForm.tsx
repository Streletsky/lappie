import React, { Component, ReactElement } from 'react'
import { View, Image, Text, TouchableOpacity, Alert } from 'react-native'
import { Button } from 'react-native-elements'

import R from '../res/R'
import AppIcons from './AppIcons'
import ScrollContainer from './ScrollContainer'
import TextInputField from './TextInputField'
import BottomModal from './BottomModal'
import LoginForm from './LoginForm'
import FirebaseAuthService from '../services/FirebaseAuthService'

interface IComponentProps {
  closeAction: () => void
}

interface IComponentState {
  email?: string
  password?: string
  isLoginModalOpen: boolean
}

class SignupForm extends Component<IComponentProps, IComponentState> {
  private authService: FirebaseAuthService

  public constructor(props: IComponentProps) {
    super(props)

    this.authService = new FirebaseAuthService()

    this.state = {
      email: undefined,
      password: undefined,
      isLoginModalOpen: false,
    }
  }

  private signUpWithEmail = (email?: string, password?: string): void => {
    const { closeAction } = this.props

    if (!email || !password) return

    this.authService
      .signUpWithEmail(email, password)
      .then(closeAction)
      .catch((reason): void => {
        Alert.alert(
          R.strings.ui.confirmAction(),
          reason.toString(),
          [
            {
              text: R.strings.ui.ok(),
              onPress: (): void => {},
            },
          ],
          { cancelable: false },
        )
      })
  }

  private facebookSignUp = (): void => {}

  private googleSignUp = (): void => {
    const { closeAction } = this.props

    this.authService
      .userGoogleLogin()
      .then(closeAction)
      .catch((reason): void => {
        Alert.alert(
          R.strings.ui.confirmAction(),
          reason.toString(),
          [
            {
              text: R.strings.ui.ok(),
              onPress: (): void => {},
            },
          ],
          { cancelable: false },
        )
      })
  }

  public render(): ReactElement {
    const { email, password, isLoginModalOpen } = this.state
    const { closeAction } = this.props

    return (
      <View style={R.styles.rootView}>
        <BottomModal
          title={R.strings.user.signin()}
          visible={isLoginModalOpen}
          onCancel={(): void => this.setState({ isLoginModalOpen: false })}
          saveButtonHidden
          saveButtonEnabled={false}
        >
          <View>
            <LoginForm closeAction={closeAction} />
          </View>
        </BottomModal>
        <View style={R.styles.safeArea}>
          <ScrollContainer>
            <Image
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                resizeMode: 'cover',
              }}
              source={R.images.signupBackground}
            />
            <View style={[R.styles.pushContentOpposite, R.styles.defaultContainer]}>
              <View style={{ marginTop: 28 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <TouchableOpacity onPress={closeAction} style={{ padding: 16 }}>
                    <AppIcons name="close" size={12} color={R.colors.white} />
                  </TouchableOpacity>
                </View>
                <View style={[R.styles.columnCenter, { marginTop: 10 }]}>
                  <Text style={[R.styles.fonts.hero, { color: R.colors.white }]}>{R.strings.user.oneStepSignup()}</Text>
                  <Text style={[R.styles.fonts.body, { color: R.colors.white, marginTop: 5, textAlign: 'center' }]}>
                    {R.strings.user.saveYourData()}
                  </Text>
                </View>
                <View style={{ marginTop: 30 }}>
                  <TextInputField
                    label={R.strings.user.email()}
                    onChangeText={(text): void => {
                      this.setState({ email: text })
                    }}
                    value={email}
                    maxLength={40}
                    returnKeyType="done"
                    containerStyle={{ ...R.styles.borderRadiuses.large }}
                    isRequired
                  />
                </View>
                <View style={{ marginTop: 8 }}>
                  <TextInputField
                    label={R.strings.user.password()}
                    onChangeText={(text): void => {
                      this.setState({ password: text })
                    }}
                    value={password}
                    maxLength={40}
                    returnKeyType="done"
                    containerStyle={{ ...R.styles.borderRadiuses.large }}
                    secureTextEntry
                    isRequired
                  />
                </View>
                <View style={{ marginTop: 14 }}>
                  <Button
                    title={R.strings.user.signup()}
                    buttonStyle={[R.styles.buttons.primaryButton, { backgroundColor: R.colors.emerald }]}
                    titleStyle={R.styles.buttons.primaryButtonTitle}
                    onPress={(): void => {
                      this.signUpWithEmail(email, password)
                    }}
                  />
                </View>
                <View style={[R.styles.columnCenter, { marginTop: 30 }]}>
                  <Text style={[R.styles.fonts.caption, { color: R.colors.white }]}>{R.strings.user.orSignup()}</Text>
                </View>
                <View style={[R.styles.rowCenter, { marginTop: 15 }]}>
                  <TouchableOpacity onPress={this.facebookSignUp} style={[R.styles.socialButton, { marginRight: 24 }]}>
                    <Image source={R.images.facebookLogo} />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this.googleSignUp} style={R.styles.socialButton}>
                    <Image source={R.images.googleLogo} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ paddingBottom: 25 }}>
                <View style={[R.styles.columnCenter, { marginTop: 20 }]}>
                  <Text style={[R.styles.fonts.headline, { color: R.colors.white }]}>{R.strings.user.alreadyMember()}</Text>
                </View>
                <View style={{ marginTop: 17 }}>
                  <Button
                    title={R.strings.user.signin()}
                    buttonStyle={[R.styles.buttons.primaryButton]}
                    titleStyle={R.styles.buttons.primaryButtonTitle}
                    onPress={(): void => this.setState({ isLoginModalOpen: true })}
                  />
                </View>
              </View>
            </View>
          </ScrollContainer>
        </View>
      </View>
    )
  }
}

export default SignupForm
