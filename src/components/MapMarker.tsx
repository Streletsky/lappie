import React, { Component, ReactElement } from 'react'
import { Marker } from 'react-native-maps'

import IMapPlace from '../types/MapPlace'
import R from '../res/R'

interface IComponentProps {
  marker: IMapPlace
  isSelected: boolean
  onMarkerClick(marker: IMapPlace): void
}

export default class MapMarker extends Component<IComponentProps> {
  public constructor(props: IComponentProps) {
    super(props)
  }

  private onMarkerClick = (): void => {
    const { marker, onMarkerClick } = this.props

    onMarkerClick(marker)
  }

  public render(): ReactElement {
    const { marker, isSelected } = this.props

    return (
      <Marker
        key={marker.id}
        coordinate={marker.coordinate}
        image={isSelected ? R.images.mapMarkerSelected : R.images.mapMarker}
        onPress={this.onMarkerClick}
        style={{ width: 70 }}
        tracksViewChanges
      />
    )
  }
}
