import React, { ReactElement } from 'react'
import { StyleSheet, ImageBackground, TouchableOpacity } from 'react-native'

import R from '../res/R'
import PetType from '../types/PetType'
import constants from '../utilities/constants'

const styles = StyleSheet.create({
  typeButton: {
    ...R.styles.defaultBorder,
    width: 44,
    height: 44,
    borderRadius: 22,
    shadowColor: R.colors.black,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.16,
    shadowRadius: 2,
    elevation: 5,
    backgroundColor: R.colors.cloud,
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
  },
})

interface IComponentProps {
  type: PetType
  active: boolean
  onPress(type: PetType): void
}

const PetTypeItem = (props: IComponentProps): ReactElement<IComponentProps> => {
  const { type, active, onPress } = props

  return (
    <TouchableOpacity
      style={[styles.typeButton, active && { borderWidth: 0, backgroundColor: constants.petTypeColor[type] }]}
      onPress={(): void => onPress(type)}
    >
      <ImageBackground source={constants.petTypeImage[type]} style={styles.image} />
    </TouchableOpacity>
  )
}

export default PetTypeItem
