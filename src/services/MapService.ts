import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import constants from '../utilities/constants'
import IMapPlace from '../types/MapPlace'
import { IPlacesJsonResult, IPlaceResult, IPlaceJsonResult } from '../types/PlacesResult'

export default class MapService {
  public getLocationAsync = async (): Promise<Location.LocationData> => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION)

    if (status !== 'granted') {
      return Promise.reject(new Error('Permission to access location was denied'))
    }

    return Location.getCurrentPositionAsync({})
  }

  public coordinatesDistance = (lat1: number, lon1: number, lat2: number, lon2: number, unit: string): number => {
    if (lat1 === lat2 && lon1 === lon2) {
      return 0
    }

    const radlat1 = (Math.PI * lat1) / 180
    const radlat2 = (Math.PI * lat2) / 180
    const theta = lon1 - lon2
    const radtheta = (Math.PI * theta) / 180
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)

    if (dist > 1) {
      dist = 1
    }

    dist = Math.acos(dist)
    dist = (dist * 180) / Math.PI
    dist = dist * 60 * 1.1515

    if (unit === 'K') {
      dist *= 1.609344
    }

    if (unit === 'N') {
      dist *= 0.8684
    }

    return dist
  }

  public fetchNearbyDoctors = async (latitude: number, longitude: number, radius = 3000): Promise<IMapPlace[]> => {
    return fetch(
      `https://maps.googleapis.com/maps/api/place/nearbysearch/json?type=veterinary_care&location=${latitude},${longitude}&radius=${radius}&key=${constants.googleMapsApiKey}`,
    )
      .then((response): Promise<IPlacesJsonResult> => response.json())
      .then(
        (responseJson): Promise<IMapPlace[]> => {
          const markers: IMapPlace[] = []

          for (let i = 0; i < responseJson.results.length; i++) {
            const item = responseJson.results[i]
            const marker: IMapPlace = {
              coordinate: {
                latitude: item.geometry.location.lat,
                longitude: item.geometry.location.lng,
              },
              address: item.vicinity,
              title: item.name,
              rating: item.rating,
              id: item.place_id,
              phone: undefined,
              modifiedAt: new Date(),
              detailsLoaded: false,
            }

            markers.push(marker)
          }

          return Promise.resolve(markers)
        },
      )
  }

  public fetchPlaceDetails = async (placeId: string): Promise<IPlaceResult> => {
    console.log(constants.googleMapsApiKey)
    return fetch(
      `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&fields=formatted_phone_number,formatted_address,id,place_id&key=${constants.googleMapsApiKey}`,
    )
      .then((response): Promise<IPlaceJsonResult> => response.json())
      .then(
        (responseJson): Promise<IPlaceResult> => {
          return Promise.resolve(responseJson.result)
        },
      )
  }
}
