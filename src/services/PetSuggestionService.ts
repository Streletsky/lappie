import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'

import IPet from '../types/Pet'
import IPetSuggestion from '../types/PetSuggestion'
import PetSuggestionType from '../types/PetSuggestionType'
import R from '../res/R'
import { showWeight } from '../utilities/helpers'

export default class PetSuggestionService {
  public getSuggestions = (pet: IPet): IPetSuggestion[] => {
    const result: IPetSuggestion[] = []

    if (!showWeight(pet)) return result

    if (pet.weightEntries.length === 0) {
      const suggestion: IPetSuggestion = {
        id: uuid(),
        title: R.strings.pets.weightSuggestion(pet.name),
        type: PetSuggestionType.AddFirstWeight,
        pet,
      }

      result.push(suggestion)
    }

    return result
  }
}
