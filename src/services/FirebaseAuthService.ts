import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth'
import { GoogleSignin } from '@react-native-community/google-signin'

export default class FirebaseAuthService {
  public onAuthStateChanged = (callback: FirebaseAuthTypes.AuthListenerCallback): (() => void) => {
    return auth().onAuthStateChanged(callback)
  }

  public signInWithEmail = (email: string, password: string): Promise<FirebaseAuthTypes.UserCredential> => {
    return auth().signInWithEmailAndPassword(email, password)
  }

  public signUpWithEmail = (email: string, password: string): Promise<FirebaseAuthTypes.UserCredential> => {
    return auth().createUserWithEmailAndPassword(email, password)
  }

  public signOut = (): Promise<void> => {
    return auth().signOut()
  }

  public userAnonymousLogin = (): Promise<FirebaseAuthTypes.UserCredential> => {
    return auth().signInAnonymously()
  }

  public userGoogleLogin = async (): Promise<FirebaseAuthTypes.UserCredential> => {
    await GoogleSignin.configure({
      webClientId: '789660170348-qrfa0ockdjebieoti8lmbiih5l22oeu4.apps.googleusercontent.com',
    })

    const { idToken } = await GoogleSignin.signIn()
    const googleCredential = auth.GoogleAuthProvider.credential(idToken)

    return auth().signInWithCredential(googleCredential)
  }

  public getCurrentUser = (): FirebaseAuthTypes.User | null => {
    return auth().currentUser
  }
}
