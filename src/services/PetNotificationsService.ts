import 'react-native-get-random-values'
import { v4 as uuid } from 'uuid'
import moment from 'moment'
import _ from 'lodash'

import IPet from '../types/Pet'
import IPetNotification from '../types/PetNotification'
import {
  isVaccinationPlanned,
  isVaccinationActive,
  isVaccinationEndUpcoming,
  isTreatmentPlanned,
  isTreatmentActive,
  showVaccination,
  showDeworming,
} from '../utilities/helpers'
import PetNotificationType from '../types/PetNotificationType'
import R from '../res/R'
import PetNotificationSubType from '../types/PetNotificationSubType'
import IVaccination from '../types/Vaccination'
import IDeworming from '../types/Deworming'

export default class PetNotificationsService {
  private getNotifications = (
    pet: IPet,
    entries: IVaccination[] | IDeworming[],
    notificationType: PetNotificationType,
  ): IPetNotification[] => {
    const result: IPetNotification[] = []
    const someActive = _.some(entries, (v): boolean => isVaccinationActive(v))
    const activeCount = _.filter(entries, (v): boolean => isVaccinationActive(v)).length
    entries.forEach((entry): void => {
      const today = new Date()
      const startDate = moment(entry.startDate)
      const endDate = moment(entry.startDate).add(entry.periodicity, 'y')
      const isPlanned = isVaccinationPlanned(entry)
      const isEndUpcoming = isVaccinationEndUpcoming(entry)

      const notification: IPetNotification = {
        id: uuid(),
        type: notificationType,
        subType: PetNotificationSubType.NoActiveVaccination,
        title: '',
        caption: undefined,
        pet,
        payload: entry,
      }

      if (isPlanned) {
        const comingUpIn = startDate.diff(today, 'days')

        notification.subType =
          notificationType === PetNotificationType.Vaccination
            ? PetNotificationSubType.PlannedVaccination
            : PetNotificationSubType.PlannedDeworming
        notification.title =
          notificationType === PetNotificationType.Vaccination
            ? R.strings.pets.health.vaccinationPlannedNotification(pet.name, comingUpIn)
            : R.strings.pets.health.dewormingPlannedNotification(pet.name, comingUpIn)

        result.push(notification)
      } else if (isEndUpcoming) {
        const comingUpIn = endDate.diff(today, 'days')

        notification.subType =
          notificationType === PetNotificationType.Vaccination
            ? PetNotificationSubType.VaccinationAboutToExpire
            : PetNotificationSubType.DewormingAboutToExpire
        notification.title =
          notificationType === PetNotificationType.Vaccination
            ? R.strings.pets.health.vaccinationAboutToExpireNotification(pet.name, comingUpIn)
            : R.strings.pets.health.dewormingAboutToExpireNotification(pet.name, comingUpIn)

        if (someActive && activeCount > 1) {
          notification.caption =
            notificationType === PetNotificationType.Vaccination
              ? R.strings.pets.health.vaccinationAboutToExpireStillActiveCaption(entry.name, activeCount)
              : R.strings.pets.health.dewormingAboutToExpireStillActiveCaption(entry.name, activeCount)
        } else {
          notification.caption =
            notificationType === PetNotificationType.Vaccination
              ? R.strings.pets.health.vaccinationAboutToExpireCaption()
              : R.strings.pets.health.dewormingAboutToExpireCaption()
        }

        result.push(notification)
      }
    })

    if (!someActive && result.length === 0) {
      const notification: IPetNotification = {
        id: uuid(),
        type: notificationType,
        subType:
          notificationType === PetNotificationType.Vaccination
            ? PetNotificationSubType.NoActiveVaccination
            : PetNotificationSubType.NoActiveDeworming,
        title:
          notificationType === PetNotificationType.Vaccination
            ? R.strings.pets.health.noActiveVaccinationNotification(pet.name)
            : R.strings.pets.health.noActiveDewormingNotification(pet.name),
        caption: R.strings.pets.health.itsDangerousCaption(),
        pet,
        payload: undefined,
      }

      result.push(notification)
    }

    return result
  }

  public getTreatmentNotifications = (pet: IPet): IPetNotification[] => {
    const { treatments } = pet
    const result: IPetNotification[] = []

    treatments.forEach((treatment): void => {
      const today = moment(new Date())
      const startDate = moment(treatment.startDate)
      const isPlanned = isTreatmentPlanned(treatment)
      const isActive = isTreatmentActive(treatment)

      const notification: IPetNotification = {
        id: uuid(),
        type: PetNotificationType.Treatment,
        subType: PetNotificationSubType.OnGoingTreatment,
        title: '',
        caption: undefined,
        pet,
        payload: treatment,
      }

      if (isPlanned) {
        const comingUpIn = startDate.diff(today, 'days')

        notification.subType = PetNotificationSubType.PlannedTreatment
        notification.title = R.strings.pets.health.treatmentPlannedNotification(pet.name, comingUpIn)

        result.push(notification)
      } else if (isActive) {
        const daysSinceStart = today.diff(startDate, 'days')
        const title =
          daysSinceStart > 45 ? R.strings.pets.monthCount(Math.ceil(daysSinceStart / 30)) : R.strings.pets.daysCount(daysSinceStart)

        notification.subType = PetNotificationSubType.OnGoingTreatment
        notification.title = `${pet.name}, ${treatment.name}, ${title}`
        notification.caption = treatment.description || undefined
        notification.payload = treatment

        result.push(notification)
      }
    })

    return result
  }

  public getVaccinationNotifications = (pet: IPet): IPetNotification[] => {
    if (!showVaccination(pet)) return []

    return this.getNotifications(pet, pet.vaccinations, PetNotificationType.Vaccination)
  }

  public getDewormingNotifications = (pet: IPet): IPetNotification[] => {
    if (!showDeworming(pet)) return []

    return this.getNotifications(pet, pet.dewormings, PetNotificationType.Deworming)
  }
}
