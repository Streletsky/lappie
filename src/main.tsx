import React, { Component, ReactElement } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { createAppContainer, NavigationState } from 'react-navigation'
import * as Font from 'expo-font'
import { registerRootComponent } from 'expo'
import * as Localization from 'expo-localization'
import { activateKeepAwake } from 'expo-keep-awake'
import { InitialProps } from 'expo/build/launch/withExpoRoot.types'
import i18n from 'i18n-js'
import { AsyncStorage } from 'react-native'

import AppNavigation from './navigators/AppNavigator'
import { store, persistor } from './store'
import AuthGate from './screens/user/AuthGate'

if (__DEV__) {
  activateKeepAwake()
}

const AppContainer = createAppContainer(AppNavigation)

export default class App extends Component<InitialProps> {
  public state = {
    isLoadingComplete: false,
  }

  public constructor(props: InitialProps) {
    super(props)
  }

  private getActiveRouteName = (navigationState: NavigationState): string | undefined => {
    if (!navigationState) {
      return undefined
    }

    const route = navigationState.routes[navigationState.index]

    if (route.routes) {
      return this.getActiveRouteName(route)
    }

    return route.routeName
  }

  private onNavigationStateChange = (prevState: NavigationState, currentState: NavigationState): void => {
    const currentRouteName = this.getActiveRouteName(currentState)
    const previousRouteName = this.getActiveRouteName(prevState)

    if (previousRouteName !== currentRouteName) {
      // Analytics.setCurrentScreen(currentRouteName)
    }
  }

  private loadResourcesAsync = async (): Promise<void> => {
    const handleLanguage = async (): Promise<void> => {
      const preferredLang = await AsyncStorage.getItem('lang')
      const [locale] = Localization.locale.split('-')

      if (preferredLang) {
        i18n.locale = preferredLang
      } else {
        i18n.locale = locale
      }
    }

    Font.loadAsync({
      'app-icons': require('../assets/fonts/app-pets.ttf'),
    })

    return Promise.resolve(
      Font.loadAsync({
        'app-icons': require('../assets/fonts/app-pets.ttf'),
      }),
    ).then(handleLanguage)
  }

  private handleLoadingError = (error: Error): void => {
    console.warn(error)
  }

  private handleFinishLoading = (): void => {
    this.setState({ isLoadingComplete: true })
  }

  public componentDidMount = (): void => {
    this.loadResourcesAsync()
      .then(this.handleFinishLoading)
      .catch(this.handleLoadingError)
  }

  public render(): ReactElement | null {
    const { isLoadingComplete } = this.state

    if (!isLoadingComplete) {
      return null
    }

    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AuthGate>
            <AppContainer onNavigationStateChange={this.onNavigationStateChange} />
          </AuthGate>
        </PersistGate>
      </Provider>
    )
  }
}

registerRootComponent(App)
