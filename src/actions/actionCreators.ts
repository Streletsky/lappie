import IDoctor from '../types/Doctor'
import IPet from '../types/Pet'
import { IUserLocation, IUserMarkersWithLocation } from '../types/PlacesResult'
import IMapPlace from '../types/MapPlace'

/* *************  Settings ************** */

export type ISettingsAction =
  | {
      type: 'SET_WEIGHT_UNIT'
      payload: number
    }
  | {
      type: 'SET_LANGUAGE'
      payload: number
    }

export const setWeightUnitSetting = (index: number): ISettingsAction => ({
  type: 'SET_WEIGHT_UNIT',
  payload: index,
})

export const setLanguageSetting = (index: number): ISettingsAction => ({
  type: 'SET_LANGUAGE',
  payload: index,
})

/* *************  Doctors ************** */

export type IDoctorsAction =
  | {
      type: 'ADD_DOCTOR'
      payload: IDoctor
    }
  | {
      type: 'SAVE_DOCTOR'
      payload: IDoctor
    }
  | {
      type: 'REMOVE_DOCTOR'
      payload: IDoctor
    }

export const addDoctor = (doctor: IDoctor): IDoctorsAction => ({
  type: 'ADD_DOCTOR',
  payload: doctor,
})

export const saveDoctor = (doctor: IDoctor): IDoctorsAction => ({
  type: 'SAVE_DOCTOR',
  payload: doctor,
})

export const removeDoctor = (doctor: IDoctor): IDoctorsAction => ({
  type: 'REMOVE_DOCTOR',
  payload: doctor,
})

/* *************  Pets ************** */

export type IPetsAction =
  | {
      type: 'ADD_PET'
      payload: IPet
    }
  | {
      type: 'SAVE_PET'
      payload: IPet
    }
  | {
      type: 'REMOVE_PET'
      payload: IPet
    }

export const addPet = (pet: IPet): IPetsAction => ({
  type: 'ADD_PET',
  payload: pet,
})

export const savePet = (pet: IPet): IPetsAction => ({
  type: 'SAVE_PET',
  payload: pet,
})

export const removePet = (pet: IPet): IPetsAction => ({
  type: 'REMOVE_PET',
  payload: pet,
})

/* ************* Location ************* */

export type ILocationAction =
  | {
      type: 'SAVE_LOCATION'
      payload: IUserLocation
    }
  | {
      type: 'SAVE_MARKERS'
      payload: IUserMarkersWithLocation
    }
  | {
      type: 'UPDATE_MARKER'
      payload: IMapPlace
    }

export const saveLocation = (location: IUserLocation): ILocationAction => ({
  type: 'SAVE_LOCATION',
  payload: location,
})

export const saveMarkers = (markers: IUserMarkersWithLocation): ILocationAction => ({
  type: 'SAVE_MARKERS',
  payload: markers,
})

export const updateMarker = (marker: IMapPlace): ILocationAction => ({
  type: 'UPDATE_MARKER',
  payload: marker,
})
