const colors = {
  // Greys
  cloud: '#EEEEEF',
  darkCloud: '#E1E0E7',
  pewter: '#B5B5C4',
  fossil: '#C3C3D2', // used in chevrons
  porpoise: '#8D8B9A',
  ghostWhite: '#EEEEF5',

  // Fills
  amber: '#D15019',
  cobalt: '#0070E0',
  aegean: '#0271E0',
  cedar: '#DA6304',
  emerald: '#45AE4D',
  scarlet: '#EF1E1E',
  periwinkle: '#5C6893',
  iris: '#8686B9',
  honey: '#C98D00',
  poison: '#19C927',
  transparent: 'rgba(0,0,0,0.80)',

  // Gradient

  gradient: {
    fill: {
      mauve: ['#9177a1', '#7034b7', '#556e8a'],
      honeycomb: ['#E6A900', '#C96000'],
      berry: ['#6565B5', '#4D4D7B'],
    },
    direction: {
      vertical: {
        start: { x: 0, y: 0 },
        end: { x: 0, y: 1 },
      },
      verticalReverse: {
        start: { x: 0, y: 1 },
        end: { x: 0, y: 0 },
      },
      diagonal: {
        start: { x: 0, y: 0 },
        end: { x: 1, y: 1 },
      },
      diagonalReverse: {
        start: { x: 1, y: 1 },
        end: { x: 0, y: 0 },
      },
    },
  },

  // Common
  white: '#FFF',
  black: '#000',

  // Pet Types
  cat: '#867AC5',
  dog: '#F8E774',
  rabbit: '#88AF70',
  rodent: '#DBD5FF',
  bird: '#5CC68E',
  fish: '#6AA3C7',
}

export default colors
