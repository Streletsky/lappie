import strings from './strings'
import images from './images'
import colors from './colors'
import styles from './styles'

const R = {
  strings,
  images,
  colors,
  styles,
}

export default R
