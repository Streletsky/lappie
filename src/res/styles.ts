import { ViewStyle, TextStyle, I18nManager, ImageStyle } from 'react-native'
import colors from './colors'
import featureChecker from '../utilities/featureChecker'
import constants from '../utilities/constants'

const screenWidth = featureChecker.getScreenWidth()

interface IFonts {
  footnote: TextStyle
  captionSmall: TextStyle
  caption: TextStyle
  callout: TextStyle
  description: TextStyle
  button: TextStyle
  body: TextStyle
  headline: TextStyle
  title: TextStyle
  star: TextStyle
  hero: TextStyle
}

interface IBorderRadiuses {
  small: ViewStyle
  medium: ViewStyle
  large: ViewStyle
  roundedBig: ViewStyle
  roundedSmall: ViewStyle
}

const defines = {
  topBarHeight: 44,
  listItemMinHeight: 44,
  containerPaddingVertical: 20,
  containerPaddingHorizontal: 16,
  defaultFontFamilyIos: 'System',
  defaultFontFamilyAndroid: 'System',
  cardMargin: screenWidth > constants.bigScreenSize ? 10 : 5,
}

const fontFamily = {
  fontFamily: featureChecker.isIos() ? defines.defaultFontFamilyIos : defines.defaultFontFamilyAndroid,
}

const fonts: IFonts = {
  footnote: {
    ...fontFamily,
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: -0.08,
    fontWeight: 'bold',
  },
  captionSmall: {
    ...fontFamily,
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0.07,
    fontWeight: 'normal',
  },
  caption: {
    ...fontFamily,
    fontSize: 12,
    lineHeight: 16,
    fontWeight: 'bold',
  },
  description: {
    ...fontFamily,
    fontSize: 15,
    lineHeight: 22,
    letterSpacing: -0.36,
    fontWeight: 'normal',
  },
  callout: {
    ...fontFamily,
    fontSize: 16,
    lineHeight: 21,
    letterSpacing: -0.32,
    fontWeight: 'normal',
  },
  button: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 17 : 15,
    lineHeight: screenWidth > constants.bigScreenSize ? 22 : 20,
    letterSpacing: -0.41,
    fontWeight: 'bold',
  },
  body: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 17 : 15,
    lineHeight: screenWidth > constants.bigScreenSize ? 22 : 20,
    letterSpacing: -0.41,
    fontWeight: 'normal',
    flexShrink: 1,
  },
  headline: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 17 : 15,
    lineHeight: screenWidth > constants.bigScreenSize ? 22 : 20,
    letterSpacing: -0.41,
    fontWeight: 'bold',
  },
  title: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 22 : 19,
    lineHeight: screenWidth > constants.bigScreenSize ? 28 : 24,
    letterSpacing: -0.1,
    fontWeight: 'normal',
  },
  star: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 26 : 21,
    lineHeight: screenWidth > constants.bigScreenSize ? 31 : 28,
    letterSpacing: 0.1,
    fontWeight: 'bold',
  },
  hero: {
    ...fontFamily,
    fontSize: screenWidth > constants.bigScreenSize ? 28 : 26,
    lineHeight: screenWidth > constants.bigScreenSize ? 34 : 32,
    letterSpacing: 0.1,
    fontWeight: 'bold',
  },
}

const borderRadiuses: IBorderRadiuses = {
  small: {
    borderRadius: 3,
  },
  medium: {
    borderRadius: 6,
  },
  large: {
    borderRadius: 9,
  },
  roundedSmall: {
    borderRadius: 17,
  },
  roundedBig: {
    borderRadius: 25,
  },
}

const reset: ViewStyle = {
  paddingTop: 0,
  paddingBottom: 0,
  paddingLeft: 0,
  paddingRight: 0,
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0,
  borderTopWidth: 0,
  borderBottomWidth: 0,
  borderLeftWidth: 0,
  borderRightWidth: 0,
}

const defaultBorder: ViewStyle = {
  borderColor: colors.darkCloud,
  borderWidth: 1,
}

const borderTop: ViewStyle = {
  borderTopColor: colors.darkCloud,
  borderTopWidth: 1,
}

const borderBottom: ViewStyle = {
  borderBottomColor: colors.darkCloud,
  borderBottomWidth: 1,
}

const rootView: ViewStyle = {
  flex: 1,
  backgroundColor: colors.black,
}

const safeArea: ViewStyle = {
  backgroundColor: colors.ghostWhite,
  flex: 1,
}

const articleContainer: ViewStyle = {
  flex: 1,
  paddingHorizontal: defines.containerPaddingHorizontal,
  paddingVertical: defines.containerPaddingVertical,
}

const defaultContainer: ViewStyle = {
  paddingHorizontal: defines.containerPaddingHorizontal,
  flex: 1,
}

const listSectionContainer: ViewStyle = {
  backgroundColor: colors.ghostWhite,
  borderTopColor: colors.darkCloud,
  borderTopWidth: 1,
}

const listSectionTitle: TextStyle = {
  ...fonts.callout,
  color: colors.porpoise,
  marginVertical: 10,
  marginLeft: 18,
}

const listItem: ViewStyle = {
  ...borderRadiuses.large,
  minHeight: defines.listItemMinHeight,
  paddingVertical: 6,
  paddingHorizontal: 14,
}

const primaryButton: ViewStyle = {
  ...borderRadiuses.roundedBig,
  backgroundColor: colors.amber,
  width: '100%',
  height: screenWidth > constants.bigScreenSize ? 50 : 44,
}

const primaryButtonDisabled: ViewStyle = {
  ...primaryButton,
  backgroundColor: colors.darkCloud,
}

const primaryButtonTitle: TextStyle = {
  ...fonts.button,
  color: colors.white,
  paddingTop: 0,
  paddingBottom: 0,
}

const primaryButtonTitleDisabled: TextStyle = {
  ...primaryButtonTitle,
  color: colors.porpoise,
}

const secondaryButton: ViewStyle = {
  ...borderRadiuses.roundedBig,
  ...defaultBorder,
  width: '100%',
  height: screenWidth > constants.bigScreenSize ? 50 : 44,
  backgroundColor: colors.white,
}

const secondaryButtonTitle: TextStyle = {
  ...fonts.button,
  fontWeight: '500',
  color: colors.amber,
}

const smallButton: ViewStyle = {
  ...borderRadiuses.roundedSmall,
  backgroundColor: colors.amber,
  height: 34,
  paddingVertical: 0,
  paddingHorizontal: 15,
}

const smallButtonDisabled: ViewStyle = {
  ...smallButton,
  backgroundColor: colors.darkCloud,
}

const smallButtonTitle: TextStyle = {
  ...fonts.button,
  paddingVertical: 0,
  color: colors.white,
}

const smallButtonTitleDisabled: TextStyle = {
  ...smallButtonTitle,
  color: colors.porpoise,
}

const textButton: ViewStyle = {
  ...smallButton,
  borderWidth: 0,
  paddingVertical: 0,
  backgroundColor: 'transparent',
}

const headerLeftButton: ViewStyle = {
  height: featureChecker.isIos() ? defines.topBarHeight : 18,
  width: featureChecker.isIos() ? defines.topBarHeight : 18,
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  alignItems: 'center',
  justifyContent: 'center',
  alignContent: 'center',
}

const headerRightButton: ViewStyle = {
  height: defines.topBarHeight,
  width: defines.topBarHeight,
  alignItems: 'center',
  justifyContent: 'center',
  alignContent: 'center',
}

const pushContentOpposite: ViewStyle = {
  flex: 1,
  justifyContent: 'space-between',
}

const divider: ViewStyle = {
  backgroundColor: colors.darkCloud,
  height: 1,
}

const genericField: ViewStyle = {
  minHeight: defines.listItemMinHeight,
  paddingRight: 14,
  backgroundColor: colors.white,
  display: 'flex',
  flexDirection: 'row',
  paddingLeft: 17,
  alignItems: 'center',
  justifyContent: 'space-between',
}

const bottomButtonWrapper: ViewStyle = {
  marginTop: defines.containerPaddingVertical,
  paddingBottom: defines.containerPaddingVertical,
  paddingHorizontal: defines.containerPaddingHorizontal,
}

const overlay: ViewStyle = {
  padding: 0,
  position: 'absolute',
  bottom: 0,
  width: '100%',
  borderBottomRightRadius: 0,
  borderBottomLeftRadius: 0,
}

const inputLabelStyle: TextStyle = {
  ...fonts.body,
  color: colors.porpoise,
}

const screenHeaderWrapper: ViewStyle = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: 20,
  paddingHorizontal: 35,
  alignItems: 'center',
  marginBottom: 12,
}

const smallAvatar: ImageStyle = {
  width: 40,
  height: 40,
  borderRadius: 20,
  resizeMode: 'cover',
}

const largeAvatar: ImageStyle = {
  width: 76,
  height: 76,
  borderRadius: 38,
  resizeMode: 'cover',
}

const rowCenter: ViewStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  alignContent: 'center',
  justifyContent: 'center',
}

const columnCenter: ViewStyle = {
  flexDirection: 'column',
  alignItems: 'center',
  alignContent: 'center',
  justifyContent: 'center',
}

const requiredIndicator: ViewStyle = {
  ...borderRadiuses.small,
  backgroundColor: colors.scarlet,
  width: 6,
  height: 6,
  top: -8,
  left: 2,
}

const dotsWrapper: ViewStyle = {
  width: 24,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
}

const theDot: ViewStyle = {
  ...borderRadiuses.small,
  backgroundColor: colors.black,
  width: 6,
  height: 6,
}

const greenDot: ViewStyle = {
  borderRadius: 5,
  backgroundColor: colors.poison,
  width: 10,
  height: 10,
  marginRight: 5,
}

const expandableCardContent: ViewStyle = {
  paddingLeft: 17,
  paddingRight: 21,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
}

const removeRounding: ViewStyle = {
  borderRadius: 0,
}

const removeRoundingTop: ViewStyle = {
  borderTopRightRadius: 0,
  borderTopLeftRadius: 0,
}

const removeRoundingBottom: ViewStyle = {
  borderBottomRightRadius: 0,
  borderBottomLeftRadius: 0,
}

const modalTopSection: ViewStyle = {
  paddingVertical: 15,
  flexDirection: 'row',
  alignItems: 'center',
  alignContent: 'center',
  justifyContent: 'center',
}

const socialButton: ViewStyle = {
  width: 44,
  height: 44,
  borderRadius: 22,
  backgroundColor: colors.white,
  justifyContent: 'center',
  alignItems: 'center',
}

const articleTitle: TextStyle = {
  ...fonts.hero,
  marginVertical: 10,
  textAlign: 'center',
}

const articleParagraph: TextStyle = {
  ...fonts.body,
  marginVertical: 5,
}

const cardShadow: ViewStyle = {
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.2,
  shadowRadius: 7,
  elevation: 7,
}

const cardShadowSmaller: ViewStyle = {
  shadowColor: '#000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.1,
  shadowRadius: 7,
  elevation: 7,
}

const styles = {
  fonts,
  borderRadiuses,
  constants: defines,
  reset,
  rootView,
  safeArea,
  defaultContainer,
  articleContainer,
  listSectionContainer,
  listSectionTitle,
  listItem,
  headerLeftButton,
  headerRightButton,
  divider,
  pushContentOpposite,
  defaultBorder,
  genericField,
  borderTop,
  borderBottom,
  bottomButtonWrapper,
  overlay,
  inputLabelStyle,
  screenHeaderWrapper,
  smallAvatar,
  largeAvatar,
  rowCenter,
  columnCenter,
  requiredIndicator,
  dotsWrapper,
  theDot,
  greenDot,
  expandableCardContent,
  removeRounding,
  removeRoundingTop,
  removeRoundingBottom,
  modalTopSection,
  socialButton,
  articleTitle,
  articleParagraph,
  cardShadow,
  cardShadowSmaller,
  fontFamily,
  buttons: {
    primaryButton,
    primaryButtonDisabled,
    primaryButtonTitle,
    primaryButtonTitleDisabled,
    secondaryButton,
    secondaryButtonTitle,
    smallButton,
    smallButtonDisabled,
    smallButtonTitle,
    smallButtonTitleDisabled,
    textButton,
  },
}

export default styles
