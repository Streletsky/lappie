const images = {
  typeCat: require('../../assets/images/type-cat.png'),
  typeDog: require('../../assets/images/type-dog.png'),
  typeBird: require('../../assets/images/type-bird.png'),
  typeRodent: require('../../assets/images/type-rodent.png'),
  typeFish: require('../../assets/images/type-fish.png'),
  typeRabbit: require('../../assets/images/type-rabbit.png'),

  doctorProfile: require('../../assets/images/doctor.png'),

  signupBackground: require('../../assets/images/signup-bg.jpg'),
  facebookLogo: require('../../assets/images/signin-facebook.png'),
  googleLogo: require('../../assets/images/signin-google.png'),

  placeholderDog: require('../../assets/images/placeholder-dog.png'),
  placeholderCat: require('../../assets/images/placeholder-cat.png'),
  placeholderRabbit: require('../../assets/images/placeholder-rabbit.png'),
  placeholderFish: require('../../assets/images/placeholder-fish.png'),
  placeholderBird: require('../../assets/images/placeholder-bird.png'),
  placeholderRodent: require('../../assets/images/placeholder-rodent.png'),

  mapMarker: require('../../assets/images/map-marker.png'),
  mapMarkerSelected: require('../../assets/images/map-marker-selected.png'),

  smallStar: require('../../assets/images/star.png'),

  paw: require('../../assets/images/paw-bg.png'),

  careCardImage: require('../../assets/images/tier-checkup.png'),
  healthCardImageCat: require('../../assets/images/tier-health-cat.png'),
  healthCardImageDog: require('../../assets/images/tier-health-dog.png'),
  healthCardImageRodent: require('../../assets/images/tier-health-rodent.png'),
  healthCardImageBird: require('../../assets/images/tier-health-bird.png'),
  healthCardImageRabbit: require('../../assets/images/tier-health-rabbit.png'),
  healthCardImageFish: require('../../assets/images/tier-health-cat.png'),

  allergyImage: require('../../assets/images/allergy.png'),
  careImage: require('../../assets/images/scissors.png'),
  foodImage: require('../../assets/images/soup.png'),
  docsImage: require('../../assets/images/document.png'),
  exportImage: require('../../assets/images/export.png'),
  dizzyImage: require('../../assets/images/dizzy.png'),
  toyImage: require('../../assets/images/toy.png'),

  settingsBetaImage: require('../../assets/images/settings-beta.png'),

  aboutImage: require('../../assets/images/puff-icon.png'),

  helpImageDewormActive: require('../../assets/images/help-example-deworm-active.png'),
  helpImageVaccinationActive: require('../../assets/images/help-example-vaccination-active.png'),
  helpImageTreatment: require('../../assets/images/help-example-treatment.png'),
  helpImageTreatmentFull: require('../../assets/images/help-example-treatment-full.png'),
  helpImageDoctorMap: require('../../assets/images/help-example-doctor.png'),

  pawsAnimation: require('../../assets/animations/loading-paws.json'),
}

export default images
